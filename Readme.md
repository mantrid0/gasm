gasm
====

gasm translates scripts for interactive web guides into JavaScript which can run in web browser.
The name stands for guide assembler. The script language which is translated is called guide script 
in this document.

Functionality is defined by a run time library and thus is modifiable and extendable by user.

The RTL documentation can be found at 
[RTL documentation](https://mantrid0.gitlab.io/gasm/rtl/index.html).

This document describes the assembler and the script language.


Usage
-----
Java is required to compile a script. Command line Syntax is:

```java -jar GASM.jar [<options>] [<input> [<output>]]```

### Parameters
 
#### `<input>`    
Input file. If undefined or '-' stdin is used.

#### `<output>`
Output file. If undefined or '-' stdout is used

### Options
    
#### `-ad`
Assemble + disassemble. Only useful for debugging (default: assemble)

#### `-p <path>`
Add a search path. Can be specified multiple times.

#### `-s <name>`
Source name (useful if read from stdin)

#### `-h`
Print a help message.

### Example
```java -jar GASM.jar guide.gsc guide.gjs```

Assembles `guide.gsc` and stores the result in `guide.gjs`.


Requirements
------------
A modern browser with support for asynchronous functions is required for playback
(currently everything but Microsoft Internet Explorer).


State recovery
--------------
The whole state is stored in the object `scriptVars`. That object should be used for storing all
script variables.

Restoring this object allows to re-entry at a saved state.
 

How it works -- branching
-------------------------
The assembler generates JavaScript code blocks. Execution order is controlled by a run time library
(RTL) which is split into two parts. The low level part controls the execution and should usually 
not be changed. The high level part is responsible for the look and feel. It may be modified or 
overwritten for more customization.

The script code runs within asynchronous context, i.e. the `await` operator can be used (and is used 
order to implement non-blocking delays).

### Execution stack
This section describes how the execution control is implemented internally. That may be helpful 
for understanding. If it looks boring, jump to [Syntax](#syntax) section.

Execution order and thus branching is controlled by the variable `scriptVars.currentBlock` which 
contains the index of the currently running block and the execution stack `scriptVars.nextBlock[]`
where `scriptVars.nextBlock[scriptVars.nextBlock.length-1]` defines the index of the next code
block. The stack pointer is `scriptVars.nextBlock.length-1`. 

The 'return' statement exits a code block. Arguments are ignored.

`scriptVars.nextBlock[scriptVars.nextBlock.length-1]` is set to -1 before starting a block.
If that variable is not a number or negative after the execution of the block
`scriptVars.currentBlock+1` (which is the next block) is used as index for the next block.

A Goto command is implemented by setting 'scriptVars.nextBlock[scriptVars.currentBlock.length-1]'
to the index of the target code block followed by 'return;'.

A Call command (branching to a sub routine) is implemented by setting 
'scriptVars.nextBlock[scriptVars.currentBlock.length-1]' to the block index which shall be 
executed after the return from the subroutine (usually the next block or the current
one) and by setting 'scriptVars.nextBlock[scriptVars.nextBlock.length]' to he block index of the
sub routine (this automatically increases the stack pointer) followed by `return;`

Return from a subroutine is implemented by decreasing the stack pointer followed by `return;`.

Syntax
------
Guide scripts are translated line by line. Leading spaces of lines are ignored. 

### `//`: Comments
Comments start with `//`. Comments in JavaScript lines are not removed.

### `\\`: Line concatenating operator
If a line ends with a backslash (`\\`), gasm removes that character and appends the next line to thes
current one. For example, this can be useful for defining long HTML elements like tables.

There must be no whitespace after the backslash. If you want that a trailing backslash is not
interpreted as line concatenating operator, just add a whitespace.

### `<label>:`: Labels
Lines that start with a word followed by a colon are treated as labels (Syntax: `<label>:`). The
word before the colon is the label. Labels are not case sensitive and must not contain spaces.

Internally, each label and each include directive, see below, starts a new code block. Empty labels
are allowed in order to enforce this.

### `#`: JavaScript code
All lines that start with `#` (after removing leading spaces) are treated as JavaScript code. (The
leading `#` is removed.)

### `.  <file>` : Include directive
This includes another file. If that file already has been included, an warning is emitted and the
file is not included again.

### Macros
All other statements are just macros which are replaced by some JavaScript code. Macros should be
preferred because they allow a better syntax checking. Macros are not case sensitive.

Only one macro per line is allowed. Arguments are separated by one or multiple spaces. Because there
is no quoting, only the last argument may contain spaces. Double quotes are allowed, singe quotes 
are escaped automatically in text arguments.

JavaScript and macros can be mixed. Some care must be taken with the macros `C` (call) and `D` 
(delay) because these macros start a new code block which acts as return target for calls. The 
@ variants (`@call` and `@delay`) can be used if this is not desired.

E.g. that's a valid expression:

	# if ( cond ) {
	#	some_cmd();
		I some_picture.jpg
		<< some text
	# }
	# else {
	#	another_cmd();
		V some_video.mp4
		<< another text
		G somwehere
	# }


But this will result in corrupt code:

	# if ( cond ) {
		. file.gsc
	# }

This will fail too:

	# if ( cond ) {
	C somewhere
	# }
	
But this will work:

	# if ( cond ) {
	# 	@call('somewhere');
	# }

#### `< [<text>]` : Set text macro
This macro sets contents  of the text field to `<text>`. In particular it is replaced by the
JavaScript code `setText('<text>');` if `<text>` is non-empty, otherwise by `clearText();` The
functions `setText` and `clearText` are defined in the high level RTL. 

#### `<< [<text>]` : Append text macro
This appends `<text>` to the contents of the text field. In particular it is replaced by the 
JavaScript code `appendText('<text>');` where `<text>` can also be empty. The function `appendText`
is defined in the high level RTL. 

#### `I [<file>]` : Image macro
This loads the image `<file>` to the media field or clears it. If no argument is given, this statement
is replaced by the Javascript code `clearMedia()`. Otherwise it is replaced by 
`setImage('<file>');`. The functions `clearMedia` and `setImage` are defined in the high level RTL.
A warning is emitted if the file cannot be found.

#### `V [<file>]` : Video macro
This plays the video <file> to the media field or clears it. If no argument is given, this statement
is replaced by the Javascript code `clearMedia()`. Otherwise it is replaced by the JavaScript code 
`playVideo('<file>');`. The function `playVideo` is defined in the high level RTL. A warning is
emitted if the file cannot be found.

#### `VS <start time> <file>` : Video macro with given start time
This loads the video `<file>` to the media field and starts playback at <start time>. In particular it
is replaced by the JavaScript code `playVideo('<file>',<start time>);`. The function `playVideo` is
defined in the high level RTL. A warning is emitted if the file cannot be found.

#### `A [<file>]` : Audio macro
Plays the audio file `<file>` or stops audio. If no argument is given, this statement is replaced by
the JavaScript code `stopAudio();`, Otherwise it is replaced by `playAudio('<file>');`. The
functions `stopAudio`and `playAudio` are defined in the high level RTL. A warning is emitted if the
file cannot be found.

#### `AS <start time> <file>` : Audio macro with given start time
This loads the audio file `<file>` and starts playback at `<start time>`. In particular it is replaced
by the JavaScript code `playAudio('<file>',<start time>);`. The function `playAudio` is defined in
the high level RTL. A warning is emitted if the file cannot be found.

#### `G <label>` : Goto macro
Jumps to another code block which is identified by `<label>`. This is replaced by the JavaScript code
`{ setGoto('<label>'); return; }`. The function `setGoto` is defined in the low level RTL. An error
is emitted if the label is not defined.

#### `C <label>` : Call macro
Jumps to another code block which is identified by `<label>` and stores the address of the next line
(which treated as a new code block) as return address on the execution stack. In particular it is
replaced by the JavaScript code `{ setCall('<label>'); return; }`. The function `setCall` is defined
in the low level RTL. An error is emitted if the label is not defined. A new code block which acts
as the return target is started after this macro.

#### `R`: Return macro
Jumps to the return address stored by the last call command/macro. In particular it is replaced by
the JavaScript code `{ callReturn(); return; }`. The function `callReturn` is defined in the low
level RTL. An error is emitted if the label is not defined.

#### `D [<delay>]` : Delay macro
This implements a delay, i.e. it waits for a given amount of time. I particular it is replaced by
the JavaScript code `{ if (await delay([<delay>]) ) return; }`. The asynchronous function `delay`
is defined in the low level RTL. 

An error is emitted if the argument is given but invalid. If argument is not given or less or equals
zero, it waits forever/until interrupted. Special values 'A' and 'V' (not case sensitive) can be
used to wait till end of the current audio of video file. Positive numbers are treated as  wait time
in seconds.

A new code block is started after this macro. This code block is executed after the wait time or if
the delay is interrupted by the `interrupt()` function.

#### `BG <label> <text>` : Goto button macro
This implements a global goto button. If pressed, the script jumps to the code block identified by
`<label>`. The button appears until it is removed using the `RB` macro. The second argument `<text>` is 
the button text.

In particular the `BG` macro is replaced by the JavaScript code 
`addButton("setGoto('<label>')",'<text>');`. The function `setGoto` is defined in the low level RTL.
The function 'addButton' is defined in the high level RTL. An error is emitted if the label is not
defined.

#### `BC <label> <text>` : Call button macro with return to next code block.
This implements a global call button. If pressed, the script jumps to the code block identified by
the `<label>` and stores the the next code block as return address on the execution stack.
Usually the button is pressed during a delay. In that case the return address is the line after the
delay macro. The button appears until it is removed using the `RB` macro. The second argument `<text>`
is the button text.

In particular the `BC` macro is replaced by the JavaScript code 
`addButton("setCall('<label>')",'<text>');`. The function `setCall` is defined in the low level RTL.
The function 'addButton' is defined in the high level RTL. An error is emitted if the label is not
defined.

#### `BCR <label> <text>` : Call button macro with return to current code block.
This implements a global call button. If pressed, the script jumps to the code block identified by
the `<label>` and stores the current code block as return address on the execution stack, i.e. the
current code block will be re-executed after executing the return macro. The button appears until it
is removed using the `RB` macro. The second argument `<text>` is the button text.

In particular the `BCR` macro is replaced by the JavaScript code 
`addButton("setCall('<label>',true)",'<text>');`. The function `setCall` is defined in the low level
RTL. The function 'addButton' is defined in the high level RTL. An error is emitted if the label is
not defined.

#### `B <action> <text>` : Button macro
This defines a global general purpose button. If pressed, `<action>` is executed. The button appears
until it is removed using the `RB` macro. The second argument `<text>` is the button text.

In particular this macro is replaced by the JavaScript code `addButton('<action>','<name>');`. The
function 'addButton' is defined in the high level RTL.

#### `RB <text>` : Remove button macro
Removes a global button which is identified by the button text `<text>`. This is replaced by the
JavaScript code `rmButton('<name>');`. The function 'rmButton' is defined in he high level RTL. A
warning is emitted a button with that name never has been defined using one of the button macros.

#### `TBG <label> <text>` : Text / temporary goto button macro
This implements a goto button which appears in the text field. If pressed, the script jumps to the
code block identified by `<label>`. The button appears until the content of the text field is
replaced (but not if content is just appended). The second argument `<text>` is the button text.

In particular then `TBG` macro is replaced by the JavaScript code
`appendTButton("setGoto('<label>')",'<name>');`. The function `setGoto` is defined in the low level
RTL. The function 'appendTButton' is defined in the high level RTL. An error is emitted if the label
is not defined.

#### `TBC <label> <text>` : Text / temporary call button macro with return to next code block.
This implements a call button which appears in the text field. If pressed, the script jumps to the
code block identified by the `<label>` and stores the line of the next code block as return address on
the execution stack. Usually the button is pressed during a delay. In that case the return address
is the line after the delay macro. The button appears until the content of the text field is
replaced (but not if content is just appended). The second argument `<text>` is the button text.

In particular the `TBC` macro is replaced by the JavaScript code 
`appendTButton("setCall('<label>')",'<name>');`. The function `setCall` is defined in the low level
RTL. The function 'appendTButton' is defined in the high level RTL. An error is emitted if the label
is not defined.

#### `TBCR <label> <text>` : Text / temporary call button macro with return to current code block.
This implements a call button which appears in the text field. If pressed, the script jumps to the
code block identified by the `<label>` and stores the current code block as return address on the
execution stack, i.e. the current code block will be re-executed after executing the return macro.
The button appears until the content of the text field is replaced (but not if content is just
appended). The second argument `<text>` is the button text.

In particular the `TBC` macro is replaced by the JavaScript code 
`appendTButton("setCall('<label>',true)",'<name>');`. The function `setCall` is defined in the low
level RTL. The function 'appendTButton' is defined in the high level RTL. An error is emitted if the
label is not defined.

#### `TB <action> <text>` : Button macro
This defines a general purpose button which appears in the text field. If pressed, `<action>` is
executed.  The button appears until the content of the text field is replaced (but not if content is
just appended). The second argument `<text>` is the button text.

In particular this macro is replaced by the JavaScript code `appendTButton('<action>','<name>');`.
The function 'appendTButton' is defined in the high level RTL.

#### `@<name>`: Variable macro
All script variables should be stored in the `scripVars` object which holds the complete state of a
script. For convenience, expressions of the form `@<name>` are replaced by `scriptVars.<name>`.
`<name>` must start with a letter and can consist in letters, digits, the `_` character, and a dot 
(`.`) followed by a letter and must  not be followed by a left left parenthesis (`(`) . If the at
character is needed, `@@` can be used instead (which is replaced by a single `@`). 

Variable macro are expand in JavaScript code and the text macros `<` and `<<`. In the latter case
quotes are added automatically. (Using variables in file names or labels would break the syntax
checking. JavaScript code should be used if that is needed.)

#### `@goto(<parameter>);`: Goto macro
This macro is replaced by `{ setGoto(<parameter>); return; }`. This macro is only expanded in 
JavaScript code. This variant can be used with other labels than literal strings, e.g. variables.

#### `@call(<parameter1>[,<parameter2>]);`: Goto macro
This macro is replaced by `{ setCall(<parameter1>[,<parameter2>]); return; }`. This macro is only
expanded in  JavaScript code. This variant can be used with other labels than literal strings,
e.g. variables and it does not interrupt the code block.

#### `@delay([<parameter1>]);`: Delay macro
This macro is replaced by `{ if (await delay(<delay>) ) return; }`. This macro is only
expanded in  JavaScript code. This variant can be used with other delay than literal numbers,
e.g. variables and it does not interrupt the code block.

#### `@label(<parameter>);`: Label check
This macro is replaced by `<parameter>;`. If parameter is a string literal gasm checks whether
this label exists and emits an error is not. The only purpose of the macro if to check for dead
links, e.g. in variable assignments. It makes no sense to call this macro without a string literal.

Note that the macro must end with `);` and that the macro expansion ends with a semicolon.

#### `@{<expr>}`: Expression evaluation within text macro
This macro is only evaluated in text macros `<` and `<<` and inserts the string representation of 
`<expr>` in text. For example

	<< Next is @{@cnt+1}.
	
is replaced by `appendText('Next is '+(scriptVars.cnt+1)+'.');`

### Extended syntax checking
Besides of the syntax checking using the macros, gasm also scans JavaScript code for occurrences of
`setGoto` and `setCall` commands. If they are called with literal string expressions (strings in
single or double quotes), gasm checks whether these labels exist and emits an error is not.

Parentheses can be used to hide string literals from syntax checker.

 
Directories
-----------
The variables `scriptVars.imageDir`, `scriptVars.videoDir` and `scriptVars.audioDir` can be used to
specify media directories which are prepend to the path names of images, videos and audio files, 
respectively. gasm scans the initial code block for assignments of these variables using the `@` 
macro and automatically searches media files in these directories. It's therefore recommended to set
these variables at start of the script.

All path names are realtive to the directory where the main script is located. This directory is
strored in the special varaible 'baseDirectroy'. That varaible can be overwritten is the guide is
moved after compilation.
