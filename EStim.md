EStim: The Theory
-----------------

### Voltage vs. current
Neurons react on voltage, but differential voltage, not absolute voltage. Because differential 
voltage is proportional to the current, a good Estim boxes provides a current controlled output
signal. That's also the reason why small contact surfaces / bad contact resistance feel stinging
(high current density).

### Carrier waveform and safety
A Estim signal must only contain high frequency components because low frequencies can cause 
electrolysis which can be very dangerous. Therefore Estim signals consist in a modulated carrier 
waveform which only contains frequency components above a few hundred Hz. (The upper frequency limit
is defined by the minimum pulse width required by the neuron to react. This is about 200µs). 

A save Estim box therefore either contains a high-pass filter or rectifies, filters and re-modulates
the input audio signal. The latter kind of devices is not recommended for the sound files generated
by MPGenerator (or many other Estim sound files) because they can't preserve phase modulation (see
below) or other special effects.

The frequency components of the modulation waveform does not appear in the spectrum of the modulated
 output signal.

### Amplitude modulation
The stimulation intensity at the contact surface of the electrodes directly depends form the
amplitude of the modulation waveform, in particular its approximately proportional in square
(doubling the volume increases intensity by about factor 4).

If more than one channels is connected to one electrode (see below) the intensity depends on the sum
 of the signals.

### Phase modulation
Each output channel has two poles. If one pole of each channel is connected to a pole of another
channel and if this pole is used as an electrode something magically happens: the path of the 
internal currents vary. This effect is often described as the "3D effect". 

In particular, the internal currents are a mix of the channel signals. If the carrier wave forms are
shifted against each other they are either amplified (phase shift between -90° and 90°) or damped 
(phase shift between 90° and 270°). This phase shift is called phase modulation.

Conclusion: Intensity at the contact surface depends on amplitude modulation, stimulation of the
internal neurons depends on amplitude and phase modulation.

Unfortunately there aren't much Estim sound files the make use of this technique. That's the reason
why this project was started.

### Pulse width modulation
This defines the wave form of the carrier signal. Its something between a sinusoidal signal (pulse
width 1) and a rectangular signal (pulse width 2).

### Modulation frequencies
Neurons can only detect variations up to a certain speed. Modulation frequency above that limit feel
like a constant signal. This limit frequency depends on the area of the body. It's highest (up to
about 30Hz to 50Hz) at the most sensitive areas.

It turned out that modulation signals close or slightly above that limit feel very intense (in a 
good way), very low frequencies (below a few Hz) feel good too but medium frequencies can be 
uncomfortable. (That's an effect of neuronal inhibition).
