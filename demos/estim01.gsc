form_start:
    #   formStart();					// starts form mode
    #   @channelConfig = encodeChannelConfig();		// converts current channel setup to configuration to a string
    #   @channelConfigErr = false;			// no error

channelConfig:
    #   estimStop();					// stop audio
    <	<h2>Device requirements and recommendations</h2>
    <<  @estimRequirements
    <<	<h2>Channel setup</h2>
    <<  Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and @maxEstimChannels. 
    <<  E.g. "1,1,1" for 3 independent channels or "2,1,1," for 4 channels where the first two ones have equal constraints.
    #	appendTextInput("channelConfig");
    <<	Click on 'Done' in order to start audio with a 5s ramp-up time.
    TB  interrupt(); Done
    #   if ( @channelConfigErr ) appendText("Error: " + @channelConfigErr );
    D   

// checks syntax of the channel configuration string and goto previos code block in an error occurred
    #   @channelConfigErr = await decodeChannelConfig( @channelConfig );
    #   if ( @channelConfigErr ) @goto("channelConfig");
// forces to end code block in order to avoid printing the stuff below if an error occurred
:

stopTraining:
    #	@volTraining = 0;
    #	@d1Training = 0;
    #	@d2Training = 0;

form2:
    #   await estimPlay();				// start audio and initializes it if necessary 
    <   <h2>EStim parameters</h2>
    <<  <h3>Calibration parameters</h3>
    <<  <b>These parameters are user dependent and should be set during calibration.</b> \
	The software does not touch the mixer, i.e. both the mixer and the amp knob can be used to calibrate volume. \
	It is recommended (but not required) to use equal volume levels for each channel and control the balance using the "Channel balance" setting that can be adjusted below.
    #	appendEstimSlider( "<b>Carrier frequency</b> in Hz. Recommended: 400..800. Parameter name is 'fc'." , "fc" );
    #	appendEstimSlider( "<b>Channel balance</b> per group. Recommended: > 0.5. Smaller values feel less intense and softer, because that setting influences penetration depth. (It is o.k. to use the value 1 because this does not affect the volume.) Parameter name is 'cb1' for channel group 1, ..." , "cb" );
    #	appendEstimSlider( "<b>Current limitation at common</b>. 0 means no limitation. 1 is the maximum feasible limitation and 2 is more or less (depending on output circuit and amplifier balance) equal to disconnecting common. Parameter name is 'ccl'." , "ccl" );
    #	appendEstimSlider( "<b>Sensitivity</b>. Chose 0.75..1 for electrodes at most sensitive places (e.g. glans) and 0..0.25 for electrodes at least sensitive places (e.g. prostate). Parameter name is 'sens1' for channel group 1, ..." , "sens" );
    <<  <h3>Tease settings</h3>
    <<	<b>These parameters should only be changed if you have special effects in mind.</b>
    #	appendEstimSlider( "<b>Minimum parameter modulation time</b> in s. Parameter modulation time is randomized between these values. Parameter name is 'tma'", "tma" );
    #	appendEstimSlider( "<b>Maximum parameter modulation time</b> in s. Parameter name is 'tmb'", "tmb" );
    #	appendEstimSlider( "<b>log10 of phase shift frequency multiplier</b>. Parameter name is 'fps'", "fps" );
    <<	<b>These parameters are the low level parameters changed by the tease. This should be done using a high-level API because low level API may change.</b>
    #	appendEstimSlider( "<b>Volume</b> per group. Parameter name is 'vol1' for channel group 1, ... (For calibration, 'Channel balance' should be used.)", "vol");
    #	appendEstimSlider( "<b>Duty ratio of low frequency amplitude modulation</b>. Values smaller than 1 are used to tease and denial effects. Parameter name is 'drl'." , "drl");
    #	appendEstimSlider( "<b>Total limit</b>. Parameter name is 'tl'" , "tl");
    #	appendEstimSlider( "<b>Difficulty 1</b> per group. This setting has a higher impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'da1' four group 1, ..." , "da" );
    #	appendEstimSlider( "<b>Difficulty 2</b>. This setting has a smaller impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'db1' for channel gruop 1 ..." , "db" );
    <<	<h3>Modulation</h3>
    <<	<b>Some of the parameters can be varied which is called parameter modulation.</b>
    #	appendEstimSlider( "<b>Volume modulation</b> per group. Parameter name 'mvol1' for channel group 1, ..." , "mvol" );
    #	appendEstimSlider( "<b>Total limit modulation</b>. Parameter name is 'mtl'" , "mtl");
    #	appendEstimSlider( "<b>Difficulty 1 modulation</b> per group. Parameter name is 'mda1' for channel group 1, ..." , "mda");
    #	appendEstimSlider( "<b>Difficulty 2 modulation</b>. Parameter name is 'mdb', ..." , "mdb" );
    <<	<h2>Training</h2>
    <<  Click on the buttons to slowly increase various settings.
    <<	Volume training: @volTraining % per minute<br>Difficulty 1 training: @d1Training percent points per minute<br>Difficulty 2 training: @d2Training percent points per minute
    #	changeEstimParam( "vol" , @volTraining/(60*100) );
    #	changeEstimParam( "da" , @d1Training/(60*100) );
    #	changeEstimParam( "db" , @d2Training/(60*100) );
    TB  interrupt(); Back
    TBG  calib_vol Calibrate volume (I=0)
    TBG  calib_bal0 Calibrate balance and current reduction at common (I=0, tl=1)
    TBG  calib_bal Calibrate balance and current reduction at common (I=0.5, tl=1)
    TBG  test_i0 Test I=0.5, tl=0.5
    TBG  test_i1 Test I=1
    TBG  calib_sens Calibrate sensitivity (P=0.67, I=0.5)
    TBG  stopTraining Stop Training
    TBG  volTrain Increase volume training by 5% per minute
    TBG  d1Train Increase difficulty 1 training by 5 percent points per minute
    TBG  d2Train Increase difficulty 2 training by 10 percent points per minute
    D  
    #   formEnd();
    G   form_start

volTrain:
    #	@volTraining += 5;
    G	form2

d1Train:
    #	@d1Training += 5;
    G	form2

d2Train:
    #	@d2Training += 10;
    G	form2

// I=0
calib_vol:
    #	estimHL1_setI(0,1);
    #	estimHL1_setP(0,1);
    G	form2

// I=0.5, tl=1
calib_bal:
    #	setEstimParam( "tl", 1 );
    #	setEstimParam( "mtl", 0 );
    #	setEstimParam( "vol", estimHL1_volMin );
    #	setEstimParam( "mvol", 0 );
    #	estimHL1_setP(0,1);
    G	form2


// I=0.0, tl=1
calib_bal0:
    #	setEstimParam( "tl", 1 );
    #	setEstimParam( "mtl", 0 );
    #	setEstimParam( "vol", estimHL1_volMin*estimHL1_volMin );
    #	setEstimParam( "mvol", 0 );
    #	estimHL1_setP(0,1);
    G	form2


// I=0.5, tl=0
test_i0:
    #	setEstimParam( "tl", 0.5 );
    #	setEstimParam( "mtl", 0 );
    #	setEstimParam( "vol", 1 );
    #	setEstimParam( "mvol", 0 );
    #	estimHL1_setP(0,1);
    G	form2

// I=1
test_i1:
    #	estimHL1_setI(1,1);
    #	estimHL1_setP(0,1);
    G	form2

// 
calib_sens:
    #	setEstimParam( "tl", 1 );
    #	setEstimParam( "mtl", 0 );
    #	setEstimParam( "vol", estimHL1_volMin );
    #	setEstimParam( "mvol", 0 );
    #	setEstimParam( "da", 1 );
    #	setEstimParam( "mda", 0 );
    #	setEstimParam( "db", 0 );
    #	setEstimParam( "mdb", 0 );
    G	form2
