const scriptCode = {
    "code" : [
	"   scriptVars.imageDir = \"i\";		// default image directory\n   scriptVars.videoDir = 'v';		// default video directory\n   scriptVars.audioDir = \"a\";		// default audio directory\n   scriptVars.userVar = 1;			// This and the next lines are equivalent.\n   scriptVars.userVar = 1;         // User variables should be stored in the ScriptVars object such that they are (re)stored \n                                        // correctly if the session state is (re)stored\n",
	"   clearMedia();\n   rmButton('Restart');\n   addButton(\"setGoto('l01')\", 'Count lines');\n   addButton('interrupt();', 'Continue');\n   addButton('callReturn();', 'Return');\n   setText('Text 1');\n   { if ( await delay(10) ) return; }\n",
	"   appendText('Text 2');\n   { if ( await delay(10) ) return; }\n",
	"   scriptVars.target=\"l01\";		// checks whether label exists, colon will be removed automatically and label will be converted to lowercase\n   { setGoto(scriptVars.target); return; }			// JavaScript macro that can be used with variables\n   appendText('Text that never will be printed');\n",
	"   rmButton('Count lines');\n   addButton(\"setGoto('l00')\", 'Restart');\n   scriptVars.line = 0;			// initialize variable line\n   clearText('');\n",
	"   scriptVars.line++;			// increment variable 'line'\n   appendText('Line '+scriptVars.line+'');\n   { if ( await delay(3.3) ) return; }\n",
	"   addButton(\"setCall('memory')\", 'Memory game');\n   { setGoto('l02'); return; }\n\n// The memory game\n",
	"   rmButton('Memory game');\n   setText('What was the number of the last line ?');\n   if ( scriptVars.line != 3 ) 		// only print the button \"3\" if that is is a wrong answer\n   appendTButton(\"setGoto('wrong')\", '3');\n   appendTButton(\"callReturn();\",scriptVars.line);	// print the button with the correct answer which returns from the memory game\n   if ( scriptVars.line != 7 ) 		// only print the button \"7\" if that is is a wrong answer\n   appendTButton(\"setGoto('wrong')\", '7');\n   { if ( await delay(10000) ) return; }\n",
	"   { setGoto('memory'); return; }\n",
	"   { setCall('wrong2'); return; }\n",
	"   { setGoto('memory'); return; }\n",
	"   setText('That was wrong. Try it again!');\n   appendTButton(\"setCall('hint')\", 'Give me a hint');\n   { if ( await delay(5) ) return; }\n",
	"   { callReturn(); return; }\n",
	"   setText(\"1 + \" + (scriptVars.line-1) + \" = ?\");	// Set text\n   { if ( await delay(5) ) return; }\n",
	"   { callReturn(); return; }\n\n",
	"   scriptVars.imageDir=\"i\";\n   scriptVars.videoDir='v'\n   scriptVars.audioDir = \"a\";\n   { setGoto('l01'); return; }\n",
    ],
    "label" : [
	null,
	"l00",
	null,
	null,
	"l01",
	"l02",
	null,
	"memory",
	null,
	"wrong",
	null,
	"wrong2",
	null,
	"hint",
	null,
	"start2",
    ],
    "sourceFN" : [
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
	"demo01.gsc",
    ],
    "sourceLN" : [
	3,
	9,
	16,
	18,
	22,
	27,
	30,
	35,
	43,
	45,
	46,
	48,
	51,
	53,
	55,
	58,
    ]
};

const imageList = [
    { "dir" : ".", "files" : [ ] },
];

const videoList = [
    { "dir" : ".", "files" : [ ] },
];

const audioList = [
    { "dir" : ".", "files" : [ ] },
];
let baseDirectory=".";

