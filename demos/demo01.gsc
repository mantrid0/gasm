					// If you use media directories, they should be set in the initial code block such that
					// GAsm knows where to search for them and can emit warnings if they do not exist.
    #   @imageDir = "i";		// default image directory
    #   @videoDir = 'v';		// default video directory
    #   @audioDir = "a";		// default audio directory
    #   @userVar = 1;			// This and the next lines are equivalent.
    #   scriptVars.userVar = 1;         // User variables should be stored in the ScriptVars object such that they are (re)stored 
                                        // correctly if the session state is (re)stored
l00:
    I
    RB  Restart				// remove button "restart" (if it exists)
    BG  l01 Count lines			// define a goto button, goes to l01
    B   interrupt(); Continue		// define an action button which interrupts delay 
    B   callReturn(); Return		// define an action button returns from a subroutine or exits this demo
    <   Text 1				// start new text page and print text
    D   10				// delay 30s
    <<  Text 2				// append text
    D   10				// delay 10s
    #   @target=@label("L01:");		// checks whether label exists, colon will be removed automatically and label will be converted to lowercase
    #   @goto(@target);			// JavaScript macro that can be used with variables
    <<  Text that never will be printed // unreachable statement !
l01:
    RB  Count lines			// remove button "Count lines"
    BG  l00 Restart			// goto button, goes to l00
    #   @line = 0;			// initialize variable line
    <					// clears text
L02:					// label will be converted to lowercase
    #   @line++;			// increment variable 'line'
    <<  Line @line			// append text
    D   3.3				// delay 3.3s
    BC  memory Memory game		// call button to label memory with text "Memory game"
    G   l02				// goto l02

// The memory game
memory:					
    RB  Memory game			// remove button "Memory game" if it exists
    <   What was the number of the last line ?	// set text
    #   if ( @line != 3 ) 		// only print the button "3" if that is is a wrong answer
    TBG wrong 3			// print button "3"
    #   appendTButton("callReturn();",@line);	// print the button with the correct answer which returns from the memory game
    #   if ( @line != 7 ) 		// only print the button "7" if that is is a wrong answer
    TBG wrong 7			// print button "7"
    D	10000				// long delay
    G	memory				// re-run loop
wrong:					
    C	wrong2				// call "wrong2" ...
    G   memory				// ... and continue the memory came after exit
wrong2:					
    <   That was wrong. Try it again!	// Set text
    TBC hint Give me a hint		// Call button to target "hint" with the text "Give me a hint"
    D   5				// wait 5s
    R					// return from this subroutine
hint: 
    #   setText("1 + " + (@line-1) + " = ?");	// Set text
    D	5				// wait 5s
    R					// return from this subroutine

start2:					// alternative start label
    #   @imageDir="i";
    #   @videoDir='v'
    #   @audioDir = "a";
    G l01
