const scriptCode = {
    "code" : [
	"   formStart();					// starts form mode\n   scriptVars.channelConfig = encodeChannelConfig();		// converts current channel setup to configuration to a string\n   scriptVars.channelConfigErr = false;			// no error\n\n",
	"   estimStop();					// stop audio\n   setText('<h2>Device requirements and recommendations</h2>');\n   appendText(''+scriptVars.estimRequirements+'');\n   appendText('<h2>Channel setup</h2>');\n   appendText('Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and '+scriptVars.maxEstimChannels+'.');\n   appendText('E.g. \"1,1,1\" for 3 independent channels or \"2,1,1,\" for 4 channels where the first two ones have equal constraints.');\n	appendTextInput(\"channelConfig\");\n   appendText('Click on \\'Done\\' in order to start audio with a 5s ramp-up time.');\n   appendTButton('interrupt();;', 'Done');\n   if ( scriptVars.channelConfigErr ) appendText(\"Error: \" + scriptVars.channelConfigErr );\n   { if ( await delay() ) return; }\n",
	"\n// checks syntax of the channel configuration string and goto previos code block in an error occurred\n   scriptVars.channelConfigErr = await decodeChannelConfig( scriptVars.channelConfig );\n   if ( scriptVars.channelConfigErr ) { setGoto(\"channelConfig\"); return; }\n// forces to end code block in order to avoid printing the stuff below if an error occurred\n",
	"	scriptVars.volTraining = 0;\n	scriptVars.d1Training = 0;\n	scriptVars.d2Training = 0;\n\n",
	"   await estimPlay();				// start audio and initializes it if necessary \n   setText('<h2>EStim parameters</h2>');\n   appendText('<h3>Calibration parameters</h3>');\n   appendText('<b>These parameters are user dependent and should be set during calibration.</b> 	The software does not touch the mixer, i.e. both the mixer and the amp knob can be used to calibrate volume. 	It is recommended (but not required) to use equal volume levels for each channel and control the balance using the \"Channel balance\" setting that can be adjusted below.');\n	appendEstimSlider( \"<b>Carrier frequency</b> in Hz. Recommended: 400..800. Parameter name is 'fc'.\" , \"fc\" );\n	appendEstimSlider( \"<b>Channel balance</b> per group. Recommended: > 0.5. Smaller values feel less intense and softer, because that setting influences penetration depth. (It is o.k. to use the value 1 because this does not affect the volume.) Parameter name is 'cb1' for channel group 1, ...\" , \"cb\" );\n	appendEstimSlider( \"<b>Current limitation at common</b>. 0 means no limitation. 1 is the maximum feasible limitation and 2 is more or less (depending on output circuit and amplifier balance) equal to disconnecting common. Parameter name is 'ccl'.\" , \"ccl\" );\n	appendEstimSlider( \"<b>Sensitivity</b>. Chose 0.75..1 for electrodes at most sensitive places (e.g. glans) and 0..0.25 for electrodes at least sensitive places (e.g. prostate). Parameter name is 'sens1' for channel group 1, ...\" , \"sens\" );\n   appendText('<h3>Tease settings</h3>');\n   appendText('<b>These parameters should only be changed if you have special effects in mind.</b>');\n	appendEstimSlider( \"<b>Minimum parameter modulation time</b> in s. Parameter modulation time is randomized between these values. Parameter name is 'tma'\", \"tma\" );\n	appendEstimSlider( \"<b>Maximum parameter modulation time</b> in s. Parameter name is 'tmb'\", \"tmb\" );\n	appendEstimSlider( \"<b>log10 of phase shift frequency multiplier</b>. Parameter name is 'fps'\", \"fps\" );\n   appendText('<b>These parameters are the low level parameters changed by the tease. This should be done using a high-level API because low level API may change.</b>');\n	appendEstimSlider( \"<b>Volume</b> per group. Parameter name is 'vol1' for channel group 1, ... (For calibration, 'Channel balance' should be used.)\", \"vol\");\n	appendEstimSlider( \"<b>Duty ratio of low frequency amplitude modulation</b>. Values smaller than 1 are used to tease and denial effects. Parameter name is 'drl'.\" , \"drl\");\n	appendEstimSlider( \"<b>Total limit</b>. Parameter name is 'tl'\" , \"tl\");\n	appendEstimSlider( \"<b>Difficulty 1</b> per group. This setting has a higher impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'da1' four group 1, ...\" , \"da\" );\n	appendEstimSlider( \"<b>Difficulty 2</b>. This setting has a smaller impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'db1' for channel gruop 1 ...\" , \"db\" );\n   appendText('<h3>Modulation</h3>');\n   appendText('<b>Some of the parameters can be varied which is called parameter modulation.</b>');\n	appendEstimSlider( \"<b>Volume modulation</b> per group. Parameter name 'mvol1' for channel group 1, ...\" , \"mvol\" );\n	appendEstimSlider( \"<b>Total limit modulation</b>. Parameter name is 'mtl'\" , \"mtl\");\n	appendEstimSlider( \"<b>Difficulty 1 modulation</b> per group. Parameter name is 'mda1' for channel group 1, ...\" , \"mda\");\n	appendEstimSlider( \"<b>Difficulty 2 modulation</b>. Parameter name is 'mdb', ...\" , \"mdb\" );\n   appendText('<h2>Training</h2>');\n   appendText('Click on the buttons to slowly increase various settings.');\n   appendText('Volume training: '+scriptVars.volTraining+' % per minute<br>Difficulty 1 training: '+scriptVars.d1Training+' percent points per minute<br>Difficulty 2 training: '+scriptVars.d2Training+' percent points per minute');\n	changeEstimParam( \"vol\" , scriptVars.volTraining/(60*100) );\n	changeEstimParam( \"da\" , scriptVars.d1Training/(60*100) );\n	changeEstimParam( \"db\" , scriptVars.d2Training/(60*100) );\n   appendTButton('interrupt();;', 'Back');\n   appendTButton(\"setGoto('calib_vol')\", 'Calibrate volume (I=0)');\n   appendTButton(\"setGoto('calib_bal0')\", 'Calibrate balance and current reduction at common (I=0, tl=1)');\n   appendTButton(\"setGoto('calib_bal')\", 'Calibrate balance and current reduction at common (I=0.5, tl=1)');\n   appendTButton(\"setGoto('test_i0')\", 'Test I=0.5, tl=0.5');\n   appendTButton(\"setGoto('test_i1')\", 'Test I=1');\n   appendTButton(\"setGoto('calib_sens')\", 'Calibrate sensitivity (P=0.67, I=0.5)');\n   appendTButton(\"setGoto('stoptraining')\", 'Stop Training');\n   appendTButton(\"setGoto('voltrain')\", 'Increase volume training by 5% per minute');\n   appendTButton(\"setGoto('d1train')\", 'Increase difficulty 1 training by 5 percent points per minute');\n   appendTButton(\"setGoto('d2train')\", 'Increase difficulty 2 training by 10 percent points per minute');\n   { if ( await delay() ) return; }\n",
	"   formEnd();\n   { setGoto('form_start'); return; }\n\n",
	"	scriptVars.volTraining += 5;\n   { setGoto('form2'); return; }\n\n",
	"	scriptVars.d1Training += 5;\n   { setGoto('form2'); return; }\n\n",
	"	scriptVars.d2Training += 10;\n   { setGoto('form2'); return; }\n\n// I=0\n",
	"	estimHL1_setI(0,1);\n	estimHL1_setP(0,1);\n   { setGoto('form2'); return; }\n\n// I=0.5, tl=1\n",
	"	setEstimParam( \"tl\", 1 );\n	setEstimParam( \"mtl\", 0 );\n	setEstimParam( \"vol\", estimHL1_volMin );\n	setEstimParam( \"mvol\", 0 );\n	estimHL1_setP(0,1);\n   { setGoto('form2'); return; }\n\n\n// I=0.0, tl=1\n",
	"	setEstimParam( \"tl\", 1 );\n	setEstimParam( \"mtl\", 0 );\n	setEstimParam( \"vol\", estimHL1_volMin*estimHL1_volMin );\n	setEstimParam( \"mvol\", 0 );\n	estimHL1_setP(0,1);\n   { setGoto('form2'); return; }\n\n\n// I=0.5, tl=0\n",
	"	setEstimParam( \"tl\", 0.5 );\n	setEstimParam( \"mtl\", 0 );\n	setEstimParam( \"vol\", 1 );\n	setEstimParam( \"mvol\", 0 );\n	estimHL1_setP(0,1);\n   { setGoto('form2'); return; }\n\n// I=1\n",
	"	estimHL1_setI(1,1);\n	estimHL1_setP(0,1);\n   { setGoto('form2'); return; }\n\n// \n",
	"	setEstimParam( \"tl\", 1 );\n	setEstimParam( \"mtl\", 0 );\n	setEstimParam( \"vol\", estimHL1_volMin );\n	setEstimParam( \"mvol\", 0 );\n	setEstimParam( \"da\", 1 );\n	setEstimParam( \"mda\", 0 );\n	setEstimParam( \"db\", 0 );\n	setEstimParam( \"mdb\", 0 );\n   { setGoto('form2'); return; }\n",
    ],
    "label" : [
	"form_start",
	"channelconfig",
	null,
	"stoptraining",
	"form2",
	null,
	"voltrain",
	"d1train",
	"d2train",
	"calib_vol",
	"calib_bal",
	"calib_bal0",
	"test_i0",
	"test_i1",
	"calib_sens",
    ],
    "sourceFN" : [
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
	"estim01.gsc",
    ],
    "sourceLN" : [
	1,
	6,
	17,
	25,
	30,
	75,
	79,
	83,
	87,
	92,
	98,
	108,
	118,
	127,
	133,
    ]
};

const imageList = [
    { "dir" : ".", "files" : [ ] },
];

const videoList = [
    { "dir" : ".", "files" : [ ] },
];

const audioList = [
    { "dir" : ".", "files" : [ ] },
];
let baseDirectory=".";

