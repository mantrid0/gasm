.PHONY: all clean distclean

JARTARGETS=GAsm.jar
ESTIMV=5

JAVAC=$(JAVAC_PATH)javac -Xlint:deprecation
JAR=$(JAVAC_PATH)jar
CLASSPATH=$(PKGDIR):.

all : $(JARTARGETS) rtl/estim.js demos/estim01.gsc rtl/calib.gsc
	make -C demos
	make -C rtl/estim-dev

GAsm.jar: $(wildcard gasm/*.java)
	@echo $^
	$(JAVAC) $^
	$(JAR) cef gasm.GAsm $@ gasm/*.class

rtl/estim.js: rtl/estim-dev/estim.js rtl/estim-dev/estim$(ESTIMV).js rtl/estim-dev/processor$(ESTIMV).js
	rtl/estim.sh $(ESTIMV)

rtl/calib.gsc: rtl/estim-dev/calib$(ESTIMV).gsc
	cp $< $@

demos/estim01.gsc: rtl/estim-dev/Test$(ESTIMV).gsc
	cp $< $@
    
clean:
	rm -f */*.class 
	make -C demos distclean
	make -C rtl/estim-dev distclean
    
distclean: clean
	rm -f *.jar 
	rm -f rtl/estim.js demos/estim01.gsc
	make -C demos distclean
	make -C rtl/estim-dev distclean


