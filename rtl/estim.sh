#!/bin/bash

D=${0%/*}

if [ "$1" = "" ]; then
    echo -e  "Usage: $0 <estim version>\nExample: $0 4" 1>&2
    exit 1
fi

testexists() {
    if [ ! -f $1 ]; then
	echo "File not found: $1" 1>&2
	exit 2
    fi
}

js=$D/estim-dev/estim$1.js
jt=$D/estim-dev/estim.js
testexists $js
testexists $jt

while IFS="" read l; do
    a=${l#*audioWorklet.addModule(}
    if [ "$a" != "$l" ]; then
	b=${l%%audioWorklet.addModule(*}"audioWorklet.addModule("
	a=${a#*\"}
	c=${a#*\"}
	a=$D/estim-dev/${a%%\"*}
	testexists  $a
	echo "$b\`data:text/javascript;base64,"
	esbuild --minify $a | base64
	echo "\`$c";
    else 
	echo "$l"
    fi
done < $js > $D/estim.js
cat $jt >> $D/estim.js

