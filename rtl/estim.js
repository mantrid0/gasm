'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	// user settings
        fc: 700,	// carrier frequency
        cb: [],		// channel balance
        ccl: 0.5,	// current limitation at common
	sens: [],	// sensitivity
	// tease settings which should only be changed for special effects
        tma: 2,		// minimum parameter modulation time
        tmb: 5,		// maximum parameter modulation time
        fps: 0,		// log10 of phase shift frequency multiplier
	// tease settings controlled by high level API
        vol: [],        // volume
        drl: 1,		// low frequency duty ratio (rise interval counts as 0.5)
        tl: 0.6,        // total limit
        da: [],        	// difficulty 1 (controls high frequency amplitude modulation frequency )
        db: 0,       	// difficulty 2 (controls frequency of +/- 180° phase swaps)
        mvol: [],	// modulation of volume (absolute)
        mtl: 0,		// difficulty 1 modulation
        mda: [],	// difficulty 1 modulation
        mdb: 0,       	// difficulty 2 modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.cb[c] = 0.75;
        p.sens[c] = 0.5;
        p.vol[c] = 0.75;
        p.da[c] = 0;
        p.mvol[c] = 0;
        p.mda[c] = 0;
    }
}

/*function updateEstimDefaults ( n ) {
    const p = scriptVars.estimParams;
    for ( let c=1; c<=n; c++ ) 
	    p.sens[c] = (n-c+0.5)/n;
    syncEstimParams();
}*/

async function loadEstimProcessor() {
    return audioContext.audioWorklet.addModule(`data:text/javascript;base64,
bGV0IGluc3RhbmNlQ291bnQ9MDtjb25zdCBtYXhDaGFubmVscz04O2xldCBtdXRlVD0wLGR0Mj0w
O2NvbnN0IGZjbWluPTMwMCxybG09NTAqMi8zLHJpc2VUaW1lTWluPS41LHJpc2VUaW1lTWF4PTEu
NSxtdXRlUmFtcD0uNS9yaXNlVGltZU1heCxmUGhhc2VTaGlmdE1heD0yL3Jpc2VUaW1lTWluLHNl
dFRpbWVNYXg9LjUqcmlzZVRpbWVNaW4sZnBtYXg9MTVlMyx2Y2U9LjU7bGV0IHBoYXNlPTAscGhh
c2VQPTA7Y29uc3QgcGhhc2VTaGlmdD1bXSxmUGhhc2VTaGlmdD1bXSxwaGFzZVNoaWZ0Mj1bXSxt
YXhWb2w9W107bGV0IG1heFZvbEM9MDtjb25zdCBwaGFzZUE9W10sZlJBPVtdO2xldCBwaGFzZUI9
MCxmUkI9MS41LFRMPTA7Y29uc3QgTE40PU1hdGgubG9nKDQpLExOMj1NYXRoLmxvZygyKTtsZXQg
dGltZUg9MixmaG1pbj01MCxmaG1pbmRpdj0xMDtjb25zdCBmaG11bD1bXSxkcmgxPVtdLGRyaDI9
W10sYWgyPVtdLHZjZj1bXTtsZXQgY2htaW49MCx0aW1lTT0yLGZNPS4zO2NvbnN0IG12b2w9W10s
d2ZTaXplPTUxMix3ZkJ1Zj1bXTtmb3IobGV0IHg9MDt4PD13ZlNpemU7eCsrKXdmQnVmW3hdPU1h
dGguY29zKHgvd2ZTaXplKjIqTWF0aC5QSSk7Y2xhc3MgRXN0aW1Qcm9jZXNzb3IgZXh0ZW5kcyBB
dWRpb1dvcmtsZXRQcm9jZXNzb3J7aW5zdGFuY2VDb3VudD0wO2NvbnN0cnVjdG9yKHUpe3N1cGVy
KHUpLGluc3RhbmNlQ291bnQrKyx0aGlzLmluc3RhbmNlQ291bnQ9aW5zdGFuY2VDb3VudCxjb25z
b2xlLmxvZygiU3RhcnRlZCBFc3RpbSB3b3JrZXIgIyIrdGhpcy5pbnN0YW5jZUNvdW50KyIuIFNh
bXBsZSByYXRlOiAiK3NhbXBsZVJhdGUrIiBIeiIpLG11dGVUPWN1cnJlbnRUaW1lO2ZvcihsZXQg
ZT0wO2U8bWF4Q2hhbm5lbHM7ZSsrKXBoYXNlU2hpZnRbZV09TWF0aC5yYW5kb20oKSxmUGhhc2VT
aGlmdFtlXT1mUGhhc2VTaGlmdE1heCooMipNYXRoLnJhbmRvbSgpLTEpLHBoYXNlU2hpZnQyW2Vd
PTAsbWF4Vm9sW2VdPTAscGhhc2VBW2VdPTAsZlJBW2VdPTEvKHJpc2VUaW1lTWluK01hdGgucmFu
ZG9tKCkqKHJpc2VUaW1lTWF4LXJpc2VUaW1lTWluKSksZmhtdWxbZV09MSxkcmgxW2VdPS41LGRy
aDJbZV09LjUsYWgyW2VdPTAsdmNmW2VdPS41LG12b2xbZV09MH1zdGF0aWMgZ2V0IHBhcmFtZXRl
ckRlc2NyaXB0b3JzKCl7bGV0IHU9W3tuYW1lOiJjaGFubmVscyIsZGVmYXVsdFZhbHVlOjIsbWlu
VmFsdWU6MixtYXhWYWx1ZTo1LGF1dG9tYXRpb25SYXRlOiJrLXJhdGUifSx7bmFtZToibXV0ZSIs
ZGVmYXVsdFZhbHVlOjEsbWluVmFsdWU6MCxtYXhWYWx1ZToxLGF1dG9tYXRpb25SYXRlOiJrLXJh
dGUifSx7bmFtZToiZmMiLGRlZmF1bHRWYWx1ZTo0MDAsbWluVmFsdWU6ZmNtaW4sbWF4VmFsdWU6
MTUwMCxhdXRvbWF0aW9uUmF0ZToiay1yYXRlIn0se25hbWU6ImNjbCIsZGVmYXVsdFZhbHVlOjAs
bWluVmFsdWU6MCxtYXhWYWx1ZToyLGF1dG9tYXRpb25SYXRlOiJrLXJhdGUifSx7bmFtZToidG1h
IixkZWZhdWx0VmFsdWU6MixtaW5WYWx1ZTouNSxtYXhWYWx1ZToyMCxhdXRvbWF0aW9uUmF0ZToi
ay1yYXRlIn0se25hbWU6InRtYiIsZGVmYXVsdFZhbHVlOjUsbWluVmFsdWU6LjUsbWF4VmFsdWU6
MjAsYXV0b21hdGlvblJhdGU6ImstcmF0ZSJ9LHtuYW1lOiJmcHMiLGRlZmF1bHRWYWx1ZTowLG1p
blZhbHVlOi0xLG1heFZhbHVlOjEsYXV0b21hdGlvblJhdGU6ImstcmF0ZSJ9LHtuYW1lOiJkcmwi
LGRlZmF1bHRWYWx1ZToxLG1pblZhbHVlOi4wNSxtYXhWYWx1ZToxLGF1dG9tYXRpb25SYXRlOiJr
LXJhdGUifSx7bmFtZToidGwiLGRlZmF1bHRWYWx1ZTouNSxtaW5WYWx1ZTowLG1heFZhbHVlOjEs
YXV0b21hdGlvblJhdGU6ImstcmF0ZSJ9LHtuYW1lOiJkYiIsZGVmYXVsdFZhbHVlOjAsbWluVmFs
dWU6MCxtYXhWYWx1ZToxLGF1dG9tYXRpb25SYXRlOiJrLXJhdGUifSx7bmFtZToibXRsIixkZWZh
dWx0VmFsdWU6LjUsbWluVmFsdWU6LTEsbWF4VmFsdWU6MSxhdXRvbWF0aW9uUmF0ZToiay1yYXRl
In0se25hbWU6Im1kYiIsZGVmYXVsdFZhbHVlOjAsbWluVmFsdWU6LTEsbWF4VmFsdWU6MSxhdXRv
bWF0aW9uUmF0ZToiay1yYXRlIn1dO2ZvcihsZXQgZT0wO2U8PW1heENoYW5uZWxzO2UrKyl1W3Uu
bGVuZ3RoXT17bmFtZToiY2IiK2UsZGVmYXVsdFZhbHVlOjEsbWluVmFsdWU6MCxtYXhWYWx1ZTox
LGF1dG9tYXRpb25SYXRlOiJrLXJhdGUifSx1W3UubGVuZ3RoXT17bmFtZToic2VucyIrZSxkZWZh
dWx0VmFsdWU6LjUsbWluVmFsdWU6MCxtYXhWYWx1ZToxLGF1dG9tYXRpb25SYXRlOiJrLXJhdGUi
fSx1W3UubGVuZ3RoXT17bmFtZToidm9sIitlLGRlZmF1bHRWYWx1ZTouNSxtaW5WYWx1ZTowLG1h
eFZhbHVlOjEsYXV0b21hdGlvblJhdGU6ImstcmF0ZSJ9LHVbdS5sZW5ndGhdPXtuYW1lOiJkYSIr
ZSxkZWZhdWx0VmFsdWU6MCxtaW5WYWx1ZTowLG1heFZhbHVlOjEsYXV0b21hdGlvblJhdGU6Imst
cmF0ZSJ9LHVbdS5sZW5ndGhdPXtuYW1lOiJtdm9sIitlLGRlZmF1bHRWYWx1ZTowLG1pblZhbHVl
Oi0uNSxtYXhWYWx1ZTouNSxhdXRvbWF0aW9uUmF0ZToiay1yYXRlIn0sdVt1Lmxlbmd0aF09e25h
bWU6Im1kYSIrZSxkZWZhdWx0VmFsdWU6MCxtaW5WYWx1ZTotMSxtYXhWYWx1ZToxLGF1dG9tYXRp
b25SYXRlOiJrLXJhdGUifTtyZXR1cm4gdX1wcm9jZXNzKHUsZSxvKXtpZihpbnN0YW5jZUNvdW50
IT10aGlzLmluc3RhbmNlQ291bnQpcmV0dXJuIGNvbnNvbGUubG9nKCJUZXJtaW5hdGluZyBFc3Rp
bSB3b3JrZXIgIyIrdGhpcy5pbnN0YW5jZUNvdW50KSwhMTtjb25zdCByPWVbMF0sYz1NYXRoLm1p
bihtYXhDaGFubmVscyxNYXRoLm1pbihyLmxlbmd0aCxNYXRoLnJvdW5kKG8uY2hhbm5lbHNbMF0p
KSksdz1yWzBdLmxlbmd0aDtpZihvLm11dGVbMF0+LjUpe211dGVUPWN1cnJlbnRUaW1lO2Zvcihs
ZXQgbT0wO208ci5sZW5ndGg7bSsrKXtjb25zdCBNPXJbbV07Zm9yKGxldCBuPTA7bjx3O24rKylN
W25dPTB9cmV0dXJuITB9Y29uc3QgUz1bXTtmb3IobGV0IG09MDttPGM7bSsrKW12b2xbbV09TWF0
aC5taW4obXZvbFttXSxvWyJtdm9sIisobSsxKV1bMF0pLFNbbV09MipNYXRoLm1pbigxLE1hdGgu
bWF4KDAsb1siY2IiKyhtKzEpXVswXSkpLTE7Y29uc3QgcD1vLmZjWzBdLHE9LjUvTWF0aC5jZWls
KC41KnNhbXBsZVJhdGUvZnBtYXgpLFQ9by5kcmxbMF07VEw9TWF0aC5tYXgoTWF0aC5taW4ocGhh
c2VCLFRMKSwxLjUvTWF0aC5tYXgoLjAxLFQpLTIpO2NvbnN0IEU9VEwrMStNYXRoLm1heCgxLChU
LS41KS9NYXRoLm1heCgxZS0xMiwxLVQpKSxDPTEvc2FtcGxlUmF0ZTtmb3IobGV0IG09MDttPHc7
bSsrKXtpZihwaGFzZUIrPUMqZlJCLHBoYXNlQjw9VEwpe2ZvcihsZXQgbj0wO248ci5sZW5ndGg7
bisrKXJbbl1bbV09MDtjb250aW51ZX1pZihkdDIrPUMsTWF0aC5mbG9vcih0aW1lSCpmaG1pbmRp
dikhPU1hdGguZmxvb3IoKHRpbWVIK2R0MipmaG1pbikqZmhtaW5kaXYpKXtjb25zdCBuPU1hdGgu
bWluKDEsKGN1cnJlbnRUaW1lLW11dGVUKy41KmR0MikqbXV0ZVJhbXAscGhhc2VCLVRMKTtpZihw
aGFzZUI+RSYmKHBoYXNlQj0wLGZSQj0uNjUvKHJpc2VUaW1lTWluK01hdGgucmFuZG9tKCkqKHJp
c2VUaW1lTWF4LXJpc2VUaW1lTWluKSkpLHRpbWVNKz1kdDIqZk0sdGltZU0+PTEpe2NvbnN0IHQ9
TWF0aC5wb3coMTAsby5mcHNbMF0pKmZQaGFzZVNoaWZ0TWF4LGE9by50bWFbMF0sZj1vLnRtYlsw
XTtmb3IobGV0IGw9MDtsPGM7bCsrKWZQaGFzZVNoaWZ0W2xdPXQqKDIqTWF0aC5yYW5kb20oKS0x
KSxmTT0xLyhhK01hdGgucmFuZG9tKCkqKGYtYSkpLG12b2xbbF09b1sibXZvbCIrKGwrMSldWzBd
fXRpbWVNLT1NYXRoLmZsb29yKHRpbWVNKTtjb25zdCBzPS41KigxLXdmQnVmW01hdGgucm91bmQo
dGltZU0qd2ZTaXplKV0pLEE9TWF0aC5tYXgoMCxNYXRoLm1pbigxLG8uZGJbMF0rcypvLm1kYlsw
XSkpLEI9TWF0aC5tYXgoMCxNYXRoLm1pbigxLG8udGxbMF0rcypvLm10bFswXSkpLEg9W10sTD1b
XTtmb3IobGV0IHQ9MDt0PGM7dCsrKXtIW3RdPU1hdGgubWF4KDAsTWF0aC5taW4oMSxvWyJ2b2wi
Kyh0KzEpXVswXStzKm12b2xbdF0pKSxMW3RdPU1hdGgubWF4KDAsTWF0aC5taW4oMSxvWyJkYSIr
KHQrMSldWzBdK3Mqb1sibWRhIisodCsxKV1bMF0pKTtsZXQgYT1waGFzZVNoaWZ0W3RdK2ZQaGFz
ZVNoaWZ0W3RdKmR0MjthLT1NYXRoLmZsb29yKGEpLHBoYXNlU2hpZnRbdF09YX1jb25zdCB6PXRp
bWVIO2lmKHRpbWVIKz1kdDIqZmhtaW4sdGltZUg+Mil7dGltZUgtPTIqTWF0aC5mbG9vciguNSp0
aW1lSCk7Y29uc3QgdD1bXTtsZXQgYT0wLGY9MWUxMjtjaG1pbj0wO2ZvcihsZXQgaT0wO2k8Yztp
Kyspe2xldCBoPU1hdGguZXhwKExONCpMW2ldKTtjb25zdCBWPU1hdGgubWF4KDEsLjUqaCk7dmNm
W2ldPU1hdGguZXhwKChNYXRoLmxvZyhWKS1MTjIpKnZjZSksZHJoMVtpXT0uNS9WLGgqPXAvKHJs
bSooMStvWyJzZW5zIisoaSsxKV1bMF0pKSx0W2ldPWg7Y29uc3QgZz1NYXRoLmZsb29yKGgpO2g+
YSYmKGE9ZyxjaG1pbj1pKSxmPU1hdGgubWluKGYsZyl9Zj1NYXRoLnRydW5jKGYpLGE9TWF0aC50
cnVuYyhhKSsxZS02O2xldCBsPTI7Zm9yKDtmKmw8PWE7KWwqPTI7ZmhtaW5kaXY9LjUqTWF0aC5j
ZWlsKHRbY2htaW5dL2wpKmw7Y29uc3QgTj0xL2ZobWluZGl2O2ZobWluPXAvZmhtaW5kaXY7Zm9y
KGxldCBpPTA7aTxjO2krKyl7bGV0IGg9MTtjb25zdCBWPXRbaV07Zm9yKDtWKmg8ZmhtaW5kaXY7
KWgqPTI7ZmhtdWxbaV09aCxoKj1OO2NvbnN0IGc9ZHJoMVtpXSpWO2RyaDFbaV09TWF0aC5jZWls
KGcpKmgsZHJoMltpXT1NYXRoLmZsb29yKGcpKmgsYWgyW2ldPU1hdGguc3FydCgyLyhoKlYpLTEp
fX1sZXQgdj0wO21heFZvbEM9MDtsZXQgUD0wLFI9MCxrPTAsYj0wO2ZvcihsZXQgdD0wO3Q8Yzt0
Kyspe2xldCBhPS41KnRpbWVIKmZobXVsW3RdO2E9MiooYS1NYXRoLmZsb29yKGEpKTtsZXQgZj0w
O2E8ZHJoMVt0XT9mPTE6YT49MSYmYTw9MStkcmgyW3RdJiYoZj1haDJbdF0pLGY+Ljc1JiZNYXRo
LmZsb29yKHRpbWVIKmZobXVsW3RdKSE9TWF0aC5mbG9vcih6KmZobXVsW3RdKSYmdCE9Y2htaW4m
JihwaGFzZVNoaWZ0Mlt0XT1waGFzZVNoaWZ0Mlt0XT4wPy0uMjUqQTouMjUqQSksYT1mUkFbdF07
bGV0IGw9cGhhc2VBW3RdK2R0MiphO2w+YiYmKGI9bCxrPXQpLGw+PTE/KHBoYXNlQVt0XT1sLGw9
MSk6KGw8PTAmJihsPTAsYT0xLyhyaXNlVGltZU1pbitNYXRoLnJhbmRvbSgpKihyaXNlVGltZU1h
eC1yaXNlVGltZU1pbikpLGZSQVt0XT1hKSxQKz1hLHBoYXNlQVt0XT1sKSxSKz1sLGE9TWF0aC5z
cXJ0KEhbdF0qbip2Y2ZbdF0qbCksdj1NYXRoLm1heCh2LGEpLG1heFZvbEMrPWEsbWF4Vm9sW3Rd
PWEqZn1sZXQgZD1NYXRoLm1heCgwLE1hdGgubWluKDIsby5jY2xbMF0pKTtpZihtYXhWb2xDPWQ+
MT8oMi1kKSp2OigxLWQpKm1heFZvbEMrZCp2LFI+YypCJiZSPjFlLTEyKXtkPU1hdGguc3FydChj
KkIvTWF0aC5tYXgoMWUtMTAsUikpLFI9MDtmb3IobGV0IHQ9MDt0PGM7dCsrKW1heFZvbFt0XSo9
ZDttYXhWb2xDKj1kLGI+MCYmKGZSQVtrXT1QPC4wMT8tMS8ocmlzZVRpbWVNaW4rTWF0aC5yYW5k
b20oKSoocmlzZVRpbWVNYXgtcmlzZVRpbWVNaW4pKToocGhhc2VBW2tdPj0xPzA6ZlJBW2tdKS1Q
LHBoYXNlQVtrXT1NYXRoLm1pbihiLDEpKX1kdDI9MH1waGFzZSs9QypwLHBoYXNlLT1NYXRoLmZs
b29yKHBoYXNlKSxwaGFzZVArPXEscGhhc2VQLT1NYXRoLmZsb29yKHBoYXNlUCk7bGV0IE09MDtm
b3IobGV0IG49MDtuPGM7bisrKXtsZXQgcz1waGFzZStwaGFzZVNoaWZ0W25dK3BoYXNlU2hpZnQy
W25dO3M9d2ZCdWZbTWF0aC5yb3VuZCgocy1NYXRoLmZsb29yKHMpKSp3ZlNpemUpXSxzKj1tYXhW
b2xbbl0scGhhc2VQPi40OSYmKHMqPVNbbl0pLHJbbl1bbV09cyxNKz1zfWlmKE1hdGguYWJzKE0p
Pm1heFZvbEMpe009KE0+MD9NLW1heFZvbEM6TSttYXhWb2xDKS9jO2ZvcihsZXQgbj0wO248Yztu
KyspcltuXVttXS09TX1mb3IobGV0IG49YztuPHIubGVuZ3RoO24rKylyW25dW21dPTB9cmV0dXJu
ITB9fXJlZ2lzdGVyUHJvY2Vzc29yKCJlc3RpbS1wcm9jZXNzb3IiLEVzdGltUHJvY2Vzc29yKTsK
`);
}

const estimControlCommon = false;


// *****************************************************************************
// ******** processor depended parts of high level API 1 ***********************
// *****************************************************************************
const estimHL1_volMin = 0.7;

// generate parameter+weight array for intensity
// params: optional, default: scriptVars.estimParams;
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_readIPWA(params) {
    const gn = scriptVars.estimGroups.length;
    const weight_vol = 1.0/gn;
    const weight_tl = 1;
    const rv = 1 / (1-estimHL1_volMin);
    if ( (typeof(params) != "object") )
	params = scriptVars.estimParams;
    const pwa = [];
    pwa.push( (params.tl-0.6)*2.5, weight_tl, (params.tl+params.mtl-0.6)*2.5, weight_tl);
    for ( let g=1; g<=gn; g++ ) {
	const v = params.vol[g];
	const v0 = Math.max(0, v-estimHL1_volMin)*rv;
	const v1 = Math.max(0, v+params.mvol[g]-estimHL1_volMin)*rv;
	pwa.push( v0*v0, weight_vol, v1*v1, weight_vol);
    }
    estimHL1_normalizePWA(pwa);
    return pwa;
}

// priorize a single channel
// ch channel group (optional). First one: 1
// p  priorization. Default: 5, Should be >1
function estimHL1_getIPrio(ch, p) {
    if ( typeof(p) != "number" ) p=5;
    if ( typeof(ch) != "number" ) ch=-1;

    const prio = [];
    pwa.push( 1.0, 1.0);
    for ( let g=1; g<=gn; g++ ) {
	if ( g == ch ) pwa.push( p, p);
	else pwa.push( 1.0, 1.0);
    }
    prio;
}

// write parameter+weight array pwa for intensity
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_writeIPWA( pwa ) {
    const tl0 = pwa[0]*0.4 + 0.6;
    const tl1 = pwa[2]*0.4 + 0.6;
    setEstimParam( "tl", tl0 );
    setEstimParam( "mtl", tl1 - tl0 );
    for ( let g=1; g<=scriptVars.estimGroups.length; g++ ) {
	const v0 = Math.sqrt(pwa[g*4 + 0])*(1-estimHL1_volMin) + estimHL1_volMin;
	const v1 = Math.sqrt(pwa[g*4 + 2])*(1-estimHL1_volMin) + estimHL1_volMin;
	setEstimParam("vol"+g, v0);
	setEstimParam("mvol"+g, v1-v0);
    }
}

// generate parameter+weight array for pain
// params: optional, default: scriptVars.estimParams;
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_readPPWA(params) {
    const gn = scriptVars.estimGroups.length;
    const weight_da = 2.0/gn;
    const weight_db = 1;
    if ( (typeof(params) != "object") )
	params = scriptVars.estimParams;
    const pwa = [];
    pwa.push( params.db, weight_db, params.db+params.mdb, weight_db);
    for ( let g=1; g<=gn; g++ ) 
	pwa.push( params.da[g], weight_da, params.da[g]+params.mda[g], weight_da);
    estimHL1_normalizePWA(pwa);
    return pwa;
}

// priorize a single channel
// ch channel group (optional). First one: 1
// p  priorization. Default: 5, Should be >1
function estimHL1_getPPrio(ch, p) {
    if ( typeof(p) != "number" ) p=5;
    if ( typeof(ch) != "number" ) ch=-1;

    const prio = [];
    pwa.push( 1.0, 1.0);
    for ( let g=1; g<=gn; g++ ) {
	if ( g == ch ) pwa.push( p, p);
	else pwa.push( 1.0, 1.0);
    }
    prio;
}


// write parameter+weight array pwa for pain
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_writePPWA( pwa ) {
    setEstimParam( "db", pwa[0] );
    setEstimParam( "mdb", pwa[2] - pwa[0] );
    for ( let g=1; g<=scriptVars.estimGroups.length; g++ ) {
	setEstimParam("da"+g, pwa[g*4]);
	setEstimParam("mda"+g, pwa[g*4+2] - pwa[g*4] );
    }
}

/**
  * Get duty ratio. 
  * 0 mean silence. 1 means EStim without breaks. Small values can be used for denial games.
  * @return Current duty ratio.
  */
function estimHL1_getDR() {
    return scriptVars.estimParams.drl;
}

/**
  * Set duty ratio.
  * 0 mean silence. 1 means EStim without breaks. Small values can be used for denial games.
  * @return Current duty ratio.
  */
function estimHL1_setDR( val ) {
    if ( typeof(val) != "number" ) scriptError( "estimHL1_setDR: Argument must be a number");
    setEstimParam( "drl", Math.max(0, Math.min(1, val)) );
}
'use strict';

/*
 * estim-dev/estim.js is the development version of the worker-independent part of the API
 * It is used in combination with a worker-dependent part which loads the worker form a file. 
 * This may cause cross origin content errors if loaded from file system.
 * 
 * Content authors should use the auto-generated estim.js  which merges the development components
 * and loads the worker as data URL in order to avoid cross origin content errors.
 */

// must be equal to maxChannels in estim-processor.js
const estimMaxChannels = 7;	// maximum number of output channels. Currently audio API only defines up to 5 full frequency channels.

/**
  * Number of audio channels. Supported values are 2 to 5.
  */
scriptVars.estimChannels = 2;

/**
  * Number of channels per group. Sum of they array must be equal to {@link scriptVars.estimChannels}.
  */
scriptVars.estimGroups = [1,1];

/**
  * A HTML-formatted text that contains the Estim device requirements and electrode recommendation
  */
scriptVars.estimRequirements = "";

/**
  * Init Estim script variables.
  */
function initScriptVarsEstim () {
    if ( estimNode ) {
	estimStop();
	estimNode = null;
	estimMute = false;
    }
    scriptVars.estimChannels = 2;
    scriptVars.estimGroups = [1,1];
    scriptVars.maxEstimChannels = Math.min(estimMaxChannels, audioContext.destination.maxChannelCount);
    scriptVars.estimTrainingParams = {};
    scriptVars.estimRequirements = 
              "The Estim signals are generated dynamically and are configurable. More than 2 output channels are supported.\n"
	    + "<p>The Estim device must not re-modulate the signal and must be current controlled. (This is achieved by serial resistors in the output driver. Parallel resistors have the opposite effect.)\n"
	    + "<p>The output signal suppresses low-frequency components. However, (digital) output hardware stages can lead to low-frequency sampling artifacts due to the complexity of the signal.\n"
	    +    "Therefore, an analog high-pass filter (serial capacitor in the output driver) with a 50% cutoff limit around 100 Hz to 200 Hz is recommended.\n"
	    + "<p>The same pole of all channels must be is connected to the common electrode.";
    if ( ! estimControlCommon ) scriptVars.estimRequirements += "\n"+
	      "This electrode should be the largest one and should not be placed in sensitive areas as it is only indirectly controlled (unlike the non-common electrodes).";
    initScriptVarsEstimProcessor();
}

// *****************************************************************************
// ******* initialization and playback control *********************************
// *****************************************************************************
//const audioContext = new AudioContext( { latencyHint: "balanced", sampleRate: 11025 } );
const audioContext = new AudioContext( { latencyHint: "balanced", sampleRate: 48000 } );	// use default sample rate in order to avoid re-sampling artifacts

console.log("Audio processor latency: " + (1000.0*audioContext.outputLatency) + "ms");

let estimNode = null;

// mute parameter of estim processor
var estimMute = false;

/**
  * (Re-)Initializes the Estim audio processor. Number of channels is read from {@link scriptVars.estimChannels}.
  * There is no reason to call this function manually because this is done automatically by {@link estimPlay()}.
  */
function estimInit() {}	 // dummy for jsDoc
async function estimInit() {
    const n2 = 2 * Math.round(scriptVars.estimChannels*0.5+0.1);        // number of hardware channels
    
    try {
        audioContext.destination.channelCount = n2;

        if ( estimNode ) {
            if ( estimNode.channelCount == n2 ) {
                estimNode.parameters.get("channels").value = scriptVars.estimChannels;
                return;
            }
            estimStop();        // may have no effect if estimStart() is called quickly
        }
        else {
            await loadEstimProcessor();
        }

        estimNode = new AudioWorkletNode( audioContext, "estim-processor", { numberOfInputs: 0, numberOfOutputs: 1, outputChannelCount: [n2] } );
        estimNode.channelCount = n2;
        estimNode.channelCountMode = "explicit"; 
        estimNode.parameters.get("channels").value = scriptVars.estimChannels;
        // copy parameters
        estimMute = estimNode.parameters.get("mute");
        syncEstimParams();
        estimNode.connect(audioContext.destination);
        audioContext.resume();
    } catch ( e ) {
        scriptError( "Error starting EStim worker: " + e.message);
    }
}


/**
  * Starts Estim audio and initializes audio processor, if necessary
  */
function estimPlay() {}	 // dummy for jsDoc
async function estimPlay() {
    await estimInit();
    estimMute.value = 0;
}

/**
  * Check whether Estim is playing
  * @return Return true is Estim is playing.
  */
function estimIsPlaying() {
    try {
	return (estimMute) && (estimMute.value<=0.5);
    } catch ( e ) { }
    return false;
}

/**
  * Stops Estim audio
  */
function estimStop() {
    if ( estimMute ) estimMute.value = 1;
    scriptVars.estimTrainingParams = {};
}

// Called before a page is loaded in order to save EStim DOM content. This is required for correct DOM restoration after restoration of scriptVars
function backupDOMEStim() {
    scriptVars.backupDOM.estimStarted = Boolean(estimNode);
    scriptVars.backupDOM.estimMute = estimMute ? estimMute.value : 1.0;
}

// async part of adio restoration.  
async function restoreDOMEStimAsync( mute ) {
    await estimInit();
    estimMute.value = mute;
}


// Called after restoration of scriptVars and can be used for things like restoring DOM content.
function restoreDOMEStim() {
    let err = false;
    scriptVars.maxEstimChannels = Math.min(estimMaxChannels, audioContext.destination.maxChannelCount);
    if ( scriptVars.estimChannels > scriptVars.maxEstimChannels ) {
        scriptError( "Unable to restore audio from previous state because number of audio cahnnels is to large: " + scriptVars.estimChannels+ " > " + scriptVars.maxEstimChannels+ ". Loading default values.");
        initScriptVarsEstim();
    }
    if ( scriptVars.backupDOM.estimStarted ) {
	// ensure to load mute parameter before the async part starts
	const m = scriptVars.backupDOM.estimMute;
	console.log("Restoring EStim: mute=" + m);
	restoreDOMEStimAsync( scriptVars.backupDOM.estimMute );
    }
}

/** 
  * Restores Estim settings from a previous session.
  * @param o The object that is contain a <code>scriptVars</code> object.
  * @param v If truthy, also restores visited blocks
  */
function estimRestoreState(o, v) {
    if ( typeof(o) != "object" ) {
        scriptError( "estimRestoreState: First parameter must be an object");
	return;
    }
    if ( (typeof(o.estimChannels)=="number") && (o.estimChannels<=scriptVars.maxEstimChannels) ) {
	scriptVars.estimChannels = o.estimChannels;
	scriptVars.estimGroups = o.estimGroups;
	scriptVars.estimParams = o.estimParams;
        syncEstimParams();
    }
    else {
        scriptError( "Unable to restore audio setting because number of audio channels is invalid: " + o.estimChannels+ " > " + scriptVars.maxEstimChannels );
	
    }
    if ( v && ( typeof(blockVisited)=="array" ) ) scriptVars.blockVisited = o.blockVisited;
}


// *****************************************************************************
// ******* Estim parameter handling ********************************************
// *****************************************************************************
/**
  * Decodes a channel configuration string which consist in comma separated list of numbers of channels per group.
  * If audio processor was created, its properties are updated.
  * @return Returns false on success, Otherwise an error string.
  */
function decodeChannelConfig(cc) {
    const mc = Math.min(estimMaxChannels, audioContext.destination.maxChannelCount);
    let channels = 0;
    let groups = [];
    if ( typeof(cc) == "number" ) {
        channels = Math.round(cc);
        groups = [channels];
    }
    else if ( typeof(cc) == "string" ) {
        const vals = cc.split(",");
        for ( let i=0; i<vals.length; i++ ) {
            let v = Number(vals[i]);
            if ( isNaN(v) ) return "Number expected, got '" + vals[i] + "'";
            v = Math.round(v); 
            channels += v;
            groups[i] = v;
        }
    }
    else return "Comma separated list of numbers of channels per group expected";
    if ( (channels<2) || (channels>mc) ) return "Total number of channels must be between 2 and " + mc;

    const reinit = ( scriptVars.estimChannels != channels ) && estimNode;
    scriptVars.estimChannels = channels;
    scriptVars.estimGroups = groups;
    if ( reinit ) estimInit();
    
    return false;
}


/**
  * Encodes the channel configuration string, i.e. the function returns a comma separated list of numbers of channels per group
  */
function encodeChannelConfig() {
    if ( scriptVars.estimGroups.length < 1 ) return ""
    let r = scriptVars.estimGroups[0].toFixed(0);
    for ( let i=1; i<scriptVars.estimGroups.length; i++ )
        r += "," + scriptVars.estimGroups[i].toFixed(0);
    return r;
}


// set audio parameter and also updates the slider if found
// ids is the suffix of a slider id. if given, it's value is updated.
// returns the value clipped to min/max
function setAudioParam( ap, v, ids=false ) {
    let n = Number(v);
    if ( Number.isNaN(n) ) return n;
    n = Math.max(ap.minValue, Math.min(ap.maxValue, n));
    ap.value = n;
    if ( ! ids ) return n;
    const slider = document.getElementById("es_slider_"+ids);
    if ( slider ) slider.value = (n-ap.minValue)/(ap.maxValue-ap.minValue)*1000;
    const text = document.getElementById("es_span_"+ids);
    if ( text ) {
	let idx = Number( ids.substring(n.length-1) );
	if ( Number.isNaN(idx) ) idx = -1
	const l = ( idx=-1) ? "" : (idx==0) ? 'C: ' : (idx+': ');
	text.innerHTML = l + n.toPrecision(4).substring(0,10-l.length);
    } 
    return n;
}


// Synchronize EStim settings from scriptVars.estimParams
function syncEstimParams( n=false ) {
    if ( ! estimNode ) return;
    if ( n ) {
	const ep = scriptVars.estimParams[n];
	if ( Array.isArray(ep) ) {
	    const groups = scriptVars.estimGroups;
	    let c = 0;
    	    for ( let g=0; g<=groups.length; g++ ) {
    		if ( ep.length<=g ) ep[g] = ep[ep.length-1];
    		const gn = (g==0) ? 1 : groups[g-1];
    		for (let i=0; i<gn; i++ )
    		    scriptVars.estimParams[n][g] = setAudioParam(estimNode.parameters.get(n+(c+i)), ep[g]);
    		c+=gn;
    	    }
	}
	else {
    	    scriptVars.estimParams[n] = setAudioParam( estimNode.parameters.get(n), ep);
	}
    } 
    else {
	const keys = Object.keys(scriptVars.estimParams);
	for ( let i=0; i<keys.length; i++ ) 
	    syncEstimParams(keys[i]);
    }
}


/**
  * Set an EStim parameter
  * @param n Parameter name. If parameter can be set per electrode and if group index (0 for common) is omitted (e.g. 'dif' instead of 'dif1'), parameter is set for all electrodes.
  * @param v New parameter value.
  */
function setEstimParam( n, v ) {
    n = String(n);
    let idx = Number( n.substring(n.length-1) );
    if ( Number.isNaN(idx) ) idx = -1
    else n = n.substring(0,n.length-1);

    const vn = Number(v);
    if ( Number.isNaN(vn) ) {
        scriptError( "Parameter v of setEstimParam(n,v) is not a valid value: '" + v + "'");
        return;
    }

    var ep;
    try {
	ep = scriptVars.estimParams[n];
    } catch ( e ) {
        scriptError( "Parameter with name '" + n + "' does not exist");
        return;
    }
	
    if ( Array.isArray(ep) ) {
	const groups = scriptVars.estimGroups;
	let c = 0;
    	for ( let g=0; g<=groups.length; g++ ) {
    	    const gn = (g==0) ? 1 : groups[g-1];
    	    if ( (idx<0) || (g==idx) ) {
    		for (let i=0; i<gn; i++ )
    	    	    scriptVars.estimParams[n][g] = estimNode ? setAudioParam(estimNode.parameters.get(n+(c+i)), vn, n+g) : Number(vn);
    	    }
    	    c+=gn;
    	}
    }
    else {
	scriptVars.estimParams[n] = estimNode ? setAudioParam( estimNode.parameters.get(n), vn, n) : Number(vn);
    }
}


/**
  * Get an EStim parameter
  * @param n Parameter name. If parameter can be set per electrode, the group index (0 for common) must be included. (e.g. 'dif1' is allowed, 'dif' not)
  * @return The parameter value
  */
function getEstimParam( n ) {
    n = String(n);
    let idx = Number( n.substring(n.length-1) );
    if ( Number.isNaN(idx) ) idx = -1
    else n = n.substring(0,n.length-1);

    var ep;
    try {
	ep = scriptVars.estimParams[n];
    } catch ( e ) {
        scriptError( "Parameter with name '" + n + "' does not exist");
        return;
    }

    if ( Array.isArray(ep) ) {
        if ( (idx<0) || (idx>scriptVars.estimGroups.length) ) scriptError( "Group index of parameter '" + n + "' is not valid" );
        return ep[idx];
    }
    else return ep;
}


/**
  * Constantly changes an EStim parameter.
  * @param n Parameter name. If parameter can be set per electrode and if group number (0 for common) is omitted, change rate parameter is set for all electrodes.
  * @param v Change rate per second. Volume is changed relatively, all other parameters are changes absolutely. Default change rate is 0.
  */
function changeEstimParam( n, v=0 ) {
    n = String(n);
    let idx = Number( n.substring(n.length-1) );
    if ( Number.isNaN(idx) ) idx = -1
    else n = n.substring(0,n.length-1);

    const vn = Number(v);
    if ( Number.isNaN(vn) ) {
        scriptError( "Parameter v of changeEstimParam(n,v) is not a valid value: '" + v + "'");
        return;
    }

    var ep;
    try {
	ep = scriptVars.estimParams[n];
    } catch ( e ) {
        scriptError( "Parameter with name '" + n + "' does not exist");
        return;
    }

    const tp = scriptVars.estimTrainingParams;
    if ( Array.isArray(ep) ) {
	const groups = scriptVars.estimGroups;
	let c = 0;
    	for ( let g=0; g<=groups.length; g++ ) {
    	    const gn = (g==0) ? 1 : groups[g-1];
    	    if ( (idx<0) || (g==idx) ) {
    		if ( Math.abs(vn)<1e-12 ) delete tp[n+g];
    		else tp[n+g] = (n=="vol") ? getEstimParam(n+g)*vn : vn; 
    	    }
    	    c+=gn;
    	}
    }
    else {
    	if ( Math.abs(vn)<1e-12 ) delete tp[n];
    	else tp[n] = vn; 
    }
//    console.log(tp);
}


// parameter update interval in ms (for training)
const updateEstimParamInterval = 200;


// update Estim parameters 
function updateEstimParam( params ) {
    const tp = scriptVars.estimTrainingParams;
    if ( (typeof(tp) != "object") ) return;
    const keys = Object.keys(tp);
    for ( let i=0; i<keys.length; i++ ) {
	const n = keys[i];
//	console.log(n+": "+ getEstimParam(n) + " --> " + (getEstimParam(n)+tp[n]*updateEstimParamInterval*1e-3) );
	setEstimParam( n, getEstimParam(n)+tp[n]*updateEstimParamInterval*1e-3 );
    }
}

setInterval( updateEstimParam, updateEstimParamInterval );

/**
  * Produces a string containing all EStim parameters.
  * @param params Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return A string containing all EStim parameters.
  */
function estimParamStr ( params ) {
    if ( (typeof(params) != "object") )
	params = scriptVars.estimParams;
    const keys = Object.keys(params);
    let str = "";
    const g0 = estimControlCommon ? 0 : 1;
    for ( let i=0; i<keys.length; i++ ) {
	if ( i > 0 ) str += "\n";
	const n = keys[i];
	const v = params[n];
	str += n + ": ";
	if ( Array.isArray(v) ) {
    	    for ( let g=g0; g<=scriptVars.estimGroups.length; g++ ) {
    		const e = v[g];
    		if ( g > g0 ) str += ", ";
		if ( (typeof(e) == "number") ) str += e.toPrecision(4);
		else str += e;
    	    }
    	}
    	else {
	    if ( (typeof(v) == "number") ) str += v.toPrecision(4)
	    else str += v;
	}
    }
    return str;
}


// *****************************************************************************
// ******* Estim parameter slider **********************************************
// *****************************************************************************
// Called if the slider is moved
// apn: audio paramter name
// epn: estimParams name (key)
// epi: estimParams indo or -1 if not an array
function estimSliderInput(apn, epn,epi) {
    if ( !estimNode ) {
	console.log("Warning: estimSliderInput: EStim has not neeb started");
	return;
    }
    const l = ( epi==-1) ? "" : (epi==0) ? 'C: ' : (epi+': ');
    const p = estimNode.parameters.get(apn);
    const id = (epi>=0) ? epn+epi : epn;
    const slider = document.getElementById("es_slider_"+id);
    const val = p.minValue + slider.value*1e-3*(p.maxValue-p.minValue);
    document.getElementById("es_span_"+id).innerHTML = l + val.toPrecision(4).substring(0,10-l.length);
    if ( epi<0 ) scriptVars.estimParams[epn] = val;
    else scriptVars.estimParams[epn][epi] = val;
    syncEstimParams( epn );
}

// Creates the slider
// apn: audio parameter name
// epn: estimParams name (key)
// epi: estimParams index or -1 if not an array
function appendEstimSliderInt(apn, epn,epi) {
    if ( !estimNode ) {
	console.log("Warning: estimSliderInput: EStim has not neeb started");
	return;
    }
    const l = ( epi==-1) ? "" : (epi==0) ? 'C: ' : (epi+': ');
    const p = estimNode.parameters.get(apn);
    if ( !p ) throw 'EStim parameter "'+epn+'" not defined';
    const val = Math.round( (p.value-p.minValue)/(p.maxValue-p.minValue)*1000);
    const id = (epi>=0) ? epn+epi : epn;
    const html = '<span style="display:inline-block;width:7em;text-align:center;vertical-align:middle" id="es_span_' + id + '">' + l + p.value.toPrecision(4).substring(0,10-l.length) + '</span><input type="range" min="0" max="1000" value="' + val + '" step="1" oninput="estimSliderInput(\'' + apn + '\',\'' + epn + '\',' + epi + ');" id="es_slider_' + id + '" style="width:calc(100% - 10em - 8px);vertical-align:middle">';
    return html;
}

/**
  * Appends a slider for an EStim parameter to text area. If EStim is played, parameter is changed interactively.
  * @param text Description text
  * @param param Parameter name.
  */
function appendEstimSlider(text, param) {
    finishPage();
    let html = "" + text;
    const ep = scriptVars.estimParams[param];
    if ( Array.isArray(ep) ) {
        let c=1;
        const groups = scriptVars.estimGroups;
        for ( let g=1; g<=groups.length; g++ ) {
	    if ( html.length>0 ) html += "<br>";
            html += appendEstimSliderInt(param+c, param,g);
            c+=groups[g-1];
        }
        if ( estimControlCommon ) {
	    if ( html.length>0 ) html += "<br>";
    	    html += appendEstimSliderInt(param+"0", param,0);
    	}
    }
    else {
	if ( html.length>0 ) html += "<br>";
        html += appendEstimSliderInt(param, param,-1);
    }
    appendText(html);

}


// *****************************************************************************
// ******** processor independent parts of high level API 1 ********************
// *****************************************************************************
// clip values and normalize weight to 1
function estimHL1_normalizePWA(pwa) {
    let s = 0;
    for ( let i=1; i<pwa.length; i+=2 ) {
	pwa[i-1] = Math.max(0, Math.min(1, pwa[i-1]));
	s += pwa[i];
    }
    s = 1/s;
    for ( let i=1; i<pwa.length; i+=2 ) 
	pwa[i] *= s;
}

// Set parameters.Ffor internal use, see estimHL1_setI() and estimHL1_setP()
// pwa: parameter+weight array
// prio: priority factors (recommended: >0)
// ran: randomization (0..1)
// val: new value (0..1)
function estimHL1_set(pwa, prio, val, ran ) {
    ran = Math.max(0, Math.min(1, ran));
    val = Math.max(0, Math.min(1, val));
    if ( ran > 1e-3 ) 
	for ( let i=1; i<pwa.length; i+=2 ) 
	    pwa[i-1] = pwa[i-1]*(1-ran) + (
	    (pwa[i-1]*(1-ran)+Math.random()*ran)*2-0.5)*ran;

    let r = -val;
    let s = 0;
    for ( let i=0; i*2+1<pwa.length; i++ ) {
	const w = pwa[i*2+1];
	r += Math.max(0,Math.min(1,pwa[i*2]))*w;
	s += prio[i]*w;
    }
    while ( (Math.abs(r)>1e-12) && (s>0) ) {
	const t = r/s;	
	r = -val;
	s = 0;
	for ( let i=0; i*2+1<pwa.length; i++ ) {
	    const u = pwa[i*2] - prio[i]*t;
	    const w = pwa[i*2+1];
	    if ( ((t>0) && (u>0)) || ((t<0) && (u<1)) )
		s += prio[i]*w;
	    pwa[i*2] = u;
	    r += Math.max(0,Math.min(1,u))*w;
	} 
    }
    for ( let i=1; i<pwa.length; i+=2 ) 
	pwa[i-1] = Math.max(0,Math.min(1,pwa[i-1]));
}

/**
  * Set intensity level. Range is from 0 to 1.
  * @param val New insensitivity value 
  * @param ran Randomization of current settings (0..1). Optional. Default: 1
  */
function estimHL1_setI(val, ran, prio) {
    if ( typeof(val) != "number" ) scriptError( "estimHL1_setI: 1st Argument must be a number");
    if ( typeof(ran) != "number" ) ran = 1;
   // TODO: select channels 
    const pwa = estimHL1_readIPWA();
    if ( ! Array.isArray(prio) ) {
	prio = [];
	for ( let i=0; i*2+1<pwa.length; i++ )
	    prio[i] = 1;
    }
    estimHL1_set(pwa, prio, val, ran );
    estimHL1_writeIPWA(pwa);
}

/**
  * Calculate intensity level.
  * @param param Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return Current intensity level.
  */
function estimHL1_getI( params ) {
    const pwa = estimHL1_readIPWA( params );
    let r = 0;
    for ( let i=1; i<pwa.length; i+=2 )
	r += pwa[i-1]*pwa[i];
    return r;
}

/**
  * Set pain level. Range is from 0 to 1.
  * @param val New pain value 
  * @param ran Randomization of current settings (0..1). Optional. Default: 1
  */
function estimHL1_setP(val, ran, prio) {
    if ( typeof(val) != "number" ) scriptError( "estimHL1_setI: 1st Argument must be a number");
    if ( typeof(ran) != "number" ) ran = 1;
   // TODO: select channels 
    const pwa = estimHL1_readPPWA();
    if ( ! Array.isArray(prio) ) {
	prio = [];
	for ( let i=0; i*2+1<pwa.length; i++ )
	    prio[i] = 1;
    }
    estimHL1_set(pwa, prio, val, ran );
    estimHL1_writePPWA(pwa);
}

/**
  * Calculate pain level.
  * @param param Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return Current pain level.
  */
function estimHL1_getP( params ) {
    const pwa = estimHL1_readPPWA( params );
    let r = 0;
    for ( let i=1; i<pwa.length; i+=2 )
	r += pwa[i-1]*pwa[i];
    return r;
}

/**
  * Produces a string containing intensity and pain level and all Estim parameters.
  * @param param Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return A string containing all EStim parameters.
  */
function estimHL1_toStr( params ) {
    return "intensity=" + estimHL1_getI(params).toPrecision(4) + "  pain=" + estimHL1_getP(params).toPrecision(4) + "\n" + estimParamStr ( params );
}
