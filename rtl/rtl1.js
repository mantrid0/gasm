/** @file  rtl1.js
  * This is the high level part of the gasm run time library.
  */

'use strict';

/* todo:
    * timer style
*/    

// add content div's
document.body.innerHTML +=
    '<div id="buttons"></div>' + 
    '<div id="media"></div>' + 
    '<div id="text"></div>' + 
    '<div id="timer"></div>' + 
    '<div id="debug"></div>';


/**
  * In form mode text and media area are swapped. Should only be changed using {@link formStart()} and {@link formEnd()}.
  */
scriptVars.formMode = false;

/** 
  * Directory where to search for images.
  */
scriptVars.imageDir = ".";

/** 
  * Directory where to search for videos.
  */
scriptVars.videoDir = ".";

/** 
  * Directory where to search for audio files.
  */
scriptVars.audioDir = ".";

/** 
  * Current image or empty string if no image is displayed.
  */
scriptVars.imageSrc = "";

/**
  * Current video file or empty string if no video is played.
  */
scriptVars.videoSrc = "";

/**
  * Whether to play video muted.
  */
scriptVars.videoMuted = false;

/**
  * Start time of current video.
  */
scriptVars.videoStartTime = "";

/**
  * Current audio file or empty string if no sound is played.
  */
scriptVars.audioSrc = "";

/**
  * Start time of current audio file
  */
scriptVars.audioStartTime = "";

/**
  * Whether to show a timer.
  */
scriptVars.showTimer = true;

/**
  * Set it to true in order to update buttons 
  */
var updateButtonsRequired = false;

/**
  * Initialize user script variables. This function is empty by default and can be overwritten by user.
  */
function initScriptVars () {
}

/*
 * Init EStim script variables. This will be overwritten by EStim API.
 */
function initScriptVarsEstim () {
}


// Init high level RTL script variables. Called by initScriptVars0.
function initScriptVars1 () {
    scriptVars.formMode = false;
    scriptVars.imageDir = ".";
    scriptVars.videoDir = ".";
    scriptVars.audioDir = ".";
    scriptVars.imageSrc = "";
    scriptVars.videoSrc = "";
    scriptVars.videoMuted = false;
    scriptVars.audioSrc = "";
    scriptVars.showTimer = true;
    if ( typeof(baseDirectory) === 'undefined' ) window.baseDirectory = ".";
    else if ( (typeof(baseDirectory) != 'string') ) baseDirectory = ".";
    scriptVars.buttons = {
	actions : [],
	names : [],
    };
    scriptVars.backupDOM = {
	text : "",
	media : "",
	audio : ""
    };
    initScriptVarsEstim();
    // user defines script variables
    initScriptVars();
    updateButtonsRequired = true;
}


// *****************************************************************************
// ******* DOM *****************************************************************
// *****************************************************************************
/** 
  * Convenience function to change the content of a HTML element, usually a <code>&lt;div&gt;</code>.
  * @param id ID if the element (if it is a string
  * @param HTML Contents of the element in HTML format
  */
function setHtml(id,html) {
    document.getElementById(id).innerHTML=html; 
}

/** 
  * Convenience function to append the content to a HTML element, usually a <code>&lt;div&gt;</code>.
  * @param id ID if the element
  * @param HTML Contents of the element in HTML format
  */
function appendHtml(id,html) {
    document.getElementById(id).innerHTML+=html; 
}

/**
  * Called before a page is loaded in order to save DOM content. This is required for correct DOM restoration after restoration of {@link scriptVars}
  * This function is empty by default and can be overwritten by user. 
  */
function backupDOM() {
}

/**
  * Called before a page is loaded in order to save EStim DOM content. This is required for correct DOM restoration after restoration of {@link scriptVars}
  * This function is empty by default and will be overwritten by EStim API.
  */
function backupDOMEStim() {
}

function backupDOM1() {
    try {
	if ( audioStarted && scriptVars.audioSrc ) scriptVars.audioStartTime = audioElement.currentTime;
    }
    catch(err) {
    }
    try {
	if ( videoStarted && scriptVars.videoSrc ) scriptVars.videoStartTime = document.getElementById("mediavid").currentTime;
    }
    catch(err) {
    }
    scriptVars.backupDOM.text    = document.getElementById("text").innerHTML;
    scriptVars.backupDOM.media   = document.getElementById("media").innerHTML;
    scriptVars.backupDOM.audio   = scriptVars.audioSrc;
    backupDOMEStim();
    backupDOM();
}

/**
  * Called after restoration of {@link scriptVars} and can be used for things like restoring DOM content.
  * This function if empty by default and can be overwritten by user. 
  */
function restoreDOM() {
}

/**
  * Called after restoration of {@link scriptVars} in order to restore EStim DOM content.
  * This function is empty by default and will be overwritten by EStim API.
  */
function restoreDOMEStim() {
}

function restoreDOM1() {
    videoStarted = false;
    audioStarted = false;
    document.getElementById("text").innerHTML    = scriptVars.backupDOM.text;
    document.getElementById("media").innerHTML   = scriptVars.backupDOM.media;
    const audio = scriptVars.backupDOM.audio;
    if ( audio.length>0 ) playAudio(audio, scriptVars.audioStartTime);
    else stopAudio();
    restoreDOMEStim();
    restoreDOM();
}


// Called by delay and is used to print buffered text buttons
function finishPage() {
    printTextButtons();
}

// *****************************************************************************
// ******* media ***************************************************************
// *****************************************************************************
/** 
  * Clears the media area.
  */
function clearMedia() {
    setHtml(scriptVars.formMode ? 'text' : 'media', '');
    scriptVars.imageSrc = "";
    scriptVars.videoSrc = "";
    scriptVars.backupDOM.media = "";	// avoids restoring media in current code block
}    


/** 
  * Draws a picture into the media area.
  * @param src File name of the image.
  */
function setImage(src) {
    scriptVars.imageSrc = baseDirectory+"/"+scriptVars.imageDir+'/'+src;
    scriptVars.videoSrc = "";
    console.log("Drawing image '"+scriptVars.imageSrc+"'");
    setHtml(scriptVars.formMode ? 'text' : 'media', '<img src="'+scriptVars.imageSrc+'" id="mediaimg">'); 
    scriptVars.backupDOM.media = "";	// avoids restoring in current code block
}


var videoStarted = false;

function videoError( err ) {
    videoStarted = true;
    scriptError(err);
}

async function videoStart() {
    const ve = document.getElementById("mediavid");
    if ( videoStarted ) return false;
    console.log("Loaded video '"+scriptVars.videoSrc + "', duration: " + ve.duration.toFixed(1)+"s, starting at " + scriptVars.videoStartTime.toFixed(1) + "s, muted:" + scriptVars.videoMuted );
    if ( scriptVars.videoStartTime > ve.duration+0.1 ) console.log("Warning: Video start time past video end");
    ve.currentTime = Math.min(scriptVars.videoStartTime, ve.duration);
    videoStarted = true;
    try {
        await ve.play();
//	    console.log("video started");
    }
    catch(err) {
        videoError(err);
    }
}

/** 
  * Plays a video into the media area.
  * @param src File name of the video.
  * @param startTime start time in seconds of the video (optional)
  */
function playVideo(src, startTime ) {
    scriptVars.videoSrc = baseDirectory+"/"+scriptVars.videoDir+'/'+src;
    scriptVars.imageSrc = "";
    if ( (typeof(startTime) != 'number') || (startTime<0) ) startTime = 0;
    scriptVars.videoStartTime = startTime;
    setHtml(scriptVars.formMode ? 'text' : 'media','<video id="mediavid"' + (scriptVars.videoMuted ? ' muted' : '' ) + ' oncanplay="videoStart()" oncanplaythrough="videoStart()" onerror="videoError(\'Cannot load video \\\''+src+'\\\'">'+
    '<source src="'+scriptVars.videoSrc+'">'+
    'Your browser does not support HTML5 video.'+
    ' </video>');
    scriptVars.backupDOM.media = "";	// avoids restoring media in current code block
    videoStarted = false;
}    

/** 
  * Returns remaining play time in seconds of current video, NaN if video has not started yet or 0 if an error occurred.
  */
function videoRemaining() {
    try {
	if ( videoStarted && scriptVars.videoSrc ) {
	    const ve = document.getElementById("mediavid");
	    const r = ve.duration - ve.currentTime;
	    return isNaN(r) ? 0 : r;
	}
    }
    catch(err) {
    }
    return NaN;
}


// *****************************************************************************
// ******* sound ***************************************************************
// *****************************************************************************
/**
  * HTMLAudioElement which should be initialized by {@link getAudioElement()}
  */
var audioElement = null;
var audioStarted = false;

/**
  * Stops playing sound.
  */
function stopAudio() {
    if ( audioElement ) audioElement.pause();
    scriptVars.audioSrc = "";
    scriptVars.backupDOM.audio = "";			// avoids restoring audio in current code block
}

function audioError( err ) {
    audioStarted = true;
    scriptError(err);
}

async function audioStart() {
    if ( audioStarted ) return false;
    console.log("Loaded audio file '"+scriptVars.audioSrc + "', duration: " + audioElement.duration.toFixed(1)+"s, starting at " + scriptVars.audioStartTime.toFixed(1) +"s" );
    if ( scriptVars.audioStartTime > audioElement.duration+0.1 ) console.log("Warning: Audio start time past audio end time");
    audioElement.currentTime = Math.min(scriptVars.audioStartTime, audioElement.duration);
    audioStarted = true;
    try {
	await audioElement.play();
	console.log("audio started");
    }
    catch(err) {
	audioError(err);
    }
}

/**
  * Returns the audio element and creates it if necessary.
  */
function getAudioElement() {
    if ( !audioElement ) {
        console.log("Creating new audio element");
        audioElement = new Audio();
        if ( typeof(scriptVars.audioVolume) == 'number' )
            audioElement.volume = scriptVars.audioVolume;
	audioElement.addEventListener("canplay", audioStart);
	audioElement.addEventListener("canplaythrough", audioStart);
	audioElement.addEventListener("error", function() { audioError("Cannot load audio file '"+scriptVars.audioSrc+"'") } );
    }
    return audioElement;
}

/**
  * Starts playing sound.
  * @param src Name of the sound file.
  * @param startTime start time in seconds of the audio file (optional)
  */
function playAudio(src,startTime) {
    console.log('pa:'+src);
    scriptVars.backupDOM.audio = "";			// avoids restoring audio in current code block
    scriptVars.audioSrc = baseDirectory+"/"+scriptVars.audioDir+'/'+src;
    if ( (typeof(startTime) != 'number') || (startTime<0) ) startTime = 0;
    scriptVars.audioStartTime = startTime;
    console.log("Play audio file '" + scriptVars.audioSrc + "'");
    getAudioElement();
    audioElement.preload = "auto";
    audioElement.controls = false;
    audioElement.autoplay = false;
    audioStarted = false;
    audioElement.src = scriptVars.audioSrc;
}

/**
  * Sets the audio volume.
  * @param vol Audio volume
  */
function audioVolume(vol) {
    if ( audioElement ) audioElement.volume = vol;
    console.log("Set volume to " + vol);
    scriptVars.audioVolume = vol;
}

/** 
  * Returns remaining play time in seconds of current sound file. This function return NaN if no audio is playing
  */
function audioRemaining() {
    try {
	if ( audioElement ) return audioElement.duration - audioElement.currentTime;
    }
    catch(err) {
    }
    return NaN;
}


// *****************************************************************************
// ******* text ****************************************************************
// *****************************************************************************
let textButtons = "";

// Prints the text buttons
function printTextButtons() {
    if ( textButtons.length<1 ) return;
    const e = document.getElementById(scriptVars.formMode ? 'media' : 'text');
    document.getElementById(scriptVars.formMode ? 'media' : 'text').innerHTML += '<p align="center" style="line-height:2.5">' + textButtons + '</p>';
    textButtons = "";
    e.scrollTop = e.scrollHeight;
}

/** 
  * Clears the text area.
  */
function clearText() {
    setHtml(scriptVars.formMode ? 'media' : 'text', '');
    textButtons = "";
    scriptVars.backupDOM.text = "";		// avoids restoring text in current code block
}


/** 
  * Set the contents of the text area.
  * @param text HTML formatted text which will be shown in text area.
  */
function setText(text) {
    setHtml(scriptVars.formMode ? 'media' : 'text', text);
    textButtons = "";
    scriptVars.backupDOM.text = "";		// avoids restoring text in current code block
}


/** 
  * Appends text to the text section.
  * @param text HTML formatted text which will be appended.
  */
function appendText(text) {
    printTextButtons();
    const e = document.getElementById(scriptVars.formMode ? 'media' : 'text');
    const t = e.innerHTML;
    let tt = String(text).trim();
    if ( tt.length < 1 ) {
	if ( t.length < 1 ) return;
	tt = "&nbsp;";
    }
    e.innerHTML = ( t.length>0 ) ? t + "<p>" + tt : tt; 
    e.scrollTop = e.scrollHeight;
//    console.log("->"+tt+"<-" + tButtonNewline);
}


/**
  * Add a button in text area.
  * @param action {String} action One or more commands which will be executed if button is pressed.
  * @param Name The button text.
  */
function appendTButton(action,name) {
    if ( textButtons.length > 0 ) textButtons+='&nbsp;&nbsp';
    textButtons+='<button onclick="' + action + '">'+name+'</button>';
}

/**
  * Add a button in text area which allows to load a JSON object from file.
  * @param {String} cb A function as string that is called with the loaded JSON object as argument.
  * @param label A label of the button
  * @return HTML snippet which defines the button.
  */
function apendFileReadTButton(cb, label) {
    if ( typeof(cb) != "string" ) {
	scriptError("First argument of <code>appendFileTButton()</code> must be function as string.");
	return;
    }
    if ( textButtons.length > 0 ) textButtons+='&nbsp;&nbsp';
    textButtons += '<button><label><input type="file" onchange="readStateFromFile(this.files,' + cb + ')" accept="text/json">' + label + '</label></button>';
}


// *****************************************************************************
// ******* buttons *************************************************************
// *****************************************************************************
// called every 100ms and updates buttons if updateButtonsRequired is true
function updateButtons() {
    if ( !updateButtonsRequired ) return;
    var text = ""
    for (var i=0; i<scriptVars.buttons.actions.length; i++) {
	var name = scriptVars.buttons.names[i];
	var action = scriptVars.buttons.actions[i];
	if ( typeof(action) == 'string' ) text+='&nbsp;<button onclick="' + action + '">'+name+'</button>';
    }
    setHtml('buttons',text);
    updateButtonsRequired = false;
}


setInterval(updateButtons, 100);

// search for button with that name
function findButton(name) {
    var lname = (""+name).toLowerCase();
    var l = scriptVars.buttons.actions.length;
    for ( var i=0; i<l; i++ ) 
	if ( scriptVars.buttons.names[i].toLowerCase() == lname ) return i;
    return -1;
}

/**
  * Add a button.
  * @param {String} action One or more commands which will be executed if button is pressed.
  * @param Name The button text.
  */
function addButton(action,name) {
    var i = findButton(name);
    if ( i < 0 ) {
	for ( i=0; i<scriptVars.buttons.actions.length; i++ ) 
	    if ( typeof(scriptVars.buttons.actions[i]) != 'string') break;
    }
    scriptVars.buttons.names[i] = name;
    scriptVars.buttons.actions[i] = action;
    updateButtonsRequired = true;
}

/**
  * Removes a button.
  * The button is identified by its name. Case is ignored.
  * @param name The button text.
  */
function rmButton(name) {
    var i = findButton(name);
    if (i >=0 )  {
	scriptVars.buttons.actions[i] = undefined;
	scriptVars.buttons.names[i] = '';
    }
    updateButtonsRequired = true;
}


// *****************************************************************************
// ******* timer / debug / idle ************************************************
// *****************************************************************************
/**
  * Function called by {@link delay()} approximately every 250ms. This function can be overwritten by user.
  * By default, it prints the remaining wait time if {@link scriptVars.showTimer} or {@link enableDebug} is truthy.
  * @param {number} remaining Remaining wait time in seconds.
  */
function delayIdle(remaining) {
    setTimerContent(  ((remaining<=999) && (scriptVars.showTimer || enableDebug ) ) ? Math.round(remaining+0.499).toString() : "" );
}

/**
  * Function called by start of a {@link delay()}. This function can be overwritten by user. 
  * By default it prints, debug information and a continue button if {@link enableDebug} is truthy and buttons for saving/restoring the state in any case.
  */
function delayStart(remaining) {
    try {
	const html = document.getElementById("debug"); 
	let text = "";
	if ( enableDebug ) {
	    const idx = currentBlock();
	    const l = scriptCode.label[idx];
	    text = (l ? (l+":, ") : (scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+"), " ) ) + ' ES: '+ scriptVars.nextBlock.length + '<br><button class="debug" onclick="interrupt();">Continue</button>&nbsp;&nbsp;';
	}
	else {
	    text = '<br>'
	}
	html.innerHTML = text + '<a href="data:text/plain;charset=utf-8,'+encodeURIComponent(JSON.stringify(scriptVars))+'" download="' + stateName + '.json"><button class="debug">Save state</button></a>'
	    + '&nbsp;&nbsp;<button class="debug"><label><input type="file" class="debug" onchange="readStateFromFile(this.files)" accept="text/json">Restore state</label></button>';
    }
    catch(err) {
	// silently ignore errors (if non-default layout is used)
    }
}


var timerText = "";
/**
  * Sets the content of the timer element. DOM if only modified it the text changes.
  * @param t Content for the timer element.
  */
function setTimerContent(t) {
    if ( t != timerText ) {  // only update DOM if tax has been changed
	timerText = t;
	setHtml("timer",timerText);
    }
}

// Called by delay() about every 250ms. Should not be overwritten by user. delayIdle() should be used instead.
function delayIdle1(remaining) {
    delayIdle(remaining);
}

// Called at at start of delay(). Should not be overwritten by user. delayStart() should be used instead.
function delayStart1() {
    delayStart();
}


// *****************************************************************************
// ******* helper pages ********************************************************
// *****************************************************************************
/**
  * This method is called after script has bee exited.
  * It can be overwritten if desired.
  */
function scriptExitPage() {
    setText("Script has been exited.<br>Reload the page for restart");
    stopAudio();
}


// *****************************************************************************
// ******* forms ***************************************************************
// *****************************************************************************
/**
  * This method starts the form mode, i.e. clears and swaps text and media area and backups and clears global buttons.
  */
function formStart() {
    scriptVars.buttonsBackup = scriptVars.buttons;
    scriptVars.buttons = {
	actions : [],
	names : [],
    };
    const te = document.getElementById('text');
    scriptVars.textAlignBackup = te.style.textAlign;
    te.style.textAlign = 'left';
    const me = document.getElementById('media');
    scriptVars.mediaAlignBackup = me.style.textAlign;
    me.style.textAlign = 'left';
    updateButtonsRequired = true;
    updateButtons();
    clearMedia();
    clearText();
    scriptVars.formMode = true;
}

/**
  * This method ends the form mode, i.e. clears and swaps text and media area and restores global buttons.
  */
function formEnd() {
    if ( scriptVars.buttonsBackup ) scriptVars.buttons = scriptVars.buttonsBackup;
    delete scriptVars.buttonsBackup
    document.getElementById('text').style.textAlign = scriptVars.textAlignBackup;
    delete scriptVars.textAlignBackup
    document.getElementById('media').style.textAlign = scriptVars.mediaAlignBackup;
    delete scriptVars.mediaAlignBackup;
    updateButtonsRequired = true;
    updateButtons();
    clearMedia();
    clearText();
    scriptVars.formMode = false;
}


/**
  * Creates a text input field that saves the content in <code>scriptVars[paramName]</code>. This variable is also used as default input.
  * @param {String} paramName Name of the parameter
  * @return A HTML snippet which defines the text input field.
  */
function textInput(paramName) {
    var v = getScriptVar(paramName);
    try {
	if ( typeof(v) !== "string" ) {
	    setScriptVar(paramName, "");
	    v = "";
	}
    }
    catch(err) {
	console.log(err);
	scriptError("First argument of 'appendTextInput()'/'textInput()' must be a valid variable name");
    }
    return '<input type="text" style="width:calc(100% - 20px);" oninput="setScriptVar(\'' + paramName + '\',this.value);" value="' + v.replace(/\"/g /*"*/, "&quot;")+ '">';
}


/**
  * Appends a text input field to text area that saves the content in <code>scriptVars[paramName]</code>. This variable is also used as default input.
  * @param {String} paramName Name if the parameter
  */
function appendTextInput(paramName) {
    appendText(textInput(paramName));
}

function sliderInputUpdate(paramName, digits) {
    const v = Number(document.getElementById("si_slider_"+paramName).value);
    setScriptVar(paramName, v);
    document.getElementById("si_span_"+paramName).innerHTML = v.toFixed(digits);
}


/**
  * Creates a slider input field that saves the content in <code>scriptVars[paramName]</code>. This variable is also used as default input.
  * @param {String} paramName the name of the parameter
  * @param min Minimum valid value
  * @param max Maximum valid value
  * @param step Slider step size (optional)
  * @return HTML snippet which defines the slider element.
  */
function sliderInput(paramName, min, max, step) {
    let v = 0;
    try {
	v = Number(getScriptVar(paramName));
	if ( Number.isNaN(v) ) throw "scriptVars." + paramName + " is not a number";
    }
    catch(err) {
	console.log(err);
	scriptError("First argument of 'sliderInput()'/'appendSliderInput()' must be a valid variable name");
        return;
    }

    let minn = Number(min);
    if ( Number.isNaN(minn) ) {
        scriptError( "Parameter min of 'sliderInput(paramName,min,max,step)'/'appendSliderInput(paramName,min,max,step)' is not a number: '" + min + "'");
        return;
    }
    let maxn = Number(max);
    if ( Number.isNaN(maxn) ) {
        scriptError( "Parameter max of 'sliderInput(paramName,min,max,step)'/'appendSliderInput(paramName,min,max,step)' is not a number: '" + max + "'");
        return;
    }
    if ( maxn < minn ) {
	const t = maxn;
	maxn = minn;
	minn = t;
    }

    let stepn = Number(step);
    if ( Number.isNaN(stepn) ) stepn = Math.pow(10, Math.floor(Math.log10((maxn-minn)/50)));	
    const digits = Math.max(0,-Math.floor(Math.log10(stepn)+0.05));
    
    return '<span style="display:inline-block;width:5em;text-align:center;vertical-align:middle" id="si_span_' + paramName + '">' + v.toFixed(digits) + '</span><input type="range" min="' + minn + '" max="' + maxn + '" value="' + v + '" step="'+stepn+'" oninput="sliderInputUpdate(\'' + paramName + '\',' + digits +');" id="si_slider_' + paramName + '" style="width:calc(100% - 8em - 8px);vertical-align:middle">';
}


/**
  * Appends a slider input field to text area that saves the content in <code>scriptVars[paramName]</code>. This variable is also used as default input.
  * @param {String} paramName the name if the parameter
  * @param min Minimum valid value
  * @param max Maximum valid value
  * @param step Slider step size (optional)
  */
function appendSliderInput(paramName, min, max, step) {
    appendText(sliderInput(paramName, min, max, step));
}


function cbInputUpdate(paramName) {
    const v = document.getElementById("cb_"+paramName).checked;
    setScriptVar(paramName, v);
//    console.log("cb: " + paramName + ": " + getScriptVar(paramName));
}


/**
  * Creates one or multiple check boxes whose state is stored in <code>scriptVars[paramName]</code>. These variables are also used as default input.
  * @param paramNames Nname(s) of the parameter(s) as string or array of strings.
  * @param labels Labels of the check boxes as string or array of strings.
  * @return HTML snippet which defines the checkbox fields.
  */
function checkBoxInput(paramNames, labels) {
    let ps = Array.isArray(paramNames) ? paramNames : [ paramNames ];
    let ls = Array.isArray(labels) ? labels : [ labels ];
    let text = ""
    for ( let i=0; i< ps.length; i++ ) {
	if ( (typeof(ps[i]) != 'string') || (ps[i].length<1) ) {
	    scriptError("First argument of 'appendCheckBoxInput()'/'checkBoxInput()' must be a non-empty string or an array of non-empty strings");
	    return;
	}
	if ( i>0 ) text+="&nbsp;&nbsp;&nbsp;"
	text +='<input type="checkbox" oninput="cbInputUpdate(\'' + ps[i] + '\');" id="cb_' + ps[i] + '"';
	if ( getScriptVar(ps[i]) ) text+=" checked"
	text += ">&nbsp;"
	if ( i < ls.length ) text += "<label>" + ls[i] + "</label>";
    }
    return text;
}


/**
  * Appends one or multiple check boxes whose state is stored in <code>scriptVars[paramName]</code> to text area. These variables are also used as default input.
  * @param paramNames Name(s) of the parameter(s) as string or array of strings.
  * @param labels Labels of the check boxes as string or array of strings.
  */
function appendCheckBoxInput(paramNames, labels) {
    appendText(checkBoxInput(paramNames, labels));
}


function rbInputUpdate(paramName) {
    const e = document.getElementById("rb_"+paramName)
    const v = e.checked;
    const es = document.getElementsByName(e.name);
    for ( const f of es ) {
	const p = f.id.substring(3);
	setScriptVar(p, p == paramName);
//	console.log("rb: " + p + ": " + getScriptVar(p));
    }
}


/**
  * Creates at least two radio buttons whose state is stored in <code>scriptVars[paramName]</code>. These variables is also used as default input.
  * Unlike check boxes, only one of the radio button can be enabled at the same time.
  * @param paramNames The names of the parameters as array of strings.
  * @param labels The labels of the radio buttons as array of strings.
  * @return HTML snippet which defines the input field.
  */
function radioButtonInput(paramNames, labels) {
    if ( (!Array.isArray(paramNames)) || (paramNames.length<2) ) throw "First argument of 'appendRadioButtonInput()'/'radioButtonInput()' must be an array of strings with the minimum length of 2."
    let ls = Array.isArray(labels) ? labels : [ labels ];
    let text = "<fieldset>"
    let c = false;
    for ( let i=0; i< paramNames.length; i++ ) {
	if ( (typeof(paramNames[i]) != 'string') || (paramNames[i].length<1) ) {
	    scriptError("First argument of 'appendRadioButtonInput()'/'radioButtonInput()' must be a non-empty string or an array of non-empty strings");
	    return;
	}
	text +='&nbsp;&nbsp;<input type="radio" oninput="rbInputUpdate(\'' + paramNames[i] + '\');" id="rb_' + paramNames[i] + '" name="rb_' + paramNames[0] + '"';
	if ( getScriptVar(paramNames[i]) ) {
	    if ( c ) setScriptVar(paramNames[i], false)
	    else text+=" checked";
	    c = true;
	}
	if ( i < ls.length ) text += "><label>" + ls[i] + "</label>";
	else text += ">";
    }
    return text+"</fieldset>";
}


/**
  * Appends at least two radio buttons whose state is stored in <code>scriptVars[paramName]</code> to text area. These variables is also used as default input.
  * Unlike check boxes, only one of the radio button can be enabled at the same time.
  * @param paramNames Nmes of the parameters as array of strings.
  * @param labels Labels of the radio buttons as array of strings.
  */
function appendRadioButtonInput(paramNames, labels) {
    appendText(radioButtonInput(paramNames, labels));
}
