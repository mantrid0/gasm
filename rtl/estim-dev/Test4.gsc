form_start:
    #   formStart();					// starts form mode
    #   @channelConfig = encodeChannelConfig();		// converts current channel config to a string
    #   @channelConfigErr = false;			// no error

channelConfig:
    #   estimStop();					// stop audio
    <	<h2>Channel setup</h2>
    <<  Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and @maxEstimChannels. 
    <<  E.g. "1,1,1" for 3 independent channels or "2,1,1," for 4 channels where the first two ones have equal constraints.
    <<  <input type="text" id="channelConfig" value="@channelConfig">
    <<	Click on 'Done' in order to start audio with a 5s ramp-up time.
    TB  interrupt(); Done
    #   if ( @channelConfigErr ) appendText("Error: " + @channelConfigErr );
    D   

// checks syntax of the channel configuration string and goto previos code block in an error occurred
    #   @channelConfig = document.getElementById("channelConfig").value;
    #   @channelConfigErr = await decodeChannelConfig( @channelConfig );
    #   if ( @channelConfigErr ) @goto("channelConfig");
// forces to end code block in order to avoid printing the stuff below if an error occurred
:

stopTraining:
    #	@volTraining = 0;
    #	@d1Training = 0;
    #	@d2Training = 0;

form2:
    #   await estimPlay();				// start audio and initializes it if necessary 
    <   <h2>EStim parameters</h2>
    <<  <h3>User defined parameters</h3>
    <<	These parameters are user dependent and should not be set by the tease (author).
    #	appendEstimSlider( "<b>Pulse width</b> in ms. If you are new in estimming, choose 0.5. If you are numb from all the estimming, 1 is a better choice. Parameter name is 'pw'." , "pw" );
    #	appendEstimSlider( "<b>Inhibition time</b> in ms. Test it with 'volume variation' 1 and 'rise time' 0. Chose the value such that the signal becomes most intense (painful if volume was not lowered). If in doubt, choose 50. Parameter name is 'it'." , "it" );
    #	appendEstimSlider( "<b>Current limitation at common</b>. 0 means no limitation. 1 is the maximum feasible limitation and 2 is more or less (depending on output circuit and amplifier balance) equal to disconnecting common. Parameter name is 'ccl'. In order to calibrate it, set 'maximum phase shift' to 0 and choose it such that changing 'phase offsets' has on impact on intensity. " , "ccl" );
    #	appendEstimSlider( "<b>Phase offsets</b>. Parameter name is 'po'. Test this value with 'maximum phase shift' set to 0." , "po" );
    <<  <h3>Main settings</h3>
    <<	These parameters should be (half-)automatically adjusted during teases.
    #	appendEstimSlider("<b>Volume</b> per group. Maximum initial setting should be 0.5 or less. Parameter name is 'vol1' for group 1, etc..", "vol");
    <<  <h3>Tease settings</h3>
    <<	These parameters are either set manually by the tease or are increased automatically. The two difficulty settings which have different effects. 
    #	appendEstimSlider( "<b>Difficulty 1</b>. This setting has a higher impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'da.'" , "da" );
    #	appendEstimSlider( "<b>Difficulty 2</b>. This setting has a smaller impact than the other difficulty setting, but it can be set per channel. Negative values are supported, but they can cause balance issues and therefore should be avoided. Parameter name is 'db0','db1' for grop 1, etc.." , "db" );
    #	appendEstimSlider( "<b>Phase shift variation frequency</b> in Hz. Parameter name is 'vf.'" , "vf" );
    #	appendEstimSlider( "<b>Maximum phase shift</b>. Parameter name is 'ps.' As larger this setting as larger the effect of 'Difficulty 2' and 'Phase shift variation frequency'. (If zero, these parameters have no effect)" , "ps" );
    <<	<h3>Slow volume variation</h3>
    <<	These settings can be used for slow volume variations and should be set by tease, i.e. it is not reasonable to set them automatically.
    <<	Use these settings if you want to produce tease and denial sessions. In all other cases it is o.k. to disable volume variations (by setting duty ratio to 1 and variation to 0).
    #	appendEstimSlider( "<b>Duty ratio</b>. Parameter name is 'dr'." , "dr");
    #	appendEstimSlider( "<b>Volume variation</b>. Parameter name is 'vv'" , "vv");
    #	appendEstimSlider( "<b>Rise time</b> in s. Parameter name is 'rt'" , "rt");
    <<	<h3>Modulation</h3>
    <<	Modulation is mainly intended for training, but also can be used manually.
    #	appendEstimSlider( "<b>Modulation time</b> in s. Parameter name is 'mt'." , "mt" );
    #	appendEstimSlider( "<b>Volume modulation</b> per group. Parameter name 'mv1' for group 1, etc.." , "mv" );
    #	appendEstimSlider( "<b>Difficulty 1 modulation</b>. Parameter name is 'mda'." , "mda");
    #	appendEstimSlider( "<b>Difficulty 2 modulation</b> per group.  Parameter name is 'mdb1' for group1, etc.." , "mdb" );
    <<	<h2>Training</h2>
    <<  Click on the buttons to slowly increase various settings.
    <<	Volume training: @volTraining % per minute<br>Difficulty 1 training: @d1Training percent points per minute<br>Difficulty 2 training: @d2Training percent points per minute
    #	changeEstimParam( "vol" , @volTraining/(60*100) );
    #	changeEstimParam( "da" , @d1Training/(60*100) );
    #	changeEstimParam( "db" , @d2Training/(60*100) );
    TB  interrupt(); Back
    TBG  stopTraining Stop Training
    TBG  volTrain Increase volume training by 5% per minute
    TBG  d1Train Increase difficulty 1 training by 5 percent points per minute
    TBG  d2Train Increase difficulty 2 training by 10 percent points per minute
    D  
    #   formEnd();
    G   form_start

volTrain:
    #	@volTraining += 5;
    G	form2
d1Train:
    #	@d1Training += 5;
    G	form2
d2Train:
    #	@d2Training += 10;
    G	form2
