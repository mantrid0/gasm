'use strict';

/*
 * estim-dev/estim.js is the development version of the worker-independent part of the API
 * It is used in combination with a worker-dependent part which loads the worker form a file. 
 * This may cause cross origin content errors if loaded from file system.
 * 
 * Content authors should use the auto-generated estim.js  which merges the development components
 * and loads the worker as data URL in order to avoid cross origin content errors.
 */

// must be equal to maxChannels in estim-processor.js
const estimMaxChannels = 7;	// maximum number of output channels. Currently audio API only defines up to 5 full frequency channels.

/**
  * Number of audio channels. Supported values are 2 to 5.
  */
scriptVars.estimChannels = 2;

/**
  * Number of channels per group. Sum of they array must be equal to {@link scriptVars.estimChannels}.
  */
scriptVars.estimGroups = [1,1];

/**
  * A HTML-formatted text that contains the Estim device requirements and electrode recommendation
  */
scriptVars.estimRequirements = "";

/**
  * Init Estim script variables.
  */
function initScriptVarsEstim () {
    if ( estimNode ) {
	estimStop();
	estimNode = null;
	estimMute = false;
    }
    scriptVars.estimChannels = 2;
    scriptVars.estimGroups = [1,1];
    scriptVars.maxEstimChannels = Math.min(estimMaxChannels, audioContext.destination.maxChannelCount);
    scriptVars.estimTrainingParams = {};
    scriptVars.estimRequirements = 
              "The Estim signals are generated dynamically and are configurable. More than 2 output channels are supported.\n"
	    + "<p>The Estim device must not re-modulate the signal and must be current controlled. (This is achieved by serial resistors in the output driver. Parallel resistors have the opposite effect.)\n"
	    + "<p>The output signal suppresses low-frequency components. However, (digital) output hardware stages can lead to low-frequency sampling artifacts due to the complexity of the signal.\n"
	    +    "Therefore, an analog high-pass filter (serial capacitor in the output driver) with a 50% cutoff limit around 100 Hz to 200 Hz is recommended.\n"
	    + "<p>The same pole of all channels must be is connected to the common electrode.";
    if ( ! estimControlCommon ) scriptVars.estimRequirements += "\n"+
	      "This electrode should be the largest one and should not be placed in sensitive areas as it is only indirectly controlled (unlike the non-common electrodes).";
    initScriptVarsEstimProcessor();
}

// *****************************************************************************
// ******* initialization and playback control *********************************
// *****************************************************************************
//const audioContext = new AudioContext( { latencyHint: "balanced", sampleRate: 11025 } );
const audioContext = new AudioContext( { latencyHint: "balanced", sampleRate: 48000 } );	// use default sample rate in order to avoid re-sampling artifacts

console.log("Audio processor latency: " + (1000.0*audioContext.outputLatency) + "ms");

let estimNode = null;

// mute parameter of estim processor
var estimMute = false;

/**
  * (Re-)Initializes the Estim audio processor. Number of channels is read from {@link scriptVars.estimChannels}.
  * There is no reason to call this function manually because this is done automatically by {@link estimPlay()}.
  */
function estimInit() {}	 // dummy for jsDoc
async function estimInit() {
    const n2 = 2 * Math.round(scriptVars.estimChannels*0.5+0.1);        // number of hardware channels
    
    try {
        audioContext.destination.channelCount = n2;

        if ( estimNode ) {
            if ( estimNode.channelCount == n2 ) {
                estimNode.parameters.get("channels").value = scriptVars.estimChannels;
                return;
            }
            estimStop();        // may have no effect if estimStart() is called quickly
        }
        else {
            await loadEstimProcessor();
        }

        estimNode = new AudioWorkletNode( audioContext, "estim-processor", { numberOfInputs: 0, numberOfOutputs: 1, outputChannelCount: [n2] } );
        estimNode.channelCount = n2;
        estimNode.channelCountMode = "explicit"; 
        estimNode.parameters.get("channels").value = scriptVars.estimChannels;
        // copy parameters
        estimMute = estimNode.parameters.get("mute");
        syncEstimParams();
        estimNode.connect(audioContext.destination);
        audioContext.resume();
    } catch ( e ) {
        scriptError( "Error starting EStim worker: " + e.message);
    }
}


/**
  * Starts Estim audio and initializes audio processor, if necessary
  */
function estimPlay() {}	 // dummy for jsDoc
async function estimPlay() {
    await estimInit();
    estimMute.value = 0;
}

/**
  * Check whether Estim is playing
  * @return Return true is Estim is playing.
  */
function estimIsPlaying() {
    try {
	return (estimMute) && (estimMute.value<=0.5);
    } catch ( e ) { }
    return false;
}

/**
  * Stops Estim audio
  */
function estimStop() {
    if ( estimMute ) estimMute.value = 1;
    scriptVars.estimTrainingParams = {};
}

// Called before a page is loaded in order to save EStim DOM content. This is required for correct DOM restoration after restoration of scriptVars
function backupDOMEStim() {
    scriptVars.backupDOM.estimStarted = Boolean(estimNode);
    scriptVars.backupDOM.estimMute = estimMute ? estimMute.value : 1.0;
}

// async part of adio restoration.  
async function restoreDOMEStimAsync( mute ) {
    await estimInit();
    estimMute.value = mute;
}


// Called after restoration of scriptVars and can be used for things like restoring DOM content.
function restoreDOMEStim() {
    let err = false;
    scriptVars.maxEstimChannels = Math.min(estimMaxChannels, audioContext.destination.maxChannelCount);
    if ( scriptVars.estimChannels > scriptVars.maxEstimChannels ) {
        scriptError( "Unable to restore audio from previous state because number of audio cahnnels is to large: " + scriptVars.estimChannels+ " > " + scriptVars.maxEstimChannels+ ". Loading default values.");
        initScriptVarsEstim();
    }
    if ( scriptVars.backupDOM.estimStarted ) {
	// ensure to load mute parameter before the async part starts
	const m = scriptVars.backupDOM.estimMute;
	console.log("Restoring EStim: mute=" + m);
	restoreDOMEStimAsync( scriptVars.backupDOM.estimMute );
    }
}

/** 
  * Restores Estim settings from a previous session.
  * @param o The object that is contain a <code>scriptVars</code> object.
  * @param v If truthy, also restores visited blocks
  */
function estimRestoreState(o, v) {
    if ( typeof(o) != "object" ) {
        scriptError( "estimRestoreState: First parameter must be an object");
	return;
    }
    if ( (typeof(o.estimChannels)=="number") && (o.estimChannels<=scriptVars.maxEstimChannels) ) {
	scriptVars.estimChannels = o.estimChannels;
	scriptVars.estimGroups = o.estimGroups;
	scriptVars.estimParams = o.estimParams;
        syncEstimParams();
    }
    else {
        scriptError( "Unable to restore audio setting because number of audio channels is invalid: " + o.estimChannels+ " > " + scriptVars.maxEstimChannels );
	
    }
    if ( v && ( typeof(blockVisited)=="array" ) ) scriptVars.blockVisited = o.blockVisited;
}


// *****************************************************************************
// ******* Estim parameter handling ********************************************
// *****************************************************************************
/**
  * Decodes a channel configuration string which consist in comma separated list of numbers of channels per group.
  * If audio processor was created, its properties are updated.
  * @return Returns false on success, Otherwise an error string.
  */
function decodeChannelConfig(cc) {
    const mc = Math.min(estimMaxChannels, audioContext.destination.maxChannelCount);
    let channels = 0;
    let groups = [];
    if ( typeof(cc) == "number" ) {
        channels = Math.round(cc);
        groups = [channels];
    }
    else if ( typeof(cc) == "string" ) {
        const vals = cc.split(",");
        for ( let i=0; i<vals.length; i++ ) {
            let v = Number(vals[i]);
            if ( isNaN(v) ) return "Number expected, got '" + vals[i] + "'";
            v = Math.round(v); 
            channels += v;
            groups[i] = v;
        }
    }
    else return "Comma separated list of numbers of channels per group expected";
    if ( (channels<2) || (channels>mc) ) return "Total number of channels must be between 2 and " + mc;

    const reinit = ( scriptVars.estimChannels != channels ) && estimNode;
    scriptVars.estimChannels = channels;
    scriptVars.estimGroups = groups;
    if ( reinit ) estimInit();
    
    return false;
}


/**
  * Encodes the channel configuration string, i.e. the function returns a comma separated list of numbers of channels per group
  */
function encodeChannelConfig() {
    if ( scriptVars.estimGroups.length < 1 ) return ""
    let r = scriptVars.estimGroups[0].toFixed(0);
    for ( let i=1; i<scriptVars.estimGroups.length; i++ )
        r += "," + scriptVars.estimGroups[i].toFixed(0);
    return r;
}


// set audio parameter and also updates the slider if found
// ids is the suffix of a slider id. if given, it's value is updated.
// returns the value clipped to min/max
function setAudioParam( ap, v, ids=false ) {
    let n = Number(v);
    if ( Number.isNaN(n) ) return n;
    n = Math.max(ap.minValue, Math.min(ap.maxValue, n));
    ap.value = n;
    if ( ! ids ) return n;
    const slider = document.getElementById("es_slider_"+ids);
    if ( slider ) slider.value = (n-ap.minValue)/(ap.maxValue-ap.minValue)*1000;
    const text = document.getElementById("es_span_"+ids);
    if ( text ) {
	let idx = Number( ids.substring(n.length-1) );
	if ( Number.isNaN(idx) ) idx = -1
	const l = ( idx=-1) ? "" : (idx==0) ? 'C: ' : (idx+': ');
	text.innerHTML = l + n.toPrecision(4).substring(0,10-l.length);
    } 
    return n;
}


// Synchronize EStim settings from scriptVars.estimParams
function syncEstimParams( n=false ) {
    if ( ! estimNode ) return;
    if ( n ) {
	const ep = scriptVars.estimParams[n];
	if ( Array.isArray(ep) ) {
	    const groups = scriptVars.estimGroups;
	    let c = 0;
    	    for ( let g=0; g<=groups.length; g++ ) {
    		if ( ep.length<=g ) ep[g] = ep[ep.length-1];
    		const gn = (g==0) ? 1 : groups[g-1];
    		for (let i=0; i<gn; i++ )
    		    scriptVars.estimParams[n][g] = setAudioParam(estimNode.parameters.get(n+(c+i)), ep[g]);
    		c+=gn;
    	    }
	}
	else {
    	    scriptVars.estimParams[n] = setAudioParam( estimNode.parameters.get(n), ep);
	}
    } 
    else {
	const keys = Object.keys(scriptVars.estimParams);
	for ( let i=0; i<keys.length; i++ ) 
	    syncEstimParams(keys[i]);
    }
}


/**
  * Set an EStim parameter
  * @param n Parameter name. If parameter can be set per electrode and if group index (0 for common) is omitted (e.g. 'dif' instead of 'dif1'), parameter is set for all electrodes.
  * @param v New parameter value.
  */
function setEstimParam( n, v ) {
    n = String(n);
    let idx = Number( n.substring(n.length-1) );
    if ( Number.isNaN(idx) ) idx = -1
    else n = n.substring(0,n.length-1);

    const vn = Number(v);
    if ( Number.isNaN(vn) ) {
        scriptError( "Parameter v of setEstimParam(n,v) is not a valid value: '" + v + "'");
        return;
    }

    var ep;
    try {
	ep = scriptVars.estimParams[n];
    } catch ( e ) {
        scriptError( "Parameter with name '" + n + "' does not exist");
        return;
    }
	
    if ( Array.isArray(ep) ) {
	const groups = scriptVars.estimGroups;
	let c = 0;
    	for ( let g=0; g<=groups.length; g++ ) {
    	    const gn = (g==0) ? 1 : groups[g-1];
    	    if ( (idx<0) || (g==idx) ) {
    		for (let i=0; i<gn; i++ )
    	    	    scriptVars.estimParams[n][g] = estimNode ? setAudioParam(estimNode.parameters.get(n+(c+i)), vn, n+g) : Number(vn);
    	    }
    	    c+=gn;
    	}
    }
    else {
	scriptVars.estimParams[n] = estimNode ? setAudioParam( estimNode.parameters.get(n), vn, n) : Number(vn);
    }
}


/**
  * Get an EStim parameter
  * @param n Parameter name. If parameter can be set per electrode, the group index (0 for common) must be included. (e.g. 'dif1' is allowed, 'dif' not)
  * @return The parameter value
  */
function getEstimParam( n ) {
    n = String(n);
    let idx = Number( n.substring(n.length-1) );
    if ( Number.isNaN(idx) ) idx = -1
    else n = n.substring(0,n.length-1);

    var ep;
    try {
	ep = scriptVars.estimParams[n];
    } catch ( e ) {
        scriptError( "Parameter with name '" + n + "' does not exist");
        return;
    }

    if ( Array.isArray(ep) ) {
        if ( (idx<0) || (idx>scriptVars.estimGroups.length) ) scriptError( "Group index of parameter '" + n + "' is not valid" );
        return ep[idx];
    }
    else return ep;
}


/**
  * Constantly changes an EStim parameter.
  * @param n Parameter name. If parameter can be set per electrode and if group number (0 for common) is omitted, change rate parameter is set for all electrodes.
  * @param v Change rate per second. Volume is changed relatively, all other parameters are changes absolutely. Default change rate is 0.
  */
function changeEstimParam( n, v=0 ) {
    n = String(n);
    let idx = Number( n.substring(n.length-1) );
    if ( Number.isNaN(idx) ) idx = -1
    else n = n.substring(0,n.length-1);

    const vn = Number(v);
    if ( Number.isNaN(vn) ) {
        scriptError( "Parameter v of changeEstimParam(n,v) is not a valid value: '" + v + "'");
        return;
    }

    var ep;
    try {
	ep = scriptVars.estimParams[n];
    } catch ( e ) {
        scriptError( "Parameter with name '" + n + "' does not exist");
        return;
    }

    const tp = scriptVars.estimTrainingParams;
    if ( Array.isArray(ep) ) {
	const groups = scriptVars.estimGroups;
	let c = 0;
    	for ( let g=0; g<=groups.length; g++ ) {
    	    const gn = (g==0) ? 1 : groups[g-1];
    	    if ( (idx<0) || (g==idx) ) {
    		if ( Math.abs(vn)<1e-12 ) delete tp[n+g];
    		else tp[n+g] = (n=="vol") ? getEstimParam(n+g)*vn : vn; 
    	    }
    	    c+=gn;
    	}
    }
    else {
    	if ( Math.abs(vn)<1e-12 ) delete tp[n];
    	else tp[n] = vn; 
    }
//    console.log(tp);
}


// parameter update interval in ms (for training)
const updateEstimParamInterval = 200;


// update Estim parameters 
function updateEstimParam( params ) {
    const tp = scriptVars.estimTrainingParams;
    if ( (typeof(tp) != "object") ) return;
    const keys = Object.keys(tp);
    for ( let i=0; i<keys.length; i++ ) {
	const n = keys[i];
//	console.log(n+": "+ getEstimParam(n) + " --> " + (getEstimParam(n)+tp[n]*updateEstimParamInterval*1e-3) );
	setEstimParam( n, getEstimParam(n)+tp[n]*updateEstimParamInterval*1e-3 );
    }
}

setInterval( updateEstimParam, updateEstimParamInterval );

/**
  * Produces a string containing all EStim parameters.
  * @param params Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return A string containing all EStim parameters.
  */
function estimParamStr ( params ) {
    if ( (typeof(params) != "object") )
	params = scriptVars.estimParams;
    const keys = Object.keys(params);
    let str = "";
    const g0 = estimControlCommon ? 0 : 1;
    for ( let i=0; i<keys.length; i++ ) {
	if ( i > 0 ) str += "\n";
	const n = keys[i];
	const v = params[n];
	str += n + ": ";
	if ( Array.isArray(v) ) {
    	    for ( let g=g0; g<=scriptVars.estimGroups.length; g++ ) {
    		const e = v[g];
    		if ( g > g0 ) str += ", ";
		if ( (typeof(e) == "number") ) str += e.toPrecision(4);
		else str += e;
    	    }
    	}
    	else {
	    if ( (typeof(v) == "number") ) str += v.toPrecision(4)
	    else str += v;
	}
    }
    return str;
}


// *****************************************************************************
// ******* Estim parameter slider **********************************************
// *****************************************************************************
// Called if the slider is moved
// apn: audio paramter name
// epn: estimParams name (key)
// epi: estimParams indo or -1 if not an array
function estimSliderInput(apn, epn,epi) {
    if ( !estimNode ) {
	console.log("Warning: estimSliderInput: EStim has not neeb started");
	return;
    }
    const l = ( epi==-1) ? "" : (epi==0) ? 'C: ' : (epi+': ');
    const p = estimNode.parameters.get(apn);
    const id = (epi>=0) ? epn+epi : epn;
    const slider = document.getElementById("es_slider_"+id);
    const val = p.minValue + slider.value*1e-3*(p.maxValue-p.minValue);
    document.getElementById("es_span_"+id).innerHTML = l + val.toPrecision(4).substring(0,10-l.length);
    if ( epi<0 ) scriptVars.estimParams[epn] = val;
    else scriptVars.estimParams[epn][epi] = val;
    syncEstimParams( epn );
}

// Creates the slider
// apn: audio parameter name
// epn: estimParams name (key)
// epi: estimParams index or -1 if not an array
function appendEstimSliderInt(apn, epn,epi) {
    if ( !estimNode ) {
	console.log("Warning: estimSliderInput: EStim has not neeb started");
	return;
    }
    const l = ( epi==-1) ? "" : (epi==0) ? 'C: ' : (epi+': ');
    const p = estimNode.parameters.get(apn);
    if ( !p ) throw 'EStim parameter "'+epn+'" not defined';
    const val = Math.round( (p.value-p.minValue)/(p.maxValue-p.minValue)*1000);
    const id = (epi>=0) ? epn+epi : epn;
    const html = '<span style="display:inline-block;width:7em;text-align:center;vertical-align:middle" id="es_span_' + id + '">' + l + p.value.toPrecision(4).substring(0,10-l.length) + '</span><input type="range" min="0" max="1000" value="' + val + '" step="1" oninput="estimSliderInput(\'' + apn + '\',\'' + epn + '\',' + epi + ');" id="es_slider_' + id + '" style="width:calc(100% - 10em - 8px);vertical-align:middle">';
    return html;
}

/**
  * Appends a slider for an EStim parameter to text area. If EStim is played, parameter is changed interactively.
  * @param text Description text
  * @param param Parameter name.
  */
function appendEstimSlider(text, param) {
    finishPage();
    let html = "" + text;
    const ep = scriptVars.estimParams[param];
    if ( Array.isArray(ep) ) {
        let c=1;
        const groups = scriptVars.estimGroups;
        for ( let g=1; g<=groups.length; g++ ) {
	    if ( html.length>0 ) html += "<br>";
            html += appendEstimSliderInt(param+c, param,g);
            c+=groups[g-1];
        }
        if ( estimControlCommon ) {
	    if ( html.length>0 ) html += "<br>";
    	    html += appendEstimSliderInt(param+"0", param,0);
    	}
    }
    else {
	if ( html.length>0 ) html += "<br>";
        html += appendEstimSliderInt(param, param,-1);
    }
    appendText(html);

}


// *****************************************************************************
// ******** processor independent parts of high level API 1 ********************
// *****************************************************************************
// clip values and normalize weight to 1
function estimHL1_normalizePWA(pwa) {
    let s = 0;
    for ( let i=1; i<pwa.length; i+=2 ) {
	pwa[i-1] = Math.max(0, Math.min(1, pwa[i-1]));
	s += pwa[i];
    }
    s = 1/s;
    for ( let i=1; i<pwa.length; i+=2 ) 
	pwa[i] *= s;
}

// Set parameters.Ffor internal use, see estimHL1_setI() and estimHL1_setP()
// pwa: parameter+weight array
// prio: priority factors (recommended: >0)
// ran: randomization (0..1)
// val: new value (0..1)
function estimHL1_set(pwa, prio, val, ran ) {
    ran = Math.max(0, Math.min(1, ran));
    val = Math.max(0, Math.min(1, val));
    if ( ran > 1e-3 ) 
	for ( let i=1; i<pwa.length; i+=2 ) 
	    pwa[i-1] = pwa[i-1]*(1-ran) + (
	    (pwa[i-1]*(1-ran)+Math.random()*ran)*2-0.5)*ran;

    let r = -val;
    let s = 0;
    for ( let i=0; i*2+1<pwa.length; i++ ) {
	const w = pwa[i*2+1];
	r += Math.max(0,Math.min(1,pwa[i*2]))*w;
	s += prio[i]*w;
    }
    while ( (Math.abs(r)>1e-12) && (s>0) ) {
	const t = r/s;	
	r = -val;
	s = 0;
	for ( let i=0; i*2+1<pwa.length; i++ ) {
	    const u = pwa[i*2] - prio[i]*t;
	    const w = pwa[i*2+1];
	    if ( ((t>0) && (u>0)) || ((t<0) && (u<1)) )
		s += prio[i]*w;
	    pwa[i*2] = u;
	    r += Math.max(0,Math.min(1,u))*w;
	} 
    }
    for ( let i=1; i<pwa.length; i+=2 ) 
	pwa[i-1] = Math.max(0,Math.min(1,pwa[i-1]));
}

/**
  * Set intensity level. Range is from 0 to 1.
  * @param val New insensitivity value 
  * @param ran Randomization of current settings (0..1). Optional. Default: 1
  */
function estimHL1_setI(val, ran, prio) {
    if ( typeof(val) != "number" ) scriptError( "estimHL1_setI: 1st Argument must be a number");
    if ( typeof(ran) != "number" ) ran = 1;
   // TODO: select channels 
    const pwa = estimHL1_readIPWA();
    if ( ! Array.isArray(prio) ) {
	prio = [];
	for ( let i=0; i*2+1<pwa.length; i++ )
	    prio[i] = 1;
    }
    estimHL1_set(pwa, prio, val, ran );
    estimHL1_writeIPWA(pwa);
}

/**
  * Calculate intensity level.
  * @param param Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return Current intensity level.
  */
function estimHL1_getI( params ) {
    const pwa = estimHL1_readIPWA( params );
    let r = 0;
    for ( let i=1; i<pwa.length; i+=2 )
	r += pwa[i-1]*pwa[i];
    return r;
}

/**
  * Set pain level. Range is from 0 to 1.
  * @param val New pain value 
  * @param ran Randomization of current settings (0..1). Optional. Default: 1
  */
function estimHL1_setP(val, ran, prio) {
    if ( typeof(val) != "number" ) scriptError( "estimHL1_setI: 1st Argument must be a number");
    if ( typeof(ran) != "number" ) ran = 1;
   // TODO: select channels 
    const pwa = estimHL1_readPPWA();
    if ( ! Array.isArray(prio) ) {
	prio = [];
	for ( let i=0; i*2+1<pwa.length; i++ )
	    prio[i] = 1;
    }
    estimHL1_set(pwa, prio, val, ran );
    estimHL1_writePPWA(pwa);
}

/**
  * Calculate pain level.
  * @param param Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return Current pain level.
  */
function estimHL1_getP( params ) {
    const pwa = estimHL1_readPPWA( params );
    let r = 0;
    for ( let i=1; i<pwa.length; i+=2 )
	r += pwa[i-1]*pwa[i];
    return r;
}

/**
  * Produces a string containing intensity and pain level and all Estim parameters.
  * @param param Optional object containing the EStim parameters. If not given, {@link scriptVars.estimParams} is used.
  * @return A string containing all EStim parameters.
  */
function estimHL1_toStr( params ) {
    return "intensity=" + estimHL1_getI(params).toPrecision(4) + "  pain=" + estimHL1_getP(params).toPrecision(4) + "\n" + estimParamStr ( params );
}
