const scriptCode = {
    "code" : [
	"   formStart();					// starts form mode\n   scriptVars.channelConfig = encodeChannelConfig();		// converts current channel configuration to a string\n   scriptVars.channelConfigErr = false;			// no error\n\n",
	"   estimStop();					// stop audio\n   setText('<h2>Channel setup</h2>');\n   appendText('Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and '+scriptVars.maxEstimChannels+'.');\n   appendText('E.g. \"1,1,1\" for 3 independent channels or \"2,1,1,\" for 4 channels where the first two ones have equal constraints.');\n   appendText('It is recommended that first channels are used for electrodes on more sensitive places. (Probably has no noticeable effect.)');\n   appendText('<input type=\"text\" id=\"channelConfig\" value=\"'+scriptVars.channelConfig+'\">');\n   appendText('Click on \\'Done\\' in order to start audio with a 5s ramp-up time.');\n   appendTButton('interrupt();;', 'Done');\n   if ( scriptVars.channelConfigErr ) appendText(\"Error: \" + scriptVars.channelConfigErr );\n   { if ( await delay() ) return; }\n",
	"\n// checks syntax of the channel configuration string and goto previos code block in an error occurred\n   scriptVars.channelConfig = document.getElementById(\"channelConfig\").value;\n   scriptVars.channelConfigErr = await decodeChannelConfig( scriptVars.channelConfig );\n   if ( scriptVars.channelConfigErr ) { setGoto(\"channelConfig\"); return; }\n\n// starts a new code block (without label) in order to avoid printing the stuff below if an error occurred\n",
	"   await estimPlay();				// start audio and initializes it if necessary \n   setText('<h2>EStim parameters</h2>');\n   appendText('<h3>User defined parameters</h3>');\n   appendText('These parameters are user dependent and should not be changed by the tease (author).');\n    // These commands create a slider for Estim settings. Syntax is appendEstimSlider(<description>,<parameter name without group index>);\n    // changing the sliders have an immediate effect is audio is playing.\n	appendEstimSlider( \"<b>Pulse width</b> in ms. If you are new in estimming, choose 0.5. If you are numb from all the estimming, 1 is a better choice. Parameter name is 'pw'.\" , \"pw\" );\n	appendEstimSlider( \"<b>Inhibition time</b> ms. Test it with difficulty 2 set to 1 for all electrodes and different difficulty 1 settings. Recommended setting is 50. Parameter name is 'it'.\" , \"it\" );\n   appendText('<h3>Main settings</h3>');\n   appendText('These parameters can be (half-)automatically adjusted during teases. This process is called training.');\n	appendEstimSlider(\"<b>Volume</b> for common electrode ('C') and per group (number). Maximum initial setting should be 0.5 or less. Is is not possible to control these values completely independently. e.g. is is not possible to have current on just one electrode. As more electrodes you have as better they can be controlled. Parameter name is 'vol0' for common, 'vol1' for group 1, etc..\", \"vol\");\n	appendEstimSlider(\"<b>Rise time</b> for common electrode ('C') and per group (number). Set variation to a high value to test this setting. That setting can also be controlled manually. If adjusted automatically, initial value should be 1.  Parameter name is 'rt0' for common, 'rt1' for group 1, etc.. \", \"rt\", 2);\n   appendText('<h3>Tease settings</h3>');\n   appendText('These parameters are set by tease.');\n	appendEstimSlider( \"<b>Duty ratio</b>. Parameter name is 'ar.'\" , \"ar\" );\n	appendEstimSlider( \"<b>Variation</b>. Parameter name is 'av.'\" , \"av\" );\n   appendText('<h3>Modulation</h3>');\n   appendText('Modulation is mainly intended for training, but also can be used manually.');\n	appendEstimSlider( \"<b>Modulation time</b> in s. Parameter name is 'mt'.\" , \"mt\" );\n	appendEstimSlider(\"<b>Volume modulation</b> for common electrode ('C') and per group (number). Parameter name is 'mv0' for common, 'mv1' for group 1, etc..\", \"mv\");\n	appendEstimSlider(\"<b>Rise time modulation</b> for common electrode ('C') and per group (number). Set variation to a high value to test this setting. Parameter name is 'mr0' for common, 'mr1' for group 1, etc..\", \"mr\");\n   appendTButton('interrupt();;', 'Back');\n   { if ( await delay() ) return; }\n",
	"\n   formEnd();\n   { setGoto('form_start'); return; }\n\n//        dv: 0,          // distribution variation\n//        ds: 1,          // softness distribution variation\n",
    ],
    "label" : [
	"form_start",
	"channelconfig",
	null,
	null,
	null,
    ],
    "sourceFN" : [
	"Test1.gsc",
	"Test1.gsc",
	"Test1.gsc",
	"Test1.gsc",
	"Test1.gsc",
    ],
    "sourceLN" : [
	1,
	6,
	16,
	24,
	47,
    ]
};

const imageList = [
    { "dir" : ".", "files" : [ ] },
    { "dir" : "old", "files" : [ ] },
];

const videoList = [
    { "dir" : ".", "files" : [ ] },
    { "dir" : "old", "files" : [ ] },
];

const audioList = [
    { "dir" : ".", "files" : [ ] },
    { "dir" : "old", "files" : [ ] },
];
let baseDirectory=".";

