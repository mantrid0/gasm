calib:
    #   estimStop();					// stop audio
    #   formStart();
    #  	@channelConfig = encodeChannelConfig();		// converts current channel configuration to a string
    #  	@channelConfigErr = false;			// ignore errors

calib0:
    <	<h2>Device requirements and recommendations</h2>
    <<  @estimRequirements
    <<	<h2>Channel setup</h2>
    <<  Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and @maxEstimChannels. 
    <<  E.g. "1,1,1" for 3 independent channels or "2,1,1," for 4 channels where the first two ones have equal constraints.
    #	appendTextInput("channelConfig");
    <<	Click on 'Done' in order to start audio with a 5s ramp-up time.
    TB  interrupt(); Done
    #	@calibButtons = true;
    #   if ( @channelConfigErr ) appendText("Error: " + @channelConfigErr );
    D   

// checks syntax of the channel configuration string and goto previos code block in an error occurred
    #   @channelConfigErr = await decodeChannelConfig( @channelConfig );
    #   if ( @channelConfigErr ) @goto("calib0");
    #	estimHL1_setI(0, 1);
    #	estimHL1_setP(0, 1);
    #	estimHL1_setDR(1);

calib1:
    #   await estimPlay();				// start audio and initializes it if necessary 
//  # 	console.log(estimHL1_toStr());

calib1b:
    <   <h2>EStim parameters</h2>
    <<	These parameters are user dependent and should not be set by the tease (author).
    #	appendEstimSlider( "<b>Carrier frequency</b> in Hz. Recommended: 400..1000." , "fc" );
    #	appendEstimSlider( "<b>Channel balance</b> per group. Recommended: > 0.5. Smaller values feel less intense and softer, because that setting influences penetration depth. (It is o.k. to use the value 1 because this does not affect the volume.)", "cb" );
    #	appendEstimSlider( "<b>Current limitation at common</b>. 0 means no limitation. 1 is the maximum feasible limitation and 2 is more or less (depending on output circuit and amplifier balance) equal to disconnecting common." , "ccl" );
    #	appendEstimSlider( "<b>Sensitivity</b>. Chose 0.75..1 for electrodes at most sensitive places (e.g. glans) and 0..0.25 for electrodes at least sensitive places (e.g. prostate)." , "sens" );
    #	if ( @calibButtons ) {
    TBG		calib Channel setup
    TBG 	calib_vol Calibrate volume
    TBG 	calib_bal Calibrate balance and current reduction at common
    TBG 	calib_sens Calibrate sensitivity
    #	}
    TB  interrupt(); Done
    D  
    #   formEnd();
    R

// Entry point for recalibration during tease. Current EStim signal is not interrupted and button are suppressed
recalib:
    #   formStart();
    #	@calibButtons = false;
    G	calib1b

// I=0
calib_vol:
    #	estimHL1_setI(0, 1);
    #	estimHL1_setP(0, 1);
    G	calib1

// I \approx 0, tl=1
calib_bal:
    #	setEstimParam( "tl", 1 );
    #	setEstimParam( "mtl", 0 );
    #	setEstimParam( "vol", estimHL1_volMin*estimHL1_volMin );
    #	setEstimParam( "mvol", 0 );
    #	estimHL1_setP(0,1);
    G	calib1

calib_sens:
    #	setEstimParam( "tl", 1 );
    #	setEstimParam( "mtl", 0 );
    #	setEstimParam( "vol", estimHL1_volMin*estimHL1_volMin );
    #	setEstimParam( "mvol", 0 );
    #	setEstimParam( "da", 0.5 );
    #	setEstimParam( "mda", 0 );
    #	setEstimParam( "db", 0 );
    #	setEstimParam( "mdb", 0 );
    G	calib1
