const scriptCode = {
    "code" : [
	"   formStart();					// starts form mode\n   scriptVars['channelConfig'] = encodeChannelConfig();		// converts current channel config to a string\n   scriptVars['channelConfigErr'] = false;			// no error\n\n",
	"   estimStop();					// stop audio\n   setText('<h2>Channel setup</h2>');\n   appendText('Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and '+scriptVars['maxEstimChannels']+'.');\n   appendText('E.g. \"1,1,1\" for 3 independent channels or \"2,1,1,\" for 4 channels where the first two ones have equal constraints.');\n   appendText('<input type=\"text\" id=\"channelConfig\" value=\"'+scriptVars['channelConfig']+'\">');\n   appendText('Click on \\'Done\\' in order to start audio with a 5s ramp-up time.');\n   appendTButton('interrupt();;', 'Done');\n   if ( scriptVars['channelConfigErr'] ) appendText(\"Error: \" + scriptVars['channelConfigErr'] );\n   { if ( await delay() ) return; }\n",
	"\n// checks syntax of the channel configuration string and goto previos code block in an error occurred\n   scriptVars['channelConfig'] = document.getElementById(\"channelConfig\").value;\n   scriptVars['channelConfigErr'] = await decodeChannelConfig( scriptVars['channelConfig'] );\n   if ( scriptVars['channelConfigErr'] ) { setGoto(\"channelConfig\"); return; }\n// forces to end code block in order to avoid printing the stuff below if an error occurred\n",
	"	scriptVars['drTraining'] = 0;\n	scriptVars['d1Training'] = 0;\n	scriptVars['d2Training'] = 0;\n\n",
	"   await estimPlay();				// start audio and initializes it if necessary \n   setText('<h2>EStim parameters</h2>');\n   appendText('<h3>User defined parameters</h3>');\n   appendText('These parameters are user dependent and should not be set by the tease (author).');\n	appendEstimSlider(\"<b>Volume</b> per group. Should not be changed by the tease. Thus, maximum setting can and should be 1. Parameter name is 'vol1' for group 1, etc..\", \"vol\");\n	appendEstimSlider( \"<b>Current limitation at common</b>. 0 means no limitation. 1 is the maximum feasible limitation and 2 is more or less (depending on output circuit and amplifier balance) equal to disconnecting common. Parameter name is 'ccl'. In order to calibrate it, set 'maximum phase shift' to 0 and choose it such that changing 'phase offsets' has on impact on intensity. \" , \"ccl\" );\n	appendEstimSlider( \"<b>Recognition limit</b> per group in Hz. Start with the most sensitive electrode and reduce (starting from maximum) until you feel a change (should feel like tickling). Parameter name is 'rl'.\" , \"rl\" );\n	appendEstimSlider( \"<b>Phase position</b> per group in Hz. Start with the most sensitive electrode and reduce (starting from maximum) until you feel a change (should feel like tickling). Parameter name is pp'.\" , \"pp\" );\n	appendEstimSlider( \"<b>Phase jump</b> (for debugging)\" , \"pj\" );\n   appendText('<h3>Tease settings</h3>');\n   appendText('These parameters are the low level parameters changed by the tease. (This should be done using a high-level API becaouse low level API may change.)');\n	appendEstimSlider( \"<b>Low frequncy duty ratio</b> per group. Parameter name is 'drl'.\" , \"drl\");\n	appendEstimSlider( \"<b>High frequncy duty ratio</b> per group. Parameter name is 'drh'.\" , \"drh\");\n	appendEstimSlider( \"<b>Difficulty 1</b> per group. This setting has a higher impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'da1' four group 1, ...\" , \"da\" );\n	appendEstimSlider( \"<b>Difficulty 2</b> per group. This setting has a smaller impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'db1' for grop 1, etc..\" , \"db\" );\n   appendText('<h3>Modulation</h3>');\n   appendText('Some of the paremeters can be varied which is called parameter modulation.');\n	appendEstimSlider( \"<b>High frequncy duty ratio modulation</b> per group. Parameter name is 'mdr1' for group 1, ...\" , \"mdr\");\n	appendEstimSlider( \"<b>Difficulty 1 modulation</b> per group. Parameter name is 'mda1' for group 1, ...\" , \"mda\");\n	appendEstimSlider( \"<b>Difficulty 2 modulation</b> per group. Parameter name is 'mdb1' for group 1, ...\" , \"mdb\" );\n   appendText('<h2>Training</h2>');\n   appendText('Click on the buttons to slowly increase various settings.');\n   appendText('High frequncy duty ratio training: '+scriptVars['drTraining']+' percent points per minute<br>Difficulty 1 training: '+scriptVars['d1Training']+' percent points per minute<br>Difficulty 2 training: '+scriptVars['d2Training']+' percent points per minute');\n	changeEstimParam( \"drh\" , scriptVars['drTraining']/(60*100) );\n	changeEstimParam( \"da\" , scriptVars['d1Training']/(60*100) );\n	changeEstimParam( \"db\" , scriptVars['d2Training']/(60*100) );\n   appendTButton('interrupt();;', 'Back');\n   appendTButton(\"setGoto('stoptraining')\", 'Stop Training');\n   appendTButton(\"setGoto('drtrain')\", 'Increase high frequncy duty ratio training by percent points per minute');\n   appendTButton(\"setGoto('d1train')\", 'Increase difficulty 1 training by 5 percent points per minute');\n   appendTButton(\"setGoto('d2train')\", 'Increase difficulty 2 training by 10 percent points per minute');\n   { if ( await delay() ) return; }\n",
	"   formEnd();\n   { setGoto('form_start'); return; }\n\n",
	"	scriptVars['drTraining'] += 5;\n   { setGoto('form2'); return; }\n",
	"	scriptVars['d1Training'] += 5;\n   { setGoto('form2'); return; }\n",
	"	scriptVars['d2Training'] += 10;\n   { setGoto('form2'); return; }\n",
    ],
    "label" : [
	"form_start",
	"channelconfig",
	null,
	"stoptraining",
	"form2",
	null,
	"drtrain",
	"d1train",
	"d2train",
    ],
    "sourceFN" : [
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
	"Test6.gsc",
    ],
    "sourceLN" : [
	1,
	6,
	15,
	24,
	29,
	61,
	65,
	68,
	71,
    ]
};

const imageList = [
    { "dir" : ".", "files" : [ ] },
];

const videoList = [
    { "dir" : ".", "files" : [ ] },
];

const audioList = [
    { "dir" : ".", "files" : [ ] },
];
let baseDirectory=".";

