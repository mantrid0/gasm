'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	// user settings
        ccl: 0.5,	// current limitation at common
        vol: [],        // volume
	rl: [],		// recognition limit in Hz
	pj: 1,		// recognition limit in Hz
	// tease settings controlled by high level API
        drl: [],	// low frequncy duty ratio (rise interval counts as 0.5)
        drh: [],	// high frequency duty ratio (0.25..1)
        da: [],        	// difficulty 1 (controls high frequncy amplitude modulation frequncy )
        db: 0,       	// difficulty 2 (controls frequency of +/- 180° phase swaps)
        mdr: [],	// high frequency duty ratio modulation
        mda: [],	// difficulty 1 modulation
        mdb: 0,       	// difficulty 2 modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.vol[c] = 0.5;
        p.rl[c] = 1000;
        p.drl[c] = 0.5;
        p.drh[c] = 0;
        p.da[c] = 0;
        p.mdr[c] = 0;
        p.mda[c] = 0;
    }
}

async function loadEstimProcessor() {
    return audioContext.audioWorklet.addModule("processor5.js");
}

const estimControlCommon = false;

const estimHL1_wi_drl = 0.5;
const estimHL1_wi_drh = 0.5;
const estimHL1_wp_da = 0.7;
const estimHL1_wp_db = 0.3;

function estimHL1_getI() {
    let i = 0;
    const groups = scriptVars.estimGroups;
    const p = scriptVars.estimParams;
    for ( let g=0; g<=groups.length; g++ ) {
	const r = p.drl[g];
	if ( r>0.5 ) i+=(r-0.5)*2*estimHL1_wi_drl;
	i+= (Math.max(0.25, Math.min(0.5, p.drh[g])) + Math.max(0.25, Math.min(0.5, p.drh[g]+p.mdr[g])) - 0.5) * 2 * estimHL1_wi_drh ;
    }
    return i / groups.length;
}

function estimHL1_getP() {
    let r = 0;
    const groups = scriptVars.estimGroups;
    const p = scriptVars.estimParams;
    for ( let g=0; g<=groups.length; g++ ) {
	r += (Math.max(0.0, Math.min(1.0, p.da[g])) + Math.max(0.0, Math.min(1.0, p.da[g]+p.mda[g])))*0.5*estimHL1_wp_da
	   + (Math.max(0.0, Math.min(1.0, p.da[g])) + Math.max(0.0, Math.min(1.0, p.da[g]+p.mda[g])))*0.5*estimHL1_wp_db
    }
    return r / groups.length;
}

function estimHL1_resetI() {
    const groups = scriptVars.estimGroups;
    const p = scriptVars.estimParams;
    for ( let g=0; g<=groups.length; g++ ) {
	p += (Math.max(0.0, Math.min(1.0, p.da[g])) + Math.max(0.0, Math.min(1.0, p.da[g]+p.mda[g])))*0.5*estimHL1_wp_da
	   + (Math.max(0.0, Math.min(1.0, p.da[g])) + Math.max(0.0, Math.min(1.0, p.da[g]+p.mda[g])))*0.5*estimHL1_wp_db
    }
}
