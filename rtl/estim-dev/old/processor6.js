let instanceCount = 0;	
const maxChannels = 8;		// maximum number of output channels. Currently audio API only defines up to 5 full frequency channels.

let muteT = 0.0;		// last time muted
let dt2 = 0;

const carrierFrequency = 600;			// in Hz
const hfrmin = 0.25;				// minmum high frequncy duty ratio
const hfa0 = 1.0/0.75;				// frequency of high frequncy amplitude modulation at difficulity1 = 0 relative to detection limit
const hfp0 = 0.51*hfa0;				// frequency of high frequncy phase modulation at difficulity1 = 0 relative to detection limit
const hfx = 0.8;				// frequency of high frequncy modulations (phase and amplitude) if calculated by 
						// fa = fd * hfa0 * (1-r) / (1-rmin) * hfx^2 / (d+hfx)^2
						// fp = fdmax * hfp0 * hfx^2 / (d+hfx)^2
						// where d is the diffculity, fd is the detection limit and r is the duty ratio
						// smaller values causes stronger reduction
const riseTimeMin = 0.5;			// minimum rise time of slow amplitude modulation
const riseTimeMax = 1.5;			// maximum rise time of slow amplitude modulation
const modulationTimeFactor = 4.0;		// quotion of motulation time and rise time of slow amplitude modulation
const muteRamp = 0.5/riseTimeMax;		// reciprocal ramp-up time after mute in seconds
const setTimeMax = 0.5*riseTimeMin;		// maximum set time of slow amplitude modulation

// carrier waveform
let phase = 0;
let phaseShift = 0;
let fPhaseShift = 1;

// amplitude modulation
const maxVol = [];
let maxVolC = 0;

// low frequncy amplitude modulation
const timeA = [];		// amplitude waveform time
const tR = [];			// rise time

// high frequency amplitude modulation
let timeH = -1e-12;		// in periods at fhmax
const fhdiv = [];		// frequency dividers
const fh = [];			// frequency of high frequncy amplitude modulation
let fhmax = 50;

// high frequency phase modulation
let timeP = 0;			// in periods at fp

// parameter modulation
const timeM = [];		// modulation waveform times
const fM = [];			// 1/modulation time
let timeM2 = 2.0;		// modulation waveform times
let fM2 = 1;			// 1/modulation time

const mdrBuf = [];
const mdaBuf = [];
let mdbBuf = 0;
const difA = [];
let difB = 0;
const drh = [];

// waveform
const wfSize = 512;
const wfBuf = [];
for (let i=0; i<=wfSize; i++)
    wfBuf[i] = Math.sin(i/wfSize*2*Math.PI);

// re-randomize and init settings vor channel c
function reRandomize( c ) { 
    timeM[c] = 0.0;
    fM[c] = 1.0 / ( modulationTimeFactor*(riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin)) );
}

function reRandomize2() { 
    timeM2 = 0.0;
    fM2 = 1.0 / ( modulationTimeFactor*(riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin)) );
//    fPhaseShift = 0.5 / (riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin));
    fPhaseShift = 0.5;
}

class EstimProcessor extends AudioWorkletProcessor {
    instanceCount = 0;

    constructor ( options ) { 
        super( options );
        instanceCount++;
        this.instanceCount = instanceCount;
        console.log("Started Estim worker #" + this.instanceCount +". Sample rate: "+sampleRate+" Hz");
    
        muteT = currentTime;
	for ( let c=0; c<maxChannels; c++ )  {
	    reRandomize(c);
	    tR[c] = riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin);
	    timeM[c] = 2.0;
	    timeA[c] = 1.0e100;
	    fh[c] = fhmax;
	    fhdiv[c] = 1.0;
	    maxVol[c] = 0;
	}
	reRandomize2();
    } 
    
    static get parameterDescriptors() {
        let params = [
            {   // true number of channels
                name: "channels",
                defaultValue: 2,
                minValue: 2,
                maxValue: 5,
                automationRate: "k-rate",
            }, {  // mute if > 0.5
                name: "mute",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // current limitation at common
                name: "ccl",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 2,
                automationRate: "k-rate",
            }, {
    	    // difficulty 2
                name: "db",
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {
            // difficulty 2 modulation
                name: "mdb",
                defaultValue: 0,
                minValue: -1,
                maxValue:  1,
                automationRate: "k-rate",
            }, {
            // phase jump
                name: "pj",
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            } ];
        // per channel parameters for common (index 0) and up to maxChannels channels
        for ( let c=0; c<=maxChannels; c++ ) {
    	    // volume
            params[params.length] = {
                name: "vol"+c,
                defaultValue: 0.5,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // recognition limit in Hz
            params[params.length] = {
                name: "rl"+c,
                defaultValue: carrierFrequency/1.5001*hfrmin/hfa0,
                minValue: 20,
                maxValue: carrierFrequency/1.5*hfrmin/hfa0,
                automationRate: "k-rate",
            };
    	    // phase position
            params[params.length] = {
                name: "pp"+c,
                defaultValue: 0.5,
                minValue: -0.25,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // low frequency duty ratio
            params[params.length] = {
                name: "drl"+c,
                defaultValue: 0.5,
                minValue: 0.0,
                maxValue: 1.0,
                automationRate: "k-rate",
            };
    	    // high frequency duty ratio
            params[params.length] = {
                name: "drh"+c,
                defaultValue: hfrmin,
                minValue: hfrmin,
                maxValue: 0.9,
                automationRate: "k-rate",
            };
    	    // difficulty 1
            params[params.length] = {
                name: "da"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // high frequency duty ratio modulation
            params[params.length] = {
                name: "mdr"+c,
                defaultValue: 0,
                minValue: hfrmin-1.0,
                maxValue: 1.0-hfrmin,
                automationRate: "k-rate",
            };
    	    // difficulty 1 modulation
            params[params.length] = {
                name: "mda"+c,
                defaultValue: 0,
                minValue: -1,
                maxValue:  1,
                automationRate: "k-rate",
            };
        }
        return params;
    }
    
    process(inputs, outputs, parameters) {
        if ( instanceCount != this.instanceCount ) {
            console.log("Terminating Estim worker #" + this.instanceCount);
            return false;
         }

        const buf = outputs[0];
        const channels = Math.min(maxChannels, Math.min(buf.length, Math.round(parameters.channels[0])) );	// calculated channels
        const samples = buf[0].length;
//        console.log("samples: "+samples);
    	        
        // muted ?
        if ( parameters.mute[0]>0.5 ) {
    	    muteT = currentTime;
            for ( let c = 0; c<buf.length; c++ ) { 	
        	const b = buf[c];
    		for ( let i = 0; i < samples; i++) 
            	    b[i] = 0;
            }
            return true;
        }
        
        const pj = 0.5*Math.max(0, Math.min(1, parameters.pj[0]));
        const poffs = [];
        for ( let c=0; c<channels; c++ ) {
    	    poffs[c] = parameters["pp" + (c+1)][0];
    	}

    	// reduce modulation settings immediately
        for ( let c=0; c<channels; c++ ) {
    	    mdrBuf[c] = Math.min( mdrBuf[c], parameters["mdr" + (c+1)][0]);
    	    mdaBuf[c] = Math.min( mdaBuf[c], parameters["mda" + (c+1)][0]);
    	}
        mdbBuf = Math.min( mdbBuf, parameters["mdb"][0]);

	// samples        
        const dt = 1/sampleRate;
        for ( let i = 0; i < samples; i++) {
    	    dt2+=dt;
    	    if ( Math.floor(timeH*8) != Math.floor((timeH+dt2*fhmax)*8) ) {
		// mute ramp
    		const ma = Math.min(1, (currentTime-muteT+0.5*dt2)*muteRamp);

        	// parameter modulation
    		timeM2 += dt2*fM2;
		if ( timeM2 >= 1.0 ) {
		    reRandomize2();
		    // buffer modulation settings in order to avoid artifacts if parameter changes
    	    	    mdbBuf = parameters["mdb"][0];
    	    	}
    	    	difB = Math.max(0, Math.min(1, parameters["db"][0] + timeM2*mdbBuf));

		fhmax = 0.0;
		let rlmax = 0;
    		for ( let c=0; c<channels; c++ ) {
        	    // parameter modulation
    		    timeM[c] += dt2*fM[c];
		    if ( timeM[c] >= 1.0 ) {
			reRandomize(c);
			// buffer modulation settings in order to avoid artifacts if parameter changes
    	    		mdrBuf[c] = parameters["mdr" + (c+1)][0];
    	    		mdaBuf[c] = parameters["mda" + (c+1)][0];
    	    	    }
    	    	    difA[c] = Math.max(0, Math.min(1, parameters["da"+(c+1)][0] + timeM[c]*mdaBuf[c]));
    	    	    drh[c] = Math.max(hfrmin, Math.min(1, parameters["drh"+(c+1)][0] + timeM[c]*mdrBuf[c]));

		    // frequencies of high frequncy amplitude modulation
		    let r = difA[c]+hfx;
		    const rl =parameters["rl" + (c+1)][0];
		    rlmax = Math.max(rlmax, rl);
		    r =  rl * hfa0 * (1-drh[c])*hfx*hfx/((1-hfrmin)*r*r);
		    fh[c] = r;
		    fhmax = Math.max(fhmax,r);
		}
		
		// phase modulation
	        rlmax *= 2 * hfp0 * hfx*hfx / ((difB+hfx)*(difB+hfx));
	        timeP += rlmax*dt2;
	        console.log("fhmax="+fhmax+" fp2="+rlmax);
        	let r = phaseShift + fPhaseShift*dt2;
        	r -= Math.floor(r);
        	phaseShift = r;
		
		// calculate parameters of high frequency amplitude modulation and phase jump
		timeH += dt2*fhmax;
		if ( timeH - Math.floor(timeH) - dt2*fhmax <=0 ) {
		    if ( timeP > 1 ) {
			timeP = 0;
			phaseShift += pj;
		    }
		    let dd = 2;
    		    for ( let c=0; c<channels; c++ ) {
    			fhdiv[c] = Math.floor(fhmax/fh[c]);
		        dd *= fhdiv[c];
    		    }
    		    timeH = timeH % dd;
//    		    console.log("fhmax=" + fhmax + " timeH = " + timeH + "  dd=" + dd+"  dt2="+(dt2*fhmax)+"  fhdiv="+fhdiv);
    		}

		// amplitude modulation
		let vm = 0;
		maxVolC = 0;
    		for ( let c=0; c<channels; c++ ) {
		    // high frequency amplitude modulation
		    let sa = 0.0;
		    let s = drh[c];
    		    s = Math.max(s, Math.min(1, s*fhmax/(fhdiv[c]*fh[c])));
    		    let r = timeH / (2*fhdiv[c]);
    		    r = 2*(r-Math.floor(r));
    		    if ( r<s ) sa = 1.0;
    		    else if ( (r>=1) && (r<1+s) ) sa = Math.max(0, 2*drh[c]/s-1);
		    
		    // low frequency amplitude modulation
		    timeA[c] += dt2;
		    if ( sa>0.01 ) {
			if ( timeA[c] <= tR[c] ) sa *= timeA[c] / tR[c];
			else {
			    r = 2*parameters["drl" + (c+1)][0];
			    let tS = setTimeMax * Math.max(0, Math.min(1-2*difA[c],1-difB));
			    let s = 0.0;
			    if ( r>1 ) {
				r = 2-r;
				s = tR[c]+tS - timeA[c]*r;
			    }
			    else s = ( tR[c] + tS - timeA[c] )*r;
			    if ( s <= 0 ) {
				sa = 0;
				if ( r*timeA[c] > tR[c]+tS ) {
				    timeA[c] = 0.5*dt2 + 1/fh[c];
				    tR[c] = riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin);
				} 
			    }
			    else if ( s < tS*r ) sa *= s / (tS*r);
			}
		    }
    	    	    r = Math.max(0, Math.min(1, parameters["vol"+(c+1)][0]));
    	    	    r = Math.sqrt(r*ma*sa); 
    	    	    vm = Math.max(vm,r);
    	    	    maxVolC += r;
		    maxVol[c] = r;
		}
		r = Math.max(0, Math.min(2, parameters.ccl[0]));
		maxVolC = (r>1) ? ((2-r)*vm) : ( (1-r)*maxVolC + r*vm );

    		dt2 = 0;
	    }
        
    	    // carrier waveform
            phase += dt*carrierFrequency;
    	    phase -= Math.floor(phase);
            let cc = 0;	// current at common
            for ( let c = 0; c<channels; c++ ) {
        	// phase
        	let p = phaseShift + poffs[c];
        	p = 1 - 4*Math.abs(p-Math.round(p));
        	p = phase + 0.5*p;
        	p -= Math.floor(p);
        	// output
        	p = maxVol[c] * wfBuf[Math.round(p*wfSize)];
        	buf[c][i] = p;
        	cc += p;
    	    }
    	    if ( Math.abs(cc) > maxVolC ) {
    		cc = (cc-maxVolC*Math.sign(cc))/channels;
        	for ( let c = 0; c<channels; c++ )
        	    buf[c][i]-=cc;
    	    }
            for ( let c = channels; c<buf.length; c++ ) 
                buf[c][i] =  0;
        }
        return true;
    }
}

registerProcessor("estim-processor", EstimProcessor);
