form_start:
    #   formStart();					// starts form mode
    #   @channelConfig = encodeChannelConfig();		// converts current channel config to a string
    #   @channelConfigErr = false;			// no error

channelConfig:
    #   estimStop();					// stop audio
    <	<h2>Channel setup</h2>
    <<  Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and @maxEstimChannels. 
    <<  E.g. "1,1,1" for 3 independent channels or "2,1,1," for 4 channels where the first two ones have equal constraints.
    <<  <input type="text" id="channelConfig" value="@channelConfig">
    <<	Click on 'Done' in order to start audio with a 5s ramp-up time.
    TB  interrupt(); Done
    #   if ( @channelConfigErr ) appendText("Error: " + @channelConfigErr );
    D   

// checks syntax of the channel configuration string and goto previos code block in an error occurred
    #   @channelConfig = document.getElementById("channelConfig").value;
    #   @channelConfigErr = await decodeChannelConfig( @channelConfig );
    #   if ( @channelConfigErr ) @goto("channelConfig");
// forces to end code block in order to avoid printing the stuff below if an error occurred
:

stopTraining:
    #	@drTraining = 0;
    #	@d1Training = 0;
    #	@d2Training = 0;

form2:
    #   await estimPlay();				// start audio and initializes it if necessary 
    <   <h2>EStim parameters</h2>
    <<  <h3>User defined parameters</h3>
    <<	These parameters are user dependent and should not be set by the tease (author).
    #	appendEstimSlider("<b>Volume</b> per group. Should not be changed by the tease. Thus, maximum setting can and should be 1. Parameter name is 'vol1' for group 1, etc..", "vol");
    #	appendEstimSlider( "<b>Current limitation at common</b>. 0 means no limitation. 1 is the maximum feasible limitation and 2 is more or less (depending on output circuit and amplifier balance) equal to disconnecting common. Parameter name is 'ccl'. In order to calibrate it, set 'maximum phase shift' to 0 and choose it such that changing 'phase offsets' has on impact on intensity. " , "ccl" );
    #	appendEstimSlider( "<b>Recognition limit</b> per group in Hz. Start with the most sensitive electrode and reduce (starting from maximum) until you feel a change (should feel like tickling). Parameter name is 'rl'." , "rl" );
    #	appendEstimSlider( "<b>Phase position</b> per group in Hz. Start with the most sensitive electrode and reduce (starting from maximum) until you feel a change (should feel like tickling). Parameter name is pp'." , "pp" );
    #	appendEstimSlider( "<b>Phase jump</b> (for debugging)" , "pj" );
    <<  <h3>Tease settings</h3>
    <<	These parameters are the low level parameters changed by the tease. (This should be done using a high-level API becaouse low level API may change.)
    #	appendEstimSlider( "<b>Low frequncy duty ratio</b> per group. Parameter name is 'drl'." , "drl");
    #	appendEstimSlider( "<b>High frequncy duty ratio</b> per group. Parameter name is 'drh'." , "drh");
    #	appendEstimSlider( "<b>Difficulty 1</b> per group. This setting has a higher impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'da1' four group 1, ..." , "da" );
    #	appendEstimSlider( "<b>Difficulty 2</b> per group. This setting has a smaller impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'db1' for grop 1, etc.." , "db" );
    <<	<h3>Modulation</h3>
    <<	Some of the paremeters can be varied which is called parameter modulation.
    #	appendEstimSlider( "<b>High frequncy duty ratio modulation</b> per group. Parameter name is 'mdr1' for group 1, ..." , "mdr");
    #	appendEstimSlider( "<b>Difficulty 1 modulation</b> per group. Parameter name is 'mda1' for group 1, ..." , "mda");
    #	appendEstimSlider( "<b>Difficulty 2 modulation</b> per group. Parameter name is 'mdb1' for group 1, ..." , "mdb" );
    <<	<h2>Training</h2>
    <<  Click on the buttons to slowly increase various settings.
    <<	High frequncy duty ratio training: @drTraining percent points per minute<br>Difficulty 1 training: @d1Training percent points per minute<br>Difficulty 2 training: @d2Training percent points per minute
    #	changeEstimParam( "drh" , @drTraining/(60*100) );
    #	changeEstimParam( "da" , @d1Training/(60*100) );
    #	changeEstimParam( "db" , @d2Training/(60*100) );
    TB  interrupt(); Back
    TBG  stopTraining Stop Training
    TBG  drTrain Increase high frequncy duty ratio training by percent points per minute
    TBG  d1Train Increase difficulty 1 training by 5 percent points per minute
    TBG  d2Train Increase difficulty 2 training by 10 percent points per minute
    D  
    #   formEnd();
    G   form_start

drTrain:
    #	@drTraining += 5;
    G	form2
d1Train:
    #	@d1Training += 5;
    G	form2
d2Train:
    #	@d2Training += 10;
    G	form2
