'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	pw: 0.8,	// pulse width in ms
        it: 50,		// inhibition time in ms
        da: 0,        	// difficulty 1
        vf: 5,      	// variation frequency
        mt: 3,          // modulation interval in s
        mda: 0,		// difficulty 1 modulation
        dr: 1,		// duty ratio
        vv: 0,		// volume variation
        rt: 1,		// rise time in s
        // array parameters are saved per group with index 0 for common
        vol: [],        // volume
        mv: [],         // volume modulation
        db: [],       	// difficulty 2
        mdb: [],       	// difficulty 2 modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.vol[c] = 0.5;
        p.mv[c] = 0;
        p.db[c] = 0;
        p.mdb[c] = 0;
    }
}

async function loadEstimProcessor() {
   return audioContext.audioWorklet.addModule("processor2.js");
}

const estimControlCommon = true;
