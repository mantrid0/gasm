'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	pw: 0.8,	// pulse width in ms
        it: 50,		// inhibition time in ms
        ccl: 0.5,	// current limitation at common
        po: 2.0/3.0,	// phase offsets
        vf: 10,      	// variation frequency
        mt: 3,          // modulation interval in s
        dr: 1,		// duty ratio
        vv: 0,		// volume variation
        ps: 1,       	// maximum phase shift
        rt: 1,		// rise time in s
        // array parameters are saved per group with index 0 for common
        vol: [],        // volume
        mv: [],         // volume modulation
        da: [],        	// difficulty 1
        mda: [],	// difficulty 1 modulation
        db: [],       	// difficulty 2
        mdb: [],       	// difficulty 2 modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.vol[c] = 0.5;
        p.mv[c] = 0;
        p.da[c] = 0;
        p.mda[c] = 0;
        p.db[c] = 0;
        p.mdb[c] = 0;
    }
}

async function loadEstimProcessor() {
    return audioContext.audioWorklet.addModule("processor4.js");
}

const estimControlCommon = false;
