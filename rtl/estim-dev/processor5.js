let instanceCount = 0;	
const maxChannels = 8;		// maximum number of output channels. Currently audio API only defines up to 7 full frequency channels.

let muteT = 0.0;		// last time muted
let dt2 = 0;

const fcmin = 300;				// minimum carrier frequency 
const rlm = 50*2/3;				// recognition limit is chosen between rlm (sensitivity=0) and 2*rlm (sensitivity=1). rlm*2 must not be larger than fcmin/4
const riseTimeMin = 0.5;			// minimum rise time of slow amplitude modulation
const riseTimeMax = 1.5;			// maximum rise time of slow amplitude modulation
const muteRamp = 0.5/riseTimeMax;		// reciprocal ramp-up time after mute in seconds
const fPhaseShiftMax = 2 / riseTimeMin;	// (slow) phase shift frequencies is randomized between +/-fPhaseShiftMax such that maximum amplitude modulation frequency of mixed signals is twice that parameter
const setTimeMax = 0.5*riseTimeMin;		// maximum set time of slow amplitude modulation
const fpmax = 15000;				// maximum frequency in Hz for volume reduction + increased penetration depth
const vce = 0.5;				// volume compensation exponent

// carrier waveform
let phase = 0;
let phaseP = 0;
const phaseShift = [];
const fPhaseShift = [];

// high frequency phase modulation
const phaseShift2 = [];

// amplitude modulation
const maxVol = [];
let maxVolC = 0;

// low frequency amplitude modulation: per channel
const phaseA = [];		// amplitude waveform phase
const fRA = [];			// rise frequency (reciprocal rise rime)

// low frequency amplitude modulation: all channels
let phaseB = 0;			// amplitude waveform phase
let fRB = 1.5;			// rise frequency (reciprocal rise rime)
let TL = 0;			// low time relative to rise time

// high frequency amplitude modulation
const LN4 = Math.log(4);
const LN2 = Math.log(2);
let timeH = 2;			// in periods at fhmin
let fhmin = 50;			// lowest modulation frequency
let fhmindiv = 10;		// frequency dividers of lowest modulation relative to carrier frequency
const fhmul = [];		// frequency multiplier relative to fhmin
const drh1 = [];		// duty ratio 1
const drh2 = [];		// duty ratio 2
const ah2 = [];			// amplitude 2
const vcf = [];			// volume compensation factor
let chmin = 0;			// no phase jumps on slowest channel

// parameter modulation
let timeM = 2.0;		// modulation waveform times
let fM = 0.3;			// 1/modulation time
const mvol = [];

// waveform
const wfSize = 512;
const wfBuf = [];
for (let i=0; i<=wfSize; i++)
    wfBuf[i] = Math.cos(i/wfSize*2*Math.PI);


class EstimProcessor extends AudioWorkletProcessor {
    instanceCount = 0;

    constructor ( options ) { 
        super( options );
        instanceCount++;
        this.instanceCount = instanceCount;
        console.log("Started Estim worker #" + this.instanceCount +". Sample rate: "+sampleRate+" Hz");
    
        muteT = currentTime;
	for ( let c=0; c<maxChannels; c++ )  {
	    phaseShift[c] = Math.random();
	    fPhaseShift[c] = fPhaseShiftMax*(2*Math.random()-1);
	    phaseShift2[c] = 0;
	    maxVol[c] = 0;
	    phaseA[c] = 0;
	    fRA[c] = 1/(riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin));
	    fhmul[c] = 1;
	    drh1[c] = 0.5;
	    drh2[c] = 0.5;
	    ah2[c] = 0;
	    vcf[c] = 0.5;
	    mvol[c] = 0;
	}
    } 

    static get parameterDescriptors() {
        let params = [
            {   // true number of channels
                name: "channels",
                defaultValue: 2,
                minValue: 2,
                maxValue: 5,
                automationRate: "k-rate",
            }, {  // mute if > 0.5
                name: "mute",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // carrier frequency
                name: "fc",  
                defaultValue: 400,
                minValue: fcmin,
                maxValue: 1500,
                automationRate: "k-rate",
            }, {  // current limitation at common
                name: "ccl",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 2,
                automationRate: "k-rate",
            }, {  // minimum parameter modulation interval
                name: "tma",  
                defaultValue: 2,
                minValue: 0.5,
                maxValue: 20,
                automationRate: "k-rate",
            }, {  // maximum parameter modulation interval
                name: "tmb",  
                defaultValue: 5,
                minValue: 0.5,
                maxValue: 20,
                automationRate: "k-rate",
            }, {  // log10 of phase shift frequency multiplier
                name: "fps",  
                defaultValue: 0,
                minValue: -1,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // low frequency duty ratio
                name: "drl",  
                defaultValue: 1,
                minValue: 0.05,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // total limit
                name: "tl",  
                defaultValue: 0.5,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {
    	    // difficulty 2
                name: "db",
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // total limit modulation
                name: "mtl",  
                defaultValue: 0.5,
                minValue: -1,
                maxValue: 1,
                automationRate: "k-rate",
            }, {
            // difficulty 2 modulation
                name: "mdb",
                defaultValue: 0,
                minValue: -1,
                maxValue:  1,
                automationRate: "k-rate",
            } ];
        // per channel parameters for common (index 0) and up to maxChannels channels
        for ( let c=0; c<=maxChannels; c++ ) {
    	    // channel balance
            params[params.length] = {
                name: "cb"+c,
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // electrode sensitivity
            params[params.length] = {
                name: "sens"+c,
                defaultValue: 0.5,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // volume
            params[params.length] = {
                name: "vol"+c,
                defaultValue: 0.5,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 1
            params[params.length] = {
                name: "da"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // volume modulation
            params[params.length] = {
                name: "mvol"+c,
                defaultValue: 0,
                minValue: -0.5,
                maxValue:  0.5,
                automationRate: "k-rate",
            };
    	    // difficulty 1 modulation
            params[params.length] = {
                name: "mda"+c,
                defaultValue: 0,
                minValue: -1,
                maxValue:  1,
                automationRate: "k-rate",
            };
        }
        return params;
    }
    
    process(inputs, outputs, parameters) {
        if ( instanceCount != this.instanceCount ) {
            console.log("Terminating Estim worker #" + this.instanceCount);
            return false;
         }

        const buf = outputs[0];
        const channels = Math.min(maxChannels, Math.min(buf.length, Math.round(parameters.channels[0])) );	// calculated channels
        const samples = buf[0].length;
//        console.log("samples: "+samples);
    	        
        // muted ?
        if ( parameters.mute[0]>0.5 ) {
    	    muteT = currentTime;
            for ( let c = 0; c<buf.length; c++ ) { 	
        	const b = buf[c];
    		for ( let i = 0; i < samples; i++) 
            	    b[i] = 0;
            }
            return true;
        }
        
    	// reduce modulation settings immediately
    	const cb = [];
        for ( let c=0; c<channels; c++ )  {
    	    mvol[c] = Math.min( mvol[c], parameters["mvol" + (c+1)][0]);
    	    cb[c] = 2*Math.min(1.0, Math.max(0.0, parameters["cb" + (c+1)][0])) - 1;
    	}
        const fc = parameters["fc"][0];
        const fp = 0.5 / Math.ceil(0.5*sampleRate/fpmax);
        
        const drl = parameters["drl"][0];
        TL = Math.max( Math.min(phaseB,TL), 1.5/Math.max(0.01,drl)-2 );
        const TLRH = TL + 1 + Math.max(1, (drl-0.5)/Math.max(1e-12,1-drl) );
//        console.log("phaseB="+phaseB+"  TL="+TL+"  TLRH="+TLRH);

	// samples        
        const dt = 1/sampleRate;
        for ( let i = 0; i < samples; i++) {

    	    phaseB += dt*fRB;
	    if ( phaseB <= TL ) {
        	for ( let c = 0; c<buf.length; c++ ) 
            	    buf[c][i] =  0;
            	continue;
    	    }

    	    dt2+=dt;
    	    if ( Math.floor(timeH*fhmindiv) != Math.floor((timeH+dt2*fhmin)*fhmindiv) ) {
		// mute ramp and drl modulation
    		const ma = Math.min(1, (currentTime-muteT+0.5*dt2)*muteRamp, phaseB-TL);
		if (phaseB > TLRH ) {
		    phaseB = 0;
		    fRB = 0.65/(riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin));
		}

        	// parameter modulation
    		timeM += dt2*fM;
		if ( timeM >= 1.0 ) {
		    const fm  = Math.pow(10,parameters["fps"][0])*fPhaseShiftMax;
		    const tma = parameters["tma"][0];
		    const tmb = parameters["tmb"][0];
    		    for ( let c=0; c<channels; c++ ) {
			fPhaseShift[c] = fm*(2*Math.random()-1);
			fM = 1 / ( tma + Math.random()*(tmb-tma) );
			// buffer modulation settings in order to avoid artifacts if parameter changes
    	    		mvol[c] = parameters["mvol" + (c+1)][0];
    	    	    }
    	    	}
    	    	timeM -= Math.floor(timeM);
    	    	const pm = 0.5*(1-wfBuf[Math.round( timeM*wfSize )]);
    	    	const difB = Math.max(0, Math.min(1, parameters["db"][0] + pm*parameters["mdb"][0]));
    	    	const tl = Math.max(0, Math.min(1, parameters["tl"][0] + pm*parameters["mtl"][0]));
		const vol = [];
		const difA = [];

    		for ( let c=0; c<channels; c++ ) {
    	    	    vol[c]  = Math.max(0, Math.min(1, parameters["vol"+(c+1)][0] + pm*mvol[c]));
    	    	    difA[c] = Math.max(0, Math.min(1, parameters["da" +(c+1)][0] + pm*parameters["mda"+(c+1)][0]));

		    // slow phase modulation
        	    let r = phaseShift[c] + fPhaseShift[c]*dt2;
        	    r -= Math.floor(r);
        	    phaseShift[c] = r;
		}
	
		// calculate parameters of high frequency amplitude modulation
		const timeHPrev = timeH;
		timeH += dt2*fhmin;
		if ( timeH > 2 ) {
		    timeH -= 2*Math.floor(0.5*timeH);
		    const fhdiv = [];
		    let dmax = 0;
		    let dmin = 1e12;
		    chmin = 0;
    		    for ( let c=0; c<channels; c++ ) {
			// frequency divider of high frequency amplitude modulation
			let d = Math.exp(LN4*difA[c]);
			const e = Math.max(1, 0.5*d);	// 1..2
			vcf[c] = Math.exp( (Math.log(e) - LN2)*vce );
			drh1[c] = 0.5 / e;
			d *= fc / (rlm*(1+parameters["sens" + (c+1)][0]));
			fhdiv[c] = d;
			const dt = Math.floor(d);
			if ( d > dmax ) {
			    dmax = dt;
			    chmin = c;
			}
			dmin = Math.min(dmin, dt);
		    }
		    dmin = Math.trunc(dmin);
		    dmax = Math.trunc(dmax) + 1e-6;
		    let c = 2;
		    while ( dmin*c<=dmax ) 
			c *= 2;
		    fhmindiv = 0.5 * Math.ceil( fhdiv[chmin] / c ) * c;
		    const rfhmindiv = 1.0 / fhmindiv;
		    fhmin = fc / fhmindiv;
//		    let dstr = "fhmindiv=" + fhmindiv;
    		    for ( let c=0; c<channels; c++ ) {
    			let r = 1;
    			const d = fhdiv[c];
    			while ( d*r < fhmindiv ) 
    			    r*=2;
    			fhmul[c] = r;
//			r = drh1[c] * d*r/fhmindiv;	// corrected duty ratio
//			r *= fhmindiv/r;		// high time in fc periods
			r*=rfhmindiv;			// = 1/d', d' <= d <= 2*d'; 1 <= d*r <= 2
			const h = drh1[c] * d;		// high time in fc periods
			drh1[c] = Math.ceil(h)*r;	// must be rounded to full periods in order to avoid low frequency components
			drh2[c] = Math.floor(h)*r;
			ah2[c] = Math.sqrt( 2 / (r*d)-1 );
//			dstr += "    ch"+c+": d'= "+(1/r)+" d="+d+" mul="+fhmul[c]+" drh1="+drh1[c]+" drh2="+drh2[c]+" ah2="+ah2[c]+" h="+h;
    		    }
//    		    console.log(dstr);
    		}

		// amplitude modulation
		let vm = 0;
		maxVolC = 0;
		let frs = 0;
		let as = 0;
		let cm = 0;
		let am = 0;
    		for ( let c=0; c<channels; c++ ) {
		    // high frequency amplitude modulation
    		    let r = 0.5 * timeH * fhmul[c];
    		    r = 2*(r-Math.floor(r));
		    let sa = 0.0;
    		    if ( r < drh1[c] ) sa = 1.0;
    		    else if ( (r>=1) && (r<=1+drh2[c]) ) sa = ah2[c];
    		    
		    // high frequency phase modulation
		    if ( (sa>0.75) && (Math.floor(timeH * fhmul[c])!=Math.floor(timeHPrev*fhmul[c])) && (c!=chmin) ) {
			phaseShift2[c] = phaseShift2[c] > 0 ? -0.25*difB : 0.25*difB;
		    }

		    // low frequency amplitude modulation
		    r = fRA[c];
		    let s = phaseA[c] + dt2*r;
		    if ( s > am ) {
			am = s;
			cm = c;
		    }
		    if ( s >= 1) {
			phaseA[c] = s;
			s = 1;
		    }
		    else {
			if ( s <= 0 ) {
			    s = 0;
			    r = 1/(riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin));
			    fRA[c] = r;
			}
			frs += r;
			phaseA[c] = s;
		    }
		    as += s;

    	    	    r = Math.sqrt( vol[c] * ma * vcf[c] * s);
    	    	    vm = Math.max(vm,r);
    	    	    maxVolC += r;
		    maxVol[c] = r*sa;
		}
		let r = Math.max(0, Math.min(2, parameters.ccl[0]));
		maxVolC = (r>1) ? ((2-r)*vm) : ( (1-r)*maxVolC + r*vm );
		
		if ( (as>channels*tl) && (as>1e-12) ) {
		    r = Math.sqrt(channels*tl / Math.max(1e-10, as));
		    as = 0;
		    for ( let c=0; c<channels; c++ ) 
			maxVol[c] *= r;
		    maxVolC *= r;
		    if ( am > 0 ) {
			fRA[cm] = ( frs < 0.01 ) ? -1/(riseTimeMin+Math.random()*(riseTimeMax-riseTimeMin)) : 
			    ( ( ( phaseA[cm] >= 1 ) ? 0 : fRA[cm] ) - frs );
			phaseA[cm] = Math.min(am, 1);
		    }
		}
		
    		dt2 = 0;
	    }
        
    	    // carrier waveform
            phase += dt*fc;
    	    phase -= Math.floor(phase);
    	    phaseP += fp;
    	    phaseP -= Math.floor(phaseP);
            let cc = 0;	// current at common
            for ( let c = 0; c<channels; c++ ) {
        	// phase
        	let p = phase + phaseShift[c] + phaseShift2[c];
        	p = wfBuf[Math.round( (p-Math.floor(p))*wfSize )];
        	// output
        	p *= maxVol[c];
        	if ( phaseP > 0.49 ) p*=cb[c]
        	buf[c][i] = p;
        	cc += p;
    	    }
    	    if ( Math.abs(cc) > maxVolC ) {
    		cc = ( (cc>0) ? (cc-maxVolC) : (cc+maxVolC) ) / channels;
        	for ( let c = 0; c<channels; c++ )
        	    buf[c][i]-=cc;
    	    }
            for ( let c = channels; c<buf.length; c++ ) 
                buf[c][i] =  0;
        }
        return true;
    }
}

registerProcessor("estim-processor", EstimProcessor);
