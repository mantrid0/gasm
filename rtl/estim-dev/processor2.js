let instanceCount = 0;	
const maxChannels = 5;		// maximum number of output channels. Currently audio API only defines up to 5 full frequency channels.
const pulsePeriod = 2.5;	// pulse period in pulse width, >2
const muteRamp = 1/5;		// reciprocal ramp-up time after mute in seconds

let debugCnt = 0;
const pulseBuffer = [];		// current pulse, index 0 is only used during calculation
let pulseSample = 0;            // sample count within current pulse period
let samplesPerPulse = -1; 	// updated if pulse is calculated, i.e. if pulseSample > samplesPerPulse*pulsePeriod
let muteT = 0.0;		// last time muted

// modulation variables
let timeM = 2.0;		// modulation waveform time
const mvBuf = [];
let mdaBuf = 0;
const mdbBuf = [];

// high frequency electrode current variations
let timeE = 2.0;		// in periods
let timeE2 = 0;			// in s
const evf = [];			// relative variation frequency (1..2)
const evpx = [], evpp = [];	// variation phase (period is 1)
let hwe = 0.25;			// half relative high time

// slow amplitude variation
let timeA = 2.0;		// amplitude waveform time
let noVar = true;		// no variation
let tph = 0.5;			
let rtr	= 2;			

// pulse patterns
const ppat3 = [
	0,1,  2,3,
	0,2,  1,3,
	1,2,  0,3
    ];

const ppat5 = [
	0,3,  1,4,  2,5,
	0,4,  2,3,  1,5,
	0,1,  2,4,  3,5,
	0,2,  1,3,  4,5,
	1,2,  3,4,  0,5,
	0,3,  2,4,  1,5,
	0,2,  1,4,  3,5,
	0,1,  3,4,  2,5,
	1,4,  2,3,  0,5,
	0,4,  1,3,  2,5,
	0,3,  1,2,  4,5,
	0,2,  3,4,  1,5,
	1,3,  2,4,  0,5,
	0,1,  2,3,  4,5,
	0,4,  1,2,  3,5
    ];

let patCnt = 0;

// random integer between 0 an max-1
function randomInt(max) {
  return Math.floor(Math.random() * max);
}

/*
// calculate average electrode correlation
// n: number of electrodes (including common)
// w: electrode duty ratio (0..1)
// s: max phase shift (0..1)
function getC(n,w,s) {
    const res = 100000;	// simulation resolution
    const ps = [];
    const q = 1/(res*(n+1));
    let r=0;
    w*=0.5;
    for ( let k=0; k<res; k++ ) {
	for ( let i=0; i<=n; i++ ) {
//	    const x = (k+i*res)*q;
	    // x -= Math.floor(x);
//	    ps[i] = 0.5*s * ( (x>0.5) ? (1-4*x) : (4*x-3) );
	    ps[i] = s * (Math.random()-0.5);
	}
	for ( let i=0; i<n; i++ )
	    for ( let j=i+1; j<=n; j++ ) {
		const d = ps[i]-ps[j];
//		const d = Math.random();
		let a = d-w;	a -= Math.round(a);
		let b = d+w;	b -= Math.round(b);
		r += (b<=a) ? ( Math.max(0,b+w) + Math.max(0,w-a) ) : Math.max(0,Math.min(w,b) - Math.max(-w,a)) ;
	    }
    }	
    return r*2 / (res*(n+1)*n);
}

// solve getC(n,w,s)=0.25 for w 
// n: number of electrodes (including common)
// s: max phase shift (0..1)
function solveW(n,s) {
    let w = 0.25+s*(0.30+s*(0.18-0.23*s)); 	// error of this approximation is less than 1%
//    for (let j=0; j<3; j++)
//        w *= 0.5*(1+0.25/getC(n,w,s));	// does not converge because getC is random 
    return w;
}

for ( let n=3; n<=6; n++ ) {
    for ( let si=0; si<=20; si++ )  {
	const s = si*0.05;
	const w = solveW(n,s);
	console.log("n=" + n + " w=" + w.toFixed(4) + " s=" + s.toFixed(4) + ":  err=" + ((getC(n,w,s)-0.25)*400).toFixed(5) +"%") ;
    }
    console.log();
}  */

class EstimProcessor extends AudioWorkletProcessor {
    instanceCount = 0;

    constructor ( options ) { 
        super( options );
        instanceCount++;
        this.instanceCount = instanceCount;
        console.log("Started estim worker #" + this.instanceCount);
    
        pulseSample = 0;
        samplesPerPulse = -1;       
        muteT = currentTime;
	timeM = 2.0;
	timeE = 2.0;
	timeA = 2.0;
	for (let c=0; c<=maxChannels; c++ )
	    evpx[c] = Math.random();
    } 
    
    static get parameterDescriptors() {
        let params = [
            {   // true number of channels
                name: "channels",
                defaultValue: 2,
                minValue: 2,
                maxValue: 5,
                automationRate: "k-rate",
            }, {  // mute if > 0.5
                name: "mute",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // pulse width in ms
                name: "pw",  
                defaultValue: 0.8,
                minValue: 1000/sampleRate,
                maxValue: 2,
                automationRate: "k-rate",
            }, {  // inhibition time in ms
                name: "it",  
                defaultValue: 50,
                minValue: 25,
                maxValue: 100,
                automationRate: "k-rate",
            }, {  // variation frequency
                name: "vf",  
                defaultValue: 5,
                minValue: 0,
                maxValue: 25,
                automationRate: "k-rate",
            }, {  // modulation time in seconds
                name: "mt",  
                defaultValue: 3,
                minValue: 1,
                maxValue: 10,
                automationRate: "k-rate",
            }, {  // difficulty 1
                name: "da",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // difficulty 1 modulation 
                name: "mda",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // duty ratio
                name: "dr",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // volume variation
                name: "vv",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // rise time
                name: "rt",
                defaultValue: 1,
                minValue: 0,
                maxValue: 2,
                automationRate: "k-rate",
            } ];
        // per channel parameters for common (index 0) and up to maxChannels channels
        for ( let c=0; c<=maxChannels; c++ ) {
    	    // volume
            params[params.length] = {
                name: "vol"+c,
//                defaultValue: c<1 ? 0.5 : 1,
                defaultValue: 0.35,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // volume modulation
            params[params.length] = {
                name: "mv"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 2
            params[params.length] = {
                name: "db"+c,
                defaultValue: 0,
                minValue: -1,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 2 modulation
            params[params.length] = {
                name: "mdb"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
        }
        return params;
    }

    process(inputs, outputs, parameters) {
        if ( instanceCount != this.instanceCount ) {
            console.log("Terminating Estim worker #" + this.instanceCount);
            return false;
         }

        const buf = outputs[0];
        const channels = Math.min(maxChannels, Math.min(buf.length, Math.round(parameters.channels[0])) );	// calculated channels
        const samples = buf[0].length;
    	        
        // muted ?
        if ( parameters.mute[0]>0.5 ) {
    	    muteT = currentTime;
    	    pulseSample = 0;
    	    samplesPerPulse = -1;       
    	    muteT = currentTime;
            for ( let c = 0; c<buf.length; c++ ) { 	
        	const b = buf[c];
    		for ( let i = 0; i < samples; i++) 
            	    b[i] = 0;
            }
            return true;
        }

	// read parameters
	const pw = Math.max(1000/sampleRate, parameters.pw[0])*1e-3;	// pulse width in seconds
	const ti = Math.max(25, parameters.it[0])*1e-3;			// inhibition time in seconds
	const fm = 1.0 / Math.max(1, parameters.mt[0]);			// modulation frequency
	const fpv = parameters.vf[0];					// phase variation frequency
	const da = Math.max(0, Math.min(1, parameters.da[0]));		// difficulty 1
	mdaBuf = Math.min(mdaBuf, Math.max(0, parameters.mda[0]));	// reduce it immediately
        const vol = [];							// volume
        const db = [];							// difficulty 2
        for ( let c=0; c<=channels; c++ ) {
    	    vol[c] = Math.max(0, Math.min(2,parameters["vol"+c][0]));
	    db[c] = 0.5 + Math.max(-1, Math.min(1, parameters["db"+c][0]))*0.5;		// 0..1
    	    mvBuf[c] = Math.min(mvBuf[c],  Math.max(0, parameters["mv" +c][0]));	// reduce it immediately
    	    mdbBuf[c] = Math.min(mdbBuf[c],  Math.max(0, parameters["mdb" +c][0]));	// reduce it immediately
    	}
    	// slow amplitude variation
    	const vv = Math.max(1e-9, Math.min(1, parameters.vv[0]));		// slow volume variation
    	const dr = Math.max(1e-9, Math.min(1, parameters.dr[0]));		// duty ratio
    	const atr = Math.max(1e-9, parameters.rt[0]);				// rise time in s
    	const ath = Math.min(10, 2*ti+atr*(1/vv-1));				// high time
    	const atl = Math.max(2*ti-atr, Math.min(20, (atr+ath)*(1/dr-1)));	// low time
    	const fa = 1 / (atr+ath+atl);						// frequency
        
        for ( let i = 0; i < samples; i++) {
        
            if ( pulseSample >= samplesPerPulse*pulsePeriod ) {
//    		const debug = (debugCnt==0) || (debugCnt==15) || (debugCnt==30);
    		const debug = false;
                const ba = [];
                let dt = pulseSample/sampleRate;
                
//                if ( debug ) console.log("fm="+fm+ "  fpv="+fpv+ "  dt="+dt+"  we="+(hwe*2)  );
                if ( debug ) console.log("atr="+atr+ "  ath="+ath+ "  atl="+atl+"  fa="+fa+"  noVar="+noVar  );

        	// modulation waveform
    		timeM += dt*fm;
    		if ( timeM >= 1 ) {
    		    timeM = 0;
    		    // update at start of new sequence in order to avoid unwanted effects if parameter changes
    		    // also re-randomize electrode variation frequencies once per modulation period
    		    for ( let c=0; c<=channels; c++ ) {
    	    		mvBuf[c] = Math.max(0, Math.min(2,parameters["mv" +c][0]));
    	    		mdbBuf[c] = Math.max(0, Math.min(1,parameters["mdb" +c][0]));
    	    		evf[c] = 1 + Math.random();
    	    	    }
    	    	    mdaBuf = Math.max(0, Math.min(1,parameters.mda[0]));
    		}
		
		// mute ramp and slow volume modulation
		const ma = Math.min(1, (currentTime-muteT)*muteRamp);
		
		// slow volume modulation
		timeA += dt*fa;
		if ( timeA > 1 ) {
		    const T = atr+ath+atl;
		    tph = (atr+ath)/T;
		    rtr = T / atr;
		    timeA = 0;
		}
		const sa = noVar ? 1 : (timeA>tph) ? 0 : Math.min(1,(timeA+0.5*dt)*rtr);
		if ( sa>0.99 ) noVar = (vv<0.005) && (dr>0.995);	// avoid jumps

                // electrode currents
                const d1 = da + timeM*mdaBuf*(1-da);
		timeE += dt / ( 2*ti*(0.2 + Math.min(0.5,d1)*1.6) );
		timeE2 += dt;
		if ( timeE > 1 ) {		// update phases once per period in minimize to avoid artifacts
		    timeE -= Math.floor(timeE);
		    timeE2 *= fpv;
		    let se = Math.min(2-d1*2,1)*0.5;		// maximum phase shift (*0.5)
		    hwe = 0.125+se*(0.30+se*(0.18-0.23*se));	// relative high time *0.5 , see solveW above
		    se*=0.5;
		    
    		    for ( let c=0; c<=channels; c++ ) {
    			let p = evpx[c] + timeE2*evf[c];
    			p -= Math.floor(p);
    			evpx[c] = p
			evpp[c] = se * ( (p>0.5) ? (1-4*p) : (4*p-3) );		
		    }
		    timeE2 = 0;
		}
		const maxVol = [];		// volume parameter, updated per pulse period
    		for ( let c=0; c<=channels; c++ ) {
    		    let a = vol[c]*(1+timeM*mvBuf[c])*sa;
    		    let p = evpp[c]-timeE;
    		    p -= Math.round(p);
		    if ( Math.abs(p) > hwe ) {
			let d2 = db[c];
			d2 += timeM*mdbBuf[c]*(1-d2);
			a*=(1-d2);
		    }
		    maxVol[c] = Math.sqrt(a*ma);
//		    maxVol[c] = a;
		    
    		    pulseBuffer[c] = 0;
    		}
    		if ( debug ) console.log("maxVol=[" + maxVol + "]    evpp=[" + evpp + "]");

                // calculate pulse
                const pulseBuffer2 = [];
                for ( let c=0; c<=channels; c++)
            	    pulseBuffer2[c] = 0;
                // 1: find pairs
                if ( (channels<2) || (channels>5) ) {	// random fallback
            	    for ( let c=1; c<=channels; c+=2 ) {
    			let c1 = randomInt(channels+1);
    			while ( ba[c1]>0 )
    			    c1 = (c1+1) % (channels+1);
    			ba[c1] = 1;
    			let c2 = randomInt(channels+1);
    			while ( ba[c2]>0 )
    			    c2 = (c2+1) % (channels+1);
    			ba[c2] = 1;
    			const v = Math.min(maxVol[c1], maxVol[c2]);
    			pulseBuffer[c1] =  v;
    			pulseBuffer[c2] = -v;
    			if ( c2 == 0 ) pulseBuffer2[c1] =  v;
    			if ( c1 == 0 ) pulseBuffer2[c2] = -v;
    		    }
    		} 
    		else {	// use optimized pulse pattern
            	    patCnt = (patCnt + 1 ) % 60;	// period is 6=3*2 for ppat3 and 60=15*4 for ppat5
            	    let pat = [], pol = [], o = 0;
    		    if ( channels < 4 ) {
    			pat = ppat3;
    			o = (patCnt % 3)*4;
			pol = [0, patCnt & 1];
		    }
    		    else {
    			pat = ppat5;
    			o = (patCnt % 15)*6;
			pol = [0, patCnt & 1, (patCnt>>1) & 1];
		    }
		    for ( let i=0; i<pol.length; i++ ) {
            		const c1 = pat[o+i*2];
            		const c2 = pat[o+i*2+1];
            		let v = ( (c1<=channels) && (c2<=channels) ) ? Math.min(maxVol[c1], maxVol[c2]) : 1e-5;
            		v*=1-2*pol[i];
    			pulseBuffer[c1] =  v;
    			pulseBuffer[c2] = -v;
    			if ( c2 == 0 ) pulseBuffer2[c1] =  v;
    			if ( c1 == 0 ) pulseBuffer2[c2] = -v;
    		    }
            	}
                // find channels which can be optimized, save them in random order
                const oi = [];	// index
                const od = [];	// difference
    		for ( let c=0; c<=channels; c++ ) {
    		    let c1 = randomInt(channels+1);
    		    while ( ba[c1]>2 )
    			c1 = (c1+1) % (channels+1);
    		    ba[c1] = 3;
    		    let r = pulseBuffer[c1];
    		    let s = Math.sign(r);
    		    if ( s == 0 ) s = 2*randomInt(2)-1;
    		    r = maxVol[c1]*s - r;
    		    if ( r*s > 1e-3 ) {
    			const j = oi.length;
    			oi[j] = c1;
    			od[j] = r;
    		    }
    		}
    		if ( debug ) console.log("pulseBuffer=[" + pulseBuffer + "]  oi=[" + oi + "]  od=[" + od +"]  debugCnt="+debugCnt);
                // maximize volume
    		for ( let i=0; i<oi.length-1; i++) {
    		    const c1 = oi[i];
    		    let r = od[i];
    		    const s = Math.sign(r);
    		    let j=i+1
    		    while ( (j<oi.length) && (r*s>1e-3) ) {
    			let d = Math.min(-od[j]*s, r*s);
    			if ( d > 0 ) {
    			    const c2 = oi[j];
    			    d*=s;
    			    pulseBuffer[c1] += d;
    			    pulseBuffer[c2] -= d;
    			    if ( c2 == 0 ) pulseBuffer2[c1] += d;
    			    if ( c1 == 0 ) pulseBuffer2[c2] -= d;
    			    r -= d;
    			    od[j] += d;
    			}
    			j++;
    		    }
    		}    
//                for ( let c=1; c<=channels; c++)
//            	    pulseBuffer[c] = 0.5*(pulseBuffer[c]+pulseBuffer2[c]);
/*            	const r = pulseBuffer[0]/channels;
            	const s = channels / (channels+1);
                for ( let c=1; c<=channels; c++)
            	    pulseBuffer[c] = (pulseBuffer[c]-r)*s;*/
            	    
//    		if ( debug ) console.log("pulseBuffer=[" + pulseBuffer +"]  t=" + (currentTime-muteT));
    		debugCnt=(debugCnt+1)%999;

		// width of next pulse		
                samplesPerPulse = Math.round(sampleRate*pw);
                pulseSample = 0;

            }
            
            const s = (pulseSample>=2*samplesPerPulse) ? 0 : (pulseSample>=samplesPerPulse) ? -1 : 1;
            pulseSample++;
            
            for ( let c = 0; c<buf.length; c++ )
                buf[c][i] = (c<channels) ? s * pulseBuffer[c+1] : 0;
        }
        return true;
    }
}

registerProcessor("estim-processor", EstimProcessor);
