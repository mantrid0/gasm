'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	pw: 0.8,	// pulse width in ms
        it: 50,		// inhibition time in ms
        ccl: 0.5,	// current limitation at common
        vf: 5,      	// variation frequency
        mt: 3,          // modulation interval in s
        dr: 1,		// duty ratio
        vv: 0,		// volume variation
        rt: 1,		// rise time in s
        // array parameters are saved per group with index 0 for common
        vol: [],        // volume
        mv: [],         // volume modulation
        da: [],        	// difficulty 1
        mda: [],	// difficulty 1 modulation
        db: [],       	// difficulty 2
        mdb: [],       	// difficulty 2 modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.vol[c] = 0.5;
        p.mv[c] = 0;
        p.da[c] = 0;
        p.mda[c] = 0;
        p.db[c] = 0;
        p.mdb[c] = 0;
    }
}

async function loadEstimProcessor() {
   return audioContext.audioWorklet.addModule("processor3.js");
}

const estimControlCommon = false;
