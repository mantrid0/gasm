'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	pw: 0.8, 	// pulse width in ms
        it: 75,         // inhibition time in ms
        ar: 1,         	// duty ratio
        av: 0,         	// variation
        dv: 0,          // distribution variation
        ds: 1,          // softness distribution variation
        mt: 3,          // modulation interval in s
        // array parameters are saved per group with index 0 for common
        vol: [],        // volume
        rt: [],         // rise time in s
        mv: [],         // volume modulation
        mr: [],         // rise time modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.vol[c] = 0.35;
        p.rt[c] = 1;
        p.mv[c] = 0;
        p.mr[c] = 0;
    }
}

async function loadEstimProcessor() {
   return audioContext.audioWorklet.addModule("processor1.js");
}

const estimControlCommon = true;
