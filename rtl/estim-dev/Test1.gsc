form_start:
    #   formStart();					// starts form mode
    #   @channelConfig = encodeChannelConfig();		// converts current channel configuration to a string
    #   @channelConfigErr = false;			// no error

channelConfig:
    #   estimStop();					// stop audio
    <	<h2>Channel setup</h2>
    <<  Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and @maxEstimChannels. 
    <<  E.g. "1,1,1" for 3 independent channels or "2,1,1," for 4 channels where the first two ones have equal constraints.
    <<  It is recommended that first channels are used for electrodes on more sensitive places. (Probably has no noticeable effect.)
    <<  <input type="text" id="channelConfig" value="@channelConfig">
    <<	Click on 'Done' in order to start audio with a 5s ramp-up time.
    TB  interrupt(); Done
    #   if ( @channelConfigErr ) appendText("Error: " + @channelConfigErr );
    D   

// checks syntax of the channel configuration string and goto previos code block in an error occurred
    #   @channelConfig = document.getElementById("channelConfig").value;
    #   @channelConfigErr = await decodeChannelConfig( @channelConfig );
    #   if ( @channelConfigErr ) @goto("channelConfig");

// starts a new code block (without label) in order to avoid printing the stuff below if an error occurred
:
    #   await estimPlay();				// start audio and initializes it if necessary 
    <   <h2>EStim parameters</h2>
    <<  <h3>User defined parameters</h3>
    <<	These parameters are user dependent and should not be changed by the tease (author).
    // These commands create a slider for Estim settings. Syntax is appendEstimSlider(<description>,<parameter name without group index>);
    // changing the sliders have an immediate effect is audio is playing.
    #	appendEstimSlider( "<b>Pulse width</b> in ms. If you are new in estimming, choose 0.5. If you are numb from all the estimming, 1 is a better choice. Parameter name is 'pw'." , "pw" );
    #	appendEstimSlider( "<b>Inhibition time</b> ms. Test it with difficulty 2 set to 1 for all electrodes and different difficulty 1 settings. Recommended setting is 50. Parameter name is 'it'." , "it" );
    <<  <h3>Main settings</h3>
    <<	These parameters can be (half-)automatically adjusted during teases. This process is called training.
    #	appendEstimSlider("<b>Volume</b> for common electrode ('C') and per group (number). Maximum initial setting should be 0.5 or less. Is is not possible to control these values completely independently. e.g. is is not possible to have current on just one electrode. As more electrodes you have as better they can be controlled. Parameter name is 'vol0' for common, 'vol1' for group 1, etc..", "vol");
    #	appendEstimSlider("<b>Rise time</b> for common electrode ('C') and per group (number). Set variation to a high value to test this setting. That setting can also be controlled manually. If adjusted automatically, initial value should be 1.  Parameter name is 'rt0' for common, 'rt1' for group 1, etc.. ", "rt", 2);
    <<  <h3>Tease settings</h3>
    <<	These parameters are set by tease.
    #	appendEstimSlider( "<b>Duty ratio</b>. Parameter name is 'ar.'" , "ar" );
    #	appendEstimSlider( "<b>Variation</b>. Parameter name is 'av.'" , "av" );
    <<	<h3>Modulation</h3>
    <<	Modulation is mainly intended for training, but also can be used manually.
    #	appendEstimSlider( "<b>Modulation time</b> in s. Parameter name is 'mt'." , "mt" );
    #	appendEstimSlider("<b>Volume modulation</b> for common electrode ('C') and per group (number). Parameter name is 'mv0' for common, 'mv1' for group 1, etc..", "mv");
    #	appendEstimSlider("<b>Rise time modulation</b> for common electrode ('C') and per group (number). Set variation to a high value to test this setting. Parameter name is 'mr0' for common, 'mr1' for group 1, etc..", "mr");
    TB  interrupt(); Back
    D  

    #   formEnd();
    G   form_start

//        dv: 0,          // distribution variation
//        ds: 1,          // softness distribution variation
