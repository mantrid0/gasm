const scriptCode = {
    "code" : [
	"   formStart();					// starts form mode\n   scriptVars.channelConfig = encodeChannelConfig();		// converts current channel configuration to a string\n   scriptVars.channelConfigErr = false;			// no error\n\n",
	"   estimStop();					// stop audio\n   setText('<h2>Channel setup</h2>');\n   appendText('Comma separated number of channels per group. Channels within each group have equal constraints. Total number of channels must be between 2 and '+scriptVars.maxEstimChannels+'.');\n   appendText('E.g. \"1,1,1\" for 3 independend channels or \"2,1,1,\" for 4 channels where the first two ones have equal constraints.');\n   appendText('It is recommended that first channels are used for electrodes on more sensitive places. (Probably has no noticeable effect.)');\n   appendText('<input type=\"text\" id=\"channelConfig\" value=\"'+scriptVars.channelConfig+'\">');\n   appendText('Click on \\'Done\\' in order to start audio with a 5s ramp-up time.');\n   appendTButton('interrupt();;', 'Done');\n   if ( scriptVars.channelConfigErr ) appendText(\"Error: \" + scriptVars.channelConfigErr );\n   { if ( await delay() ) return; }\n",
	"\n// checks syntax of the channel configuration string and goto previos code block in an error occurred\n   scriptVars.channelConfig = document.getElementById(\"channelConfig\").value;\n   scriptVars.channelConfigErr = await decodeChannelConfig( scriptVars.channelConfig );\n   if ( scriptVars.channelConfigErr ) { setGoto(\"channelConfig\"); return; }\n// forces to end code block in order to avoid printing the stuff below if an error occurred\n",
	"	scriptVars.volTraining = 0;\n	scriptVars.d1Training = 0;\n	scriptVars.d2Training = 0;\n\n",
	"   await estimPlay();				// start audio and initializes it if necessary \n   setText('<h2>EStim parameters</h2>');\n   appendText('<h3>User defined parameters</h3>');\n   appendText('These parameters are user dependent and should not be set by the tease (author).');\n	appendEstimSlider( \"<b>Pulse width</b> in ms. If you are new in estimming, choose 0.5. If you are numb from all the estimming, 1 is a better choice. Parameter name is 'pw'.\" , \"pw\" );\n	appendEstimSlider( \"<b>Inhibition time</b> ms. Test it with variation 1 and all rise times 0. Chose the value such that the signal becomes most intense (painful if volume was not lowered). If in doubt, choose 50. Parameter name is 'it'.\" , \"it\" );\n   appendText('<h3>Main settings</h3>');\n   appendText('These parameters can be (half-)automatically adjusted during teases. This process is called training.');\n	appendEstimSlider(\"<b>Volume</b> for common electrode ('C') and per group (number). Maximum initial setting should be 0.5 or less. Is is not possible to control these values completely independently. e.g. is is not possible to have current on just one electrode. As more electrodes you have as better they can be controlled. Parameter name is 'vol0' for common, 'vol1' for group 1, etc..\", \"vol\");\n   appendText('<h3>Tease settings</h3>');\n   appendText('These parameters are set by tease. There are two different difficulty settings which have different effects.');\n	appendEstimSlider( \"<b>Difficulty 1</b>. This setting has a higher impact than the other difficulty setting. Values close to 1 should be used for pain effects. Parameter name is 'da.'\" , \"da\" );\n	appendEstimSlider( \"<b>Difficulty 2</b>. This setting has a smaller impact than the other difficulty setting, but it can be set per channel. Negative values are supported, but they can cause balance issues and therefore should be avoided. Parameter name is 'db0','db1' for grop 1, etc..\" , \"db\" );\n	appendEstimSlider( \"<b>Variation frequency in Hz</b>. Parameter name is 'vf.'\" , \"vf\" );\n   appendText('<h3>Slow volume variation</h3>');\n   appendText('These settings can be used for slow volume variations and should be set by tease.');\n   appendText('Use these settings if you want to produce tease and denial sessions. In all other cases it is o.k. to disable volume variations (by setting duty ratio to 1 and variation to 0) because these effects are typically boring, especially if rise time is slow.');\n	appendEstimSlider( \"<b>Duty ratio</b>. Parameter name is 'dr'.\" , \"dr\");\n	appendEstimSlider( \"<b>Volume variation</b>. Parameter name is 'vv'\" , \"vv\");\n	appendEstimSlider( \"<b>Rise time</b> in s. Parameter name is 'rt'\" , \"rt\");\n   appendText('<h3>Modulation</h3>');\n   appendText('Modulation is mainly intended for training, but also can be used manually.');\n	appendEstimSlider( \"<b>Modulation time</b> in s. Parameter name is 'mt'.\" , \"mt\" );\n	appendEstimSlider( \"<b>Volume modulation</b> for common electrode ('C') and per group (number). Parameter name is 'mv0' for common, 'mv1' for group 1, etc..\" , \"mv\" );\n	appendEstimSlider( \"<b>Difficulty 1 modulation</b>. Parameter name is 'mda'.\" , \"mda\");\n	appendEstimSlider( \"<b>Difficulty 2 modulation</b> for common electrode ('C') and per group (number).  Parameter name is 'mdb0' for common, 'mdb1' for group1, etc..\" , \"mdb\" );\n   appendText('Volume training: '+scriptVars.volTraining+' % per minute<br>Difficulty 1 training: '+scriptVars.d1Training+' percent points per minute<br>Difficulty 2 training: '+scriptVars.d2Training+' percent points per minute');\n	changeEstimParam( \"vol\" , scriptVars.volTraining/(60*100) );\n	changeEstimParam( \"da\" , scriptVars.d1Training/(60*100) );\n	changeEstimParam( \"db\" , scriptVars.d2Training/(60*100) );\n   appendTButton('interrupt();;', 'Back');\n   appendTButton(\"setGoto('stoptraining')\", 'Stop Training');\n   appendTButton(\"setGoto('voltrain')\", 'Increase volume training by 5% per minute');\n   appendTButton(\"setGoto('d1train')\", 'Increase difficulty 1 training by 5 percent points per minute');\n   appendTButton(\"setGoto('d2train')\", 'Increase difficulty 2 training by 10 percent points per minute');\n   { if ( await delay() ) return; }\n",
	"   formEnd();\n   { setGoto('form_start'); return; }\n\n",
	"	scriptVars.volTraining += 5;\n   { setGoto('form2'); return; }\n",
	"	scriptVars.d1Training += 5;\n   { setGoto('form2'); return; }\n",
	"	scriptVars.d2Training += 10;\n   { setGoto('form2'); return; }\n",
    ],
    "label" : [
	"form_start",
	"channelconfig",
	null,
	"stoptraining",
	"form2",
	null,
	"voltrain",
	"d1train",
	"d2train",
    ],
    "sourceFN" : [
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
	"Test2.gsc",
    ],
    "sourceLN" : [
	1,
	6,
	16,
	25,
	30,
	66,
	70,
	73,
	76,
    ]
};

const imageList = [
    { "dir" : ".", "files" : [ ] },
    { "dir" : "old", "files" : [ ] },
];

const videoList = [
    { "dir" : ".", "files" : [ ] },
    { "dir" : "old", "files" : [ ] },
];

const audioList = [
    { "dir" : ".", "files" : [ ] },
    { "dir" : "old", "files" : [ ] },
];
let baseDirectory=".";

