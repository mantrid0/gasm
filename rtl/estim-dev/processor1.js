let instanceCount = 0;	
const maxChannels = 5;		// maximum number of output channels. Currently audio API only defines up to 5 full frequency channels.
const pulsePeriod = 2.5;	// pulse period in pulse width, >2
const muteRamp = 1/5;		// reciprocal ramp-up time after mute in seconds

let debugCnt = 0;
const pulseBuffer = [];		// current pulse, index 0 is only used during calculation
let pulseSample = 0;            // sample count within current pulse period
let samplesPerPulse = -1; 	// updated if pulse is calculated, i.e. if pulseSample > samplesPerPulse*pulsePeriod
let muteT = 0.0;		// last time muted
const cop = [];			// channel order for polarity calculation

// waveform variables
let timeA = 0.0;		// amplitude waveform time
let timeM = 2.0;		// modulation waveform time
let novar = true;		// no variation
let tp1	= 0;			
let tp2 = 0;
let tp3 = -1;
const rtbuf = []; 
const mvbuf = [];


// waveform patterns
const pat3 = [
	0,1,  2,3,
	0,2,  1,3,
	1,2,  0,3
    ];

const pat5 = [
	0,3,  1,4,  2,5,
	0,4,  2,3,  1,5,
	0,1,  2,4,  3,5,
	0,2,  1,3,  4,5,
	1,2,  3,4,  0,5,
	0,3,  2,4,  1,5,
	0,2,  1,4,  3,5,
	0,1,  3,4,  2,5,
	1,4,  2,3,  0,5,
	0,4,  1,3,  2,5,
	0,3,  1,2,  4,5,
	0,2,  3,4,  1,5,
	1,3,  2,4,  0,5,
	0,1,  2,3,  4,5,
	0,4,  1,2,  3,5
    ];

let patCnt = 0;

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

class EstimProcessor extends AudioWorkletProcessor {
    instanceCount = 0;

    constructor ( options ) { 
        super( options );
        instanceCount++;
        this.instanceCount = instanceCount;
        console.log("Started Estim worker #" + this.instanceCount);
    
        pulseSample = 0;
        samplesPerPulse = -1;       
        muteT = currentTime;
	timeM = 2.0;
	tp3 = -1;
        for ( let c=0; c<maxChannels+2; c++ ) 
    	    cop[c]=c;
    } 
    
    static get parameterDescriptors() {
        let params = [
            {   // true number of channels
                name: "channels",
                defaultValue: 2,
                minValue: 2,
                maxValue: 5,
                automationRate: "k-rate",
            }, {  // mute if > 0.5
                name: "mute",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // pulse width in ms
                name: "pw",  
                defaultValue: 1,
                minValue: 1000/sampleRate,
                maxValue: 2,
                automationRate: "k-rate",
            }, {  // inhibition time in ms
                name: "it",  
                defaultValue: 100,
                minValue: 25,
                maxValue: 250,
                automationRate: "k-rate",
            }, {  // duty ratio
                name: "ar",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // variation
                name: "av",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // distribution variation
                name: "dv",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // softness distribution variation
                name: "ds",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // modulation time in seconds
                name: "mt",  
                defaultValue: 3,
                minValue: 1,
                maxValue: 10,
                automationRate: "k-rate",
            } ];
        // per channel parameters for common (index 0) and up to maxChannels channels
        for ( let c=0; c<=maxChannels; c++ ) {
    	    // volume
            params[params.length] = {
                name: "vol"+c,
//                defaultValue: c<1 ? 0.5 : 1,
                defaultValue: 0.5,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // rise time in s
            params[params.length] = {
                name: "rt"+c,
                defaultValue: 1,
                minValue: 0,
                maxValue: 4,
                automationRate: "k-rate",
            };
    	    // volume modulation
            params[params.length] = {
                name: "mv"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // rise time modulation
            params[params.length] = {
                name: "mr"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
        }
        return params;
    }

    process(inputs, outputs, parameters) {
        if ( instanceCount != this.instanceCount ) {
            console.log("Terminating Estim worker #" + this.instanceCount);
            return false;
         }

        const buf = outputs[0];
        const channels = Math.min(maxChannels, Math.min(buf.length, Math.round(parameters["channels"][0])) );	// calculated channels
        const samples = buf[0].length;
    	        
        // muted ?
        if ( parameters["mute"][0]>0.5 ) {
    	    muteT = currentTime;
    	    pulseSample = 0;
    	    samplesPerPulse = -1;       
    	    muteT = currentTime;
            for ( let c = 0; c<buf.length; c++ ) { 	
        	const b = buf[c];
    		for ( let i = 0; i < samples; i++) 
            	    b[i] = 0;
            }
            return true;
        }

	// read parameters
	const pw = Math.max(1000/sampleRate, parameters["pw"][0])*1e-3;	// pulse width in seconds
	const ti = Math.max(25, parameters["it"][0])*1e-3;		// inhibition time in seconds
	const av = Math.min(1,parameters["av"][0]);
	const ar = Math.min(1,parameters["ar"][0]);
	const tm = Math.max(1, parameters["mt"][0]);			// modulation time
	const id = pulsePeriod*pw/ti;					// inhibition step per pulse period
        const vol = [];		// volume
        for ( let c=0; c<=channels; c++ )
    	    vol[c] = Math.max(0, Math.min(2,parameters["vol"+c][0]));
        
        // Only reduce high and low time if parameters changed. Increasing may cause unwanted effects.
        tp2 = Math.min(tp2, tp1 + ( (av<1e-9) ? 10 : Math.max((pulsePeriod*2)*pw, Math.min(10, (tp1+ti)*(1/av-1))) ) );
        tp3 = Math.min(tp3, tp2+ti + ( (ar<1e-9) ? 20 : Math.min(20, (tp2+ti)*(1/ar-1)) ) );
                
        for ( let i = 0; i < samples; i++) {
        
            if ( pulseSample >= samplesPerPulse*pulsePeriod ) {
    		// const debug = (debugCnt==0) || (debugCnt==15) || (debugCnt==30);
    		const debug = false;
                const ba = [];

        	// modulation waveform
    		timeM += pulseSample/(sampleRate*tm);
    		if ( timeM >= 1 ) {
    		    timeM = 0;
    		    // update at start of new sequence in order to avoid unwanted effects if parameter changes
    		    for ( let c=0; c<=channels; c++ )  
    	    		mvbuf[c] = Math.max(0, Math.min(2,parameters["mv" +c][0]));
    		}
		
		// amplitude waveform
    		timeA += pulseSample/sampleRate;
    		if ( timeA > tp3 ) {
    		    timeA = 0;
    		    novar = (av<0.005) && (ar>0.995);
    		    if ( novar ) tp3 = 0.5
    		    else {
    			tp1 = 0;
    			for ( let c=0; c<=channels; c++ ) {
    			    const r = Math.max(0,parameters["rt" +c][0]) * (1-timeM*Math.max(0,Math.min(1,parameters["mr" +c][0])));
    			    rtbuf[c*2  ] = r;
    			    rtbuf[c*2+1] = 1/Math.max(1e-12,r);
    			    tp1 = Math.max(tp1,r);
    			    ba[c] = -1;
    			}
    			tp2 = tp1 + ( (av<1e-9) ? 10 : Math.max((pulsePeriod*2)*pw, Math.min(10, (tp1+ti)*(1/av-1))) );
    			tp3 = tp2+ti + ( (ar<1e-9) ? 20 : Math.min(20, (tp2+ti)*(1/ar-1)) );
			// new random channel order
			ba[channels+1] = -1;
    			for ( let c=0; c<channels+2; c++ ) {
    			    let c1 = randomInt(channels+2);
    			    while ( ba[c1]>0 )
    				c1 = (c1+1) % (channels+2);
    			    cop[c1] = c;
    			    ba[c1] = 1;
    			}
    		    }
    		}

		// mute ramp
		const ma = Math.min(1, (currentTime-muteT)*muteRamp);

		// width of next pulse		
                samplesPerPulse = Math.round(sampleRate*pw);
                pulseSample = 0;

                // calculate new pulse
		const maxVol = [];		// volume parameter, updated per pulse period
    		if ( debug && (!novar) ) console.log(timeA+":  "+tp1+"   "+tp2+"   "+tp3);
    		for ( let c=0; c<=channels; c++ ) {
    		    let a = vol[c]*(1+timeM*mvbuf[c]);
    		    
    		    if ( !novar ) {
    			const tr = rtbuf[c*2];
    			let t = 0.5*(tp1-tr);
    			if ( (timeA<=t) || (timeA>=tp2) ) a=0
    			else {
    			    t = timeA - t + pw;
    			    if (t<tr) a*=t*rtbuf[c*2+1];
    			}
    		    }
    		    maxVol[c] = Math.sqrt(a*ma);
    		    ba[c] = -1;
    		    pulseBuffer[c] = 0;
    		}

                // calculate pulse
                // 1: find pairs
                if ( (channels<2) || (channels>5) ) {	// random fallback
            	    for ( let c=1; c<=channels; c+=2 ) {
    			let c1 = randomInt(channels+1);
    			while ( ba[c1]>0 )
    			    c1 = (c1+1) % (channels+1);
    			ba[c1] = 1;
    			let c2 = randomInt(channels+1);
    			while ( ba[c2]>0 )
    			    c2 = (c2+1) % (channels+1);
    			ba[c2] = 1;
    			const v = Math.min(maxVol[c1], maxVol[c2]);
    			pulseBuffer[c1] =  v;
    			pulseBuffer[c2] = -v;
    		    }
    		} 
    		else {	// waveform
            	    patCnt = (patCnt + 1 ) % 60;	// period is 6=3*2 for pat3 and 60=15*4 for pat5
            	    let pat = [], pol = [], o = 0;
    		    if ( channels < 4 ) {
    			pat = pat3;
    			o = (patCnt % 3)*4;
			pol = [0, patCnt & 1];
		    }
    		    else {
    			pat = pat5;
    			o = (patCnt % 15)*6;
			pol = [0, patCnt & 1, (patCnt>>1) & 1];
		    }
		    for ( let i=0; i<pol.length; i++ ) {
            		const c1 = pat[o+i*2];
            		const c2 = pat[o+i*2+1];
            		let v = ( (c1<=channels) && (c2<=channels) ) ? Math.min(maxVol[c1], maxVol[c2]) : 1e-5;
            		const d = cop[c1]-cop[c2];
            		if ( novar || (Math.abs(d)<Math.max(0.1,timeA-2)) ) v*=1-2*pol[i];
            		else if ( d < 0 ) v=-v;
    			pulseBuffer[c1] =  v;
    			pulseBuffer[c2] = -v;
    		    }
            	}
                // find channels which can be optimized, save them in random order
                const oi = [];	// index
                const od = [];	// difference
    		for ( let c=0; c<=channels; c++ ) {
    		    let c1 = randomInt(channels+1);
    		    while ( ba[c1]>2 )
    			c1 = (c1+1) % (channels+1);
    		    ba[c1] = 3;
    		    let r = pulseBuffer[c1];
    		    let s = Math.sign(r);
    		    if ( s == 0 ) s = 2*randomInt(2)-1;
    		    r = maxVol[c1]*s - r;
    		    if ( r*s > 1e-3 ) {
    			const j = oi.length;
    			oi[j] = c1;
    			od[j] = r;
    		    }
    		}
    		if ( debug ) console.log("pulseBuffer=[" + pulseBuffer + "]  oi=[" + oi + "]  od=[" + od +"]  debugCnt="+debugCnt);
                // maximize volume
    		for ( let i=0; i<oi.length-1; i++) {
    		    const c1 = oi[i];
    		    let r = od[i];
    		    const s = Math.sign(r);
    		    let j=i+1
    		    while ( (j<oi.length) && (r*s>1e-3) ) {
    			let d = Math.min(-od[j]*s, r*s);
    			if ( d > 0 ) {
    			    const c2 = oi[j];
    			    d*=s;
    			    pulseBuffer[c1] += d;
    			    pulseBuffer[c2] -= d;
    			    r -= d;
    			    od[j] += d;
    			}
    			j++;
    		    }
    		}    
//    		if ( debug ) console.log("pulseBuffer=[" + pulseBuffer +"]  t=" + (currentTime-muteT));
    		debugCnt=(debugCnt+1)%999;
            }
            
            const s = (pulseSample>=2*samplesPerPulse) ? 0 : (pulseSample>=samplesPerPulse) ? -1 : 1;
            pulseSample++;
            
            for ( let c = 0; c<buf.length; c++ )
                buf[c][i] = (c<channels) ? s * pulseBuffer[c+1] : 0;
        }
        return true;
    }
}

registerProcessor("estim-processor", EstimProcessor);
