let instanceCount = 0;	
const maxChannels = 8;		// maximum number of output channels. Currently audio API only defines up to 5 full frequency channels.
const muteRamp = 1.0/5;		// reciprocal ramp-up time after mute in seconds

let muteT = 0.0;		// last time muted
let dt2 = 0;

// carrier waveform
let phase = 0;
const phaseShift = [];
const fPhaseShift = [];
const difA = [];
const difB = [];

// high frequency modulation
let timeH = 2.0;		// in periods
let dutyRatioH = [];

// modulation variables
let timeM = 2.0;		// modulation waveform time
const mvBuf = [];
const mdaBuf = [];
const mdbBuf = [];

// slow amplitude variation
let timeA = 2.0;		// amplitude waveform time
let noVar = true;		// no variation
let tph = 0.5;			
let rtr	= 2;			

class EstimProcessor extends AudioWorkletProcessor {
    instanceCount = 0;

    constructor ( options ) { 
        super( options );
        instanceCount++;
        this.instanceCount = instanceCount;
        console.log("Started estim worker #" + this.instanceCount +". Sample rate: "+sampleRate+" Hz");
    
        muteT = currentTime;
	timeH = 2.0;
	timeM = 2.0;
	timeA = 2.0;
	for ( let c=0; c<maxChannels; c++ ) 
	    phaseShift[c] = Math.random();
    } 
    
    static get parameterDescriptors() {
        let params = [
            {   // true number of channels
                name: "channels",
                defaultValue: 2,
                minValue: 2,
                maxValue: 5,
                automationRate: "k-rate",
            }, {  // mute if > 0.5
                name: "mute",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // pulse width in ms
                name: "pw",  
                defaultValue: 0.8,
                minValue: 1000/sampleRate,
                maxValue: 2,
                automationRate: "k-rate",
            }, {  // inhibition time in ms
                name: "it",  
                defaultValue: 50,
                minValue: 25,
                maxValue: 100,
                automationRate: "k-rate",
            }, {  // current limitation at common
                name: "ccl",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 2,
                automationRate: "k-rate",
            }, {  // variation frequency
                name: "vf",  
                defaultValue: 5,
                minValue: 0,
                maxValue: 25,
                automationRate: "k-rate",
            }, {  // modulation time in seconds
                name: "mt",  
                defaultValue: 3,
                minValue: 1,
                maxValue: 10,
                automationRate: "k-rate",
            }, {  // duty ratio
                name: "dr",  
                defaultValue: 1,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // volume variation
                name: "vv",  
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            }, {  // rise time
                name: "rt",
                defaultValue: 1,
                minValue: 0,
                maxValue: 2,
                automationRate: "k-rate",
            } ];
        // per channel parameters for common (index 0) and up to maxChannels channels
        for ( let c=0; c<=maxChannels; c++ ) {
    	    // volume
            params[params.length] = {
                name: "vol"+c,
//                defaultValue: c<1 ? 0.5 : 1,
                defaultValue: 0.35,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // volume modulation
            params[params.length] = {
                name: "mv"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 1
            params[params.length] = {
                name: "da"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 1 modulation
            params[params.length] = {
                name: "mda"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 2
            params[params.length] = {
                name: "db"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
    	    // difficulty 2 modulation
            params[params.length] = {
                name: "mdb"+c,
                defaultValue: 0,
                minValue: 0,
                maxValue: 1,
                automationRate: "k-rate",
            };
        }
        return params;
    }

    process(inputs, outputs, parameters) {
        if ( instanceCount != this.instanceCount ) {
            console.log("Terminating Estim worker #" + this.instanceCount);
            return false;
         }

        const buf = outputs[0];
        const channels = Math.min(maxChannels, Math.min(buf.length, Math.round(parameters.channels[0])) );	// calculated channels
        const samples = buf[0].length;
//        console.log("samples: "+samples);
    	        
        // muted ?
        if ( parameters.mute[0]>0.5 ) {
    	    muteT = currentTime;
            for ( let c = 0; c<buf.length; c++ ) { 	
        	const b = buf[c];
    		for ( let i = 0; i < samples; i++) 
            	    b[i] = 0;
            }
            return true;
        }

	// read parameters
        const fc = Math.min(0.125*sampleRate, 500.0/Math.max(0.1,parameters.pw[0]));	// pulse width --> carrier frequency
	const ti = Math.max(25, parameters.it[0])*1e-3;				// inhibition time in seconds
	const fh = 1 / (ti*2);							// HF modulation frequency
	const fm = 1.0 / Math.max(1, parameters.mt[0]);				// parameter modulation frequency
	const fps = parameters.vf[0];						// max. phase variation frequency
    	// slow amplitude variation
    	const vv = Math.max(1e-9, Math.min(1, parameters.vv[0]));		// slow volume variation
    	const dr = Math.max(1e-9, Math.min(1, parameters.dr[0]));		// duty ratio
    	const atr = Math.max(1e-9, parameters.rt[0]);				// rise time in s
    	const ath = Math.min(10, 2*ti+atr*(1/vv-1));				// high time
    	const atl = Math.max(2*ti-atr, Math.min(20, (atr+ath)*(1/dr-1)));	// low time
    	const fa = 1 / (atr+ath+atl);						// frequency
    	// reduce modulation settings immediately
        for ( let c=0; c<channels; c++ ) {
    	    mvBuf[c]  = Math.min( mvBuf[c] , Math.max(0, Math.min(1,parameters["mv"  + (c+1)][0])));
    	    mdaBuf[c] = Math.min( mdaBuf[c], Math.max(0, Math.min(1,parameters["mda" + (c+1)][0])));
    	    mdbBuf[c] = Math.min( mdbBuf[c], Math.max(0, Math.min(1,parameters["mdb" + (c+1)][0])));
    	}

	// samples        
        const dt = 1/sampleRate;
        const maxVol = [];
        let maxVolC = 0;
        for ( let i = 0; i < samples; i++) {
	    // calculated at least every every 256 samples
    	    if ( (i & 255) == 0 ) {
		// mute ramp
    		const ma = Math.min(1, (currentTime-muteT+dt*64)*muteRamp);    

        	// modulation waveform
    		timeM += dt2*fm;
    		if ( timeM >= 1 ) {
    		    timeM = 0;
    		    // re-randomize phase shift frequencies
    		    for ( let c=0; c<channels; c++ ) 
    			fPhaseShift[c] = -3;
    		    for ( let c=0; c<channels; c++ ) {
    			let c1 = Math.floor(Math.random() * channels);
    			while ( fPhaseShift[c1]>-2 ) 
    			    c1 = (c1+1) % channels;
    			fPhaseShift[c1] = 2*c/(channels-1) - 1;
    		    }
		    // buffer modulation settings in order to avoid artifacts if parameter changes
    		    for ( let c=0; c<channels; c++ ) {
    	    		mvBuf[c] = Math.max(0, Math.min(1,parameters["mv" + (c+1)][0]));
    	    		mdaBuf[c] = Math.max(0, Math.min(1,parameters["mda" + (c+1)][0]));
    	    		mdbBuf[c] = Math.max(0, Math.min(1,parameters["mdb" + (c+1)][0]));
    	    	    }
    		}

		// slow volume modulation
		timeA += dt2*fa;
		if ( timeA > 1 ) {
		    timeA = 0;
		    const T = atr+ath+atl;
		    tph = (atr+ath)/T;
		    rtr = T / atr;
		}
		const sa = noVar ? 1 : (timeA>tph) ? 0 : Math.min(1,(timeA+0.5*dt2)*rtr);
		if ( sa>0.99 ) noVar = (vv<0.005) && (dr>0.995);	// avoid jumps

		// volume, difficulties and duty ration of high frequency amplitude modulation
		let vm = 0;
		maxVolC = 0;
    	        for ( let c=0; c<channels; c++ ) {
    	    	    let r = Math.max(0, Math.min(1, parameters["da"+(c+1)][0]));
    	    	    difA[c] = r + timeM*(1-r)*mdaBuf[c];
    	    	    r = Math.max(0, Math.min(1, parameters["db"+(c+1)][0]));
    	    	    difB[c] = r + timeM*(1-r)*mdbBuf[c];
    	    	    r = Math.max(0, Math.min(1, parameters["vol"+(c+1)][0])) * (1+timeM*mvBuf[c]);
    	    	    r = Math.sqrt(r*sa*ma);
    	    	    vm = Math.max(vm,r);
    	    	    maxVolC += r;
		    maxVol[c] = r;
		}
		const r = Math.max(0, Math.min(2, parameters.ccl[0]));
		maxVolC = (r>1) ? ((2-r)*vm) : ( (1-r)*maxVolC+r*vm);
//    		console.log("fc="+fc+"  fh="+fh+"  fm="+fm+"  maxVol=["+maxVol+"]");
    		dt2 = 0;
	    }
    	    dt2+=dt;
        
    	    // high frequency modulation
    	    timeH += dt*fh;
    	    if ( timeH > 1 ) {
		timeH -= Math.floor(timeH);
        	for ( let c = 0; c<channels; c++ )
		    dutyRatioH[c] = 0.25 + 0.75*(1-difA[c]);
	    }
    	    // carrier waveform
            phase += dt*fc;
    	    phase -= Math.floor(phase);
            let cc = 0;	// current at common
            for ( let c = 0; c<channels; c++ ) {
        	if ( timeH > dutyRatioH[c] ) {
        	    buf[c][i] = 0;
        	    continue;
        	} 
        	// phase shift
        	let p = phaseShift[c] + fPhaseShift[c]*fps*dt;
        	p -= Math.floor(p);
        	phaseShift[c] = p;
        	let d = difB[c];
        	// phase
        	p = phase + (1-d)*p + d*(0.25+0.5*Math.round(p));
        	p -= Math.floor(p);
        	p = (p<0.5) ? (1-4*p) : (4*p-3);
        	d = difA[c];
        	// output
        	p = maxVol[c] * ((1-d)*p + d*Math.sign(p));
        	buf[c][i] = p;
        	cc += p;
    	    }
    	    if ( Math.abs(cc)> maxVolC ) {
    		cc = (cc-maxVolC*Math.sign(cc))/channels;
        	for ( let c = 0; c<channels; c++ )
        	    buf[c][i]-=cc;
    	    }
            for ( let c = channels; c<buf.length; c++ ) 
                buf[c][i] =  0;
        }
        return true;
    }
}

registerProcessor("estim-processor", EstimProcessor);
