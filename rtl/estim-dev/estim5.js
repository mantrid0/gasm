'use strict';

function initScriptVarsEstimProcessor () {
    scriptVars.estimParams = {
	// user settings
        fc: 700,	// carrier frequency
        cb: [],		// channel balance
        ccl: 0.5,	// current limitation at common
	sens: [],	// sensitivity
	// tease settings which should only be changed for special effects
        tma: 2,		// minimum parameter modulation time
        tmb: 5,		// maximum parameter modulation time
        fps: 0,		// log10 of phase shift frequency multiplier
	// tease settings controlled by high level API
        vol: [],        // volume
        drl: 1,		// low frequency duty ratio (rise interval counts as 0.5)
        tl: 0.6,        // total limit
        da: [],        	// difficulty 1 (controls high frequency amplitude modulation frequency )
        db: 0,       	// difficulty 2 (controls frequency of +/- 180° phase swaps)
        mvol: [],	// modulation of volume (absolute)
        mtl: 0,		// difficulty 1 modulation
        mda: [],	// difficulty 1 modulation
        mdb: 0,       	// difficulty 2 modulation
    }
    const p = scriptVars.estimParams;
    for ( let c=0; c<=estimMaxChannels; c++ ) {
        p.cb[c] = 0.75;
        p.sens[c] = 0.5;
        p.vol[c] = 0.75;
        p.da[c] = 0;
        p.mvol[c] = 0;
        p.mda[c] = 0;
    }
}

/*function updateEstimDefaults ( n ) {
    const p = scriptVars.estimParams;
    for ( let c=1; c<=n; c++ ) 
	    p.sens[c] = (n-c+0.5)/n;
    syncEstimParams();
}*/

async function loadEstimProcessor() {
    return audioContext.audioWorklet.addModule("processor5.js");
}

const estimControlCommon = false;


// *****************************************************************************
// ******** processor depended parts of high level API 1 ***********************
// *****************************************************************************
const estimHL1_volMin = 0.7;

// generate parameter+weight array for intensity
// params: optional, default: scriptVars.estimParams;
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_readIPWA(params) {
    const gn = scriptVars.estimGroups.length;
    const weight_vol = 1.0/gn;
    const weight_tl = 1;
    const rv = 1 / (1-estimHL1_volMin);
    if ( (typeof(params) != "object") )
	params = scriptVars.estimParams;
    const pwa = [];
    pwa.push( (params.tl-0.6)*2.5, weight_tl, (params.tl+params.mtl-0.6)*2.5, weight_tl);
    for ( let g=1; g<=gn; g++ ) {
	const v = params.vol[g];
	const v0 = Math.max(0, v-estimHL1_volMin)*rv;
	const v1 = Math.max(0, v+params.mvol[g]-estimHL1_volMin)*rv;
	pwa.push( v0*v0, weight_vol, v1*v1, weight_vol);
    }
    estimHL1_normalizePWA(pwa);
    return pwa;
}

// priorize a single channel
// ch channel group (optional). First one: 1
// p  priorization. Default: 5, Should be >1
function estimHL1_getIPrio(ch, p) {
    if ( typeof(p) != "number" ) p=5;
    if ( typeof(ch) != "number" ) ch=-1;

    const prio = [];
    pwa.push( 1.0, 1.0);
    for ( let g=1; g<=gn; g++ ) {
	if ( g == ch ) pwa.push( p, p);
	else pwa.push( 1.0, 1.0);
    }
    prio;
}

// write parameter+weight array pwa for intensity
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_writeIPWA( pwa ) {
    const tl0 = pwa[0]*0.4 + 0.6;
    const tl1 = pwa[2]*0.4 + 0.6;
    setEstimParam( "tl", tl0 );
    setEstimParam( "mtl", tl1 - tl0 );
    for ( let g=1; g<=scriptVars.estimGroups.length; g++ ) {
	const v0 = Math.sqrt(pwa[g*4 + 0])*(1-estimHL1_volMin) + estimHL1_volMin;
	const v1 = Math.sqrt(pwa[g*4 + 2])*(1-estimHL1_volMin) + estimHL1_volMin;
	setEstimParam("vol"+g, v0);
	setEstimParam("mvol"+g, v1-v0);
    }
}

// generate parameter+weight array for pain
// params: optional, default: scriptVars.estimParams;
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_readPPWA(params) {
    const gn = scriptVars.estimGroups.length;
    const weight_da = 2.0/gn;
    const weight_db = 1;
    if ( (typeof(params) != "object") )
	params = scriptVars.estimParams;
    const pwa = [];
    pwa.push( params.db, weight_db, params.db+params.mdb, weight_db);
    for ( let g=1; g<=gn; g++ ) 
	pwa.push( params.da[g], weight_da, params.da[g]+params.mda[g], weight_da);
    estimHL1_normalizePWA(pwa);
    return pwa;
}

// priorize a single channel
// ch channel group (optional). First one: 1
// p  priorization. Default: 5, Should be >1
function estimHL1_getPPrio(ch, p) {
    if ( typeof(p) != "number" ) p=5;
    if ( typeof(ch) != "number" ) ch=-1;

    const prio = [];
    pwa.push( 1.0, 1.0);
    for ( let g=1; g<=gn; g++ ) {
	if ( g == ch ) pwa.push( p, p);
	else pwa.push( 1.0, 1.0);
    }
    prio;
}


// write parameter+weight array pwa for pain
// format: tl0, tl1, gn*(vol0, vol1)
function estimHL1_writePPWA( pwa ) {
    setEstimParam( "db", pwa[0] );
    setEstimParam( "mdb", pwa[2] - pwa[0] );
    for ( let g=1; g<=scriptVars.estimGroups.length; g++ ) {
	setEstimParam("da"+g, pwa[g*4]);
	setEstimParam("mda"+g, pwa[g*4+2] - pwa[g*4] );
    }
}

/**
  * Get duty ratio. 
  * 0 mean silence. 1 means EStim without breaks. Small values can be used for denial games.
  * @return Current duty ratio.
  */
function estimHL1_getDR() {
    return scriptVars.estimParams.drl;
}

/**
  * Set duty ratio.
  * 0 mean silence. 1 means EStim without breaks. Small values can be used for denial games.
  * @return Current duty ratio.
  */
function estimHL1_setDR( val ) {
    if ( typeof(val) != "number" ) scriptError( "estimHL1_setDR: Argument must be a number");
    setEstimParam( "drl", Math.max(0, Math.min(1, val)) );
}
