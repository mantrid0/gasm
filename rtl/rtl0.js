/** @file  rtl0.js
  * This is the low level part of the gasm run time library.
  */

/* todo:
   - line numbers within code blocks 
 */
 
'use strict';

// Compatibility tests.
try {
    let AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;
    if ( (typeof(Promise)!="function" ) || (typeof(AsyncFunction)!="function") ) throw 0;
} 
catch ( e ) {
    alert("Your browser seems to be incompatible: No support for 'Promise' and 'AsyncFunction'.");
}
const AsyncFunction = Object.getPrototypeOf(async function(){}).constructor;

// whether to enable debug
if ( typeof(enableDebug) === 'undefined' ) window.enableDebug = false;

// name used for the state file
/*if ( typeof(stateName) === 'undefined' ) {
    const s = scriptCode.sourceLN[0];
    const i = s.lastIndexOf('.');
    window.stateName = (i>0) ? s.substring(0,i) : 'state';
} */

/** @namespace
  * All script variables should be stored in this object. Restoring this object should
  * allow re-entry of the script at a saved state.
  */
var scriptVars = {};

/**
  * Contains the index of the currently running code block
  */
scriptVars.currentBlock = 0;

/** 
  * This array forms the execution stack. The last entry contains the index 
  * of the next code block. If this is negative or not a number <code>{@link scriptVars.currentBlock}+1</code> will
  * be used instead. 
  */
scriptVars.nextBlock = [];

/** 
  * If a code block has been visited it is marked with true.
  * That flag can be toggled by user 
  */
scriptVars.blockVisited = [];

/** 
  * If a script state has been restored automatically this contains the {@link scriptVars} object of the previous session.
  * It can be used to detect whether the script starts with restored state in order to allow re-entry ({@link restoreScript(scriptVarsPrev)}) or reset ({@link resetScript()}).
  * Currently that function is not used.
  */
var scriptVarsPrev = null;

// Index of start block
var startIdx = -1;

// Init low level RTL script variables. Called by initScriptVars0.
function initScriptVars0 () {
    // global variables
    // whether to enable debug
    if ( typeof(enableDebug) === 'undefined' ) window.enableDebug = false;

    // name used for the state file
    if ( typeof(stateName) === 'undefined' ) {
	const s = scriptCode.sourceFN[0];
	const i = (typeof(s)==='string') ? s.lastIndexOf('.') : -1;
	window.stateName = (i>0) ? s.substring(0,i) : 'state';
    }
    // scriptVars object
    scriptVars = {};
    scriptVars.currentBlock = startIdx;
    scriptVars.nextBlock = [];
    scriptVars.nextBlock[0] = startIdx;
    scriptVars.blockVisited = [];
    // high level RTL script variables
    initScriptVars1();
}


/**
  * Return a script variable given by name. Nested variables are supported, i.e. <code>getScriptVar("abc.def")</code> returns <code>scriptVars.abc.def</code>.
  * @param name {String} Variable name
  * @return The script variable or nothing (undefined) if the variable does not exist
  */
function getScriptVar(name) {
    if ( typeof(name) != 'string') return;
    const ns = name.split('.');
    var o = scriptVars;
    for (let i=0; i<ns.length; i++ ) {
	if (typeof(o) != 'object') return;
	o = o[ns[i]];
    }
    return o;
}

/**
  * Set a script variable given by name. Nested variables are supported, i.e. <code>setScriptVar("abc.def",123)</code> is equal to <code>scriptVars.abc.def=123</code>.
  * Variables are created if necessary. <b>Note that nested objects may overwrite existing variables.</b>
  * @param name {String} Variable name
  * @param val Variable name
  */
function setScriptVar(name, val) {
    if ( typeof(name) != 'string') throw "First argument of 'setScriptName()' must be a string";
    const ns = name.split('.');
    var o = scriptVars;
    for (let i=0; i<ns.length-1; i++ ) {
	if ( typeof(o[ns[i]]) != 'object') o[ns[i]] = {};
	o = o[ns[i]];
    }
    o[ns[ns.length-1]] = val;
}

/**
  * Return the index of the current block.
  * This also works if {@link scriptVars.currentBlock} is invalid. In that case 0 is returned, which points to the entry code block.
  */
function currentBlock() {
    if ( (typeof(scriptVars.currentBlock) == 'number') && (scriptVars.currentBlock>=0) ) return scriptVars.currentBlock;
    return 0;
}

    
/**
  * Searches for a code block with a given label and return its index. If no code block is found, -1 is returned.
  * @param label The label of the code block. It is automatically converted to lower case because all labels are lower case.
  */
function findLabel(label) {
    if ( (typeof(label) != 'string') ) return -1;
    if ( label.length < 1 ) return -1;
    var llabel = label.toLowerCase();
    for (var i=0; i<scriptCode.label.length; i++ ) 
	if ( scriptCode.label[i] == llabel ) return i;
    return -1;
}

// True if we are in script
var runScriptActive = false;

/**
  * Generates a error alert which includes information about the code block where it occurred.
  * @param err The error as Error object or as string containing the message
  */
function scriptError(err) {
    var msg;
    if ( runScriptActive ) {
	const idx = currentBlock();
	msg = "Error in code block at " + scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+"): ";
    } else {
	msg = "Error: "
    }
    if ( err.message ) {
	msg+=err.message;
	if ( err.stack ) msg+= "\nStack trace:\n" + err.stack;
    }
    else {
	msg+=err;
    }
    console.log(msg);
    alert(msg);
}

/**
  * Sets the target for the next page. In particular, this stets the last element in
  * the execution stack {@link scriptVars.nextBlock} to the index of the code block
  * with the given label.
  * In order to implement GOTO functionality, the code block must be exited using the <code>return;</code> statement.
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  * @param label Label of the code block which is executed as next.
  */
function setGoto(label) {
    var idx = findLabel(label);
    if ( idx < 0 ) scriptError("label `" + label + "' does not exist");
    else scriptVars.nextBlock[scriptVars.nextBlock.length-1] = idx;
}


/**
  * Repeat the current code block.
  * In particular, this stets the last element in the execution stack {@link scriptVars.nextBlock} to {@link scriptVars.currentBlock}.
  * In order to implement that functionality, the code block must be exited using the <code>return;</code> statement.
  * If that function is called during {@link delay} (usually by pressing a button), {@link d
This method ends the form mode, i.e. clears and swaps text and media area and restores global buttons. elay} will exit with return value <code>true</code>.
  */
function setGotoRepeat() {
    scriptVars.nextBlock[scriptVars.nextBlock.length-1] = scriptVars.currentBlock;
}


/**
  * Sets the call target for the next page. In particular, this stets the last element in
  * the execution stack {@link scriptVars.nextBlock} to the index of the code block
  * with the label <code>returnLabel</code> and pushes the index given by <code>label</code>
  * to the stack.
  * In order to implement call functionality, the code block must be exited using the <code>return;</code> statement.
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  * @param label The label of the code block which is executed as next.
  * @param returnLabel Code block used as return target.  If it is a string, it is interpreted as a label.
  * If it is a boolean and true, {@link currentBlock()} is used, otherwise {@link currentBlock()}+1.
  */
function setCall(label, returnLabel) {
    var idx;
    if ( typeof(returnLabel) == 'string' ) {
	idx = findLabel(returnLabel);
	if ( idx < 0 ) {
	    scriptError("label `" + returnLabel + "' does not exist");
	    return;
	}
    }
    else if ( (typeof(returnLabel) == 'boolean') && returnLabel ) idx = currentBlock();
    else idx = currentBlock()+1;
    scriptVars.nextBlock[scriptVars.nextBlock.length-1] = idx;
    
    idx = findLabel(label);
    if ( idx < 0 ) scriptError("label `" + label + "' does not exist");
    else scriptVars.nextBlock[scriptVars.nextBlock.length] = idx;
}


/**
  * Returns from a call. 
  * This is done by removing the last element from the execution stack {@link scriptVars.nextBlock};
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>true</code>.
  */
function callReturn() {
    scriptVars.nextBlock.pop();
}


// Set to true in order to interrupt delay
var delayInterrupt = false;


/**
  * If that function is called during {@link delay} (usually by pressing a button), {@link delay} will exit with return value <code>false</code>.
  */
function interrupt() {
    delayInterrupt = true;
}


/** 
  * This implements the delay function. It waits until either a timeout is reached, {@link interrupt()} is called
  * or execution pointer {@link scriptVar.nextBlock} is modified. In this case it returns <code>true</code>, in all other cases <code>false</code>.
  * <p>
  * This function is asynchronous, i.e. it always must be called using the <code>await</code> operator. In order to implement branching (see {@link setGoto} and {@link setCall})
  * the delay function should be used as follows:
  * <pre class="code">if ( await delay(seconds) ) return; </pre>
  * @param seconds Positive numbers are treated as wait time in seconds. 'V' or 'v' means to wait until current video ends. 'A' or 'a' means to wait until current audio file ends. In all other cases delay waits for ever / until interrupted. 
  */
function delay(wait) {}  // dummy for jsDoc
async function delay(wait) {
    finishPage();
    let ct = new Date().getTime();
    let waitUntil = ct + ( ( (typeof(wait) == 'number') && (wait>=1e-10) ) ? wait*1000 : 1e6*1000 );
    saveState();		// best place to save state on server
    let ep = scriptVars.nextBlock.length-1;
    var nextBlock = null;
    if ( ep < 0 ) ep = 0;
    else {
	nextBlock = scriptVars.nextBlock[ep];
	scriptVars.nextBlock[ep] = -1234;	// in order to detect any assignmnent
    }
    delayInterrupt = false
    let result = false;
    let cnt = 0;
    const wv = ( wait == 'V' ) || ( wait == 'v' );
    const wa = ( wait == 'A' ) || ( wait == 'a' );
    delayStart1();
    while ( !delayInterrupt && !result && (ct<waitUntil) ) {
	if ( cnt%5 === 0 ) {
	    const r = wv ? videoRemaining() : wa ? audioRemaining() : NaN;
	    if ( !isNaN(r) ) waitUntil = ct + r*1000;
	    delayIdle1((waitUntil-ct)*0.001);
	} 
	cnt++;
	if ( (scriptVars.nextBlock.length==ep+1) && (scriptVars.nextBlock[ep]==-1234) ) {
	    await new Promise(resolve => setTimeout(resolve, 50));		// 50 ms tick
	} else {
	    result = true;
	}
	ct = new Date().getTime();
    }
    if ( (scriptVars.nextBlock.length>ep) && (scriptVars.nextBlock[ep] == -1234 ) ) scriptVars.nextBlock[ep] = nextBlock;
    return result;
}


/** 
  * Automatically saves the state. Currently not used.
  */
function saveState() {
}

/** 
  * This is the main script loop. The script is either exited if the execution stack 
  * {@link scriptVars.nextBlock} becomes empty (usually by using {@link callReturn}
  * or after the last code block has been executed.
  * For starting a script the function {@link startScript} should be used.
  */
function runScript() {}		// to trick jsDoc
async function runScript() {
    if ( typeof(scriptVars.currentBlock) != "number" || startIdx<0 ) {
	scriptError("runScript cannot be called directly");
	return;
    }
    if ( runScriptActive ) {
	scriptError("runScript cannot be called if script is running");
	return;
    }
    runScriptActive = true;
    console.group("Started script");
    var idx = currentBlock();
    while (( idx>=0) && (idx<=scriptCode.code.length) ) {
	var ep = scriptVars.nextBlock.length-1;
	if ( ep < 0 ) ep=0;
	console.group("Running code block " + idx + " label="+scriptCode.label[idx] + " src=" + scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+"), ep="+ep );
	scriptVars.currentBlock = idx;
	scriptVars.nextBlock[ep] = -1;
	scriptVars.blockVisited[idx] = true;
	
	try {
 	    backupDOM1();
	    await AsyncFunction(scriptCode.code[idx]+"\n//# sourceURL="+ scriptCode.sourceFN[idx]+"("+scriptCode.sourceLN[idx]+")")();
	}
	catch ( e ) {
	    scriptError( e );
	}
	ep = scriptVars.nextBlock.length-1;
	if ( ep < 0 ) idx=-1;
	else {
	    idx = scriptVars.nextBlock[ep];
	    if ( (typeof(idx)!='number') || (idx<0) ) idx = currentBlock()+1;
	}
	console.groupEnd();
    }
    console.log("Script exited");

    scriptExitPage();
    
    console.groupEnd();
    scriptVars.nextBlock = [];
    runScriptActive = false;
}


/** 
  * This (re-)starts a script. If no start label is given the script starts with the first code block.
  * @param label Optional start label of the script. If it is not a string this argument is ignored.
  */
function startScript(label) {
    if ( typeof(label) == 'string' ) {
	var idx = findLabel(label);
	if ( idx < 0 ) scriptError("Start label `" + label + "' does not exist");
	startIdx = idx;
    } else startIdx = 0;
    initScriptVars0();
    autoRestoreScript();
    runScript();
}

/** 
  * This resets and restarts a script.
  */
function resetScript() {
    console.log("Reset script");
    scriptVarsPrev = null;
    initScriptVars0();
}

// Tries to restore automatically stored state. Currently not used. This operation is asynchronous. On success script is restarted.
function autoRestoreScript() {
/*    var hr = new XMLHttpRequest();
    hr.open("GET", "%3Aloadjson?name="+encodeURIComponent(stateName));
    hr.onload = function () {
	if ( hr.responseText.length<2 ) return;  // no error
	var scriptVarsTmp;
	try {
	    scriptVarsPrev = JSON.parse(hr.responseText);
	    initScriptVars0();
	    scriptVars.currentBlock = startIdx;
	    scriptVars.nextBlock = [];
	    scriptVars.nextBlock[0] = startIdx;
	    serverMode = true;
	    console.log("Loaded previous state. Enabling server mode. Restarting script.");
	} catch ( e ) {
	    scriptError( "Error parsing automatically restored state: " + e.message);
	}
    };
    try {
	hr.send();
    }
    catch ( e ) {
        // not an error
    } */
}


/** 
  * Restore the state of a script 
  * @param data If it is a string, it is interpreted as JSON object. If it is null, nothing happens. If it is an object it is used to set {@link scriptVars} directly.
  */
function restoreScript(data) {
    var scriptVarsTmp = null;
    if ( typeof(data) == "string" ) {
	try {
	    scriptVarsTmp = JSON.parse(data);
	} catch ( e ) {
    	    scriptError( "Error parsing restored state from JSON: " + e.message);
    	}
    }
    else if ( typeof(data) == "object" ) scriptVarsTmp = data;
    else scriptError( "Invalid argument: String or object expected");
    if ( scriptVarsTmp == null ) return;

    initScriptVars0();
    scriptVars = scriptVarsTmp;
    restoreDOM1();
    const ep = scriptVars.nextBlock.length-1;
    if ( ep>=0 ) scriptVars.nextBlock[ep] = scriptVars.currentBlock;
}


/** 
  * Reads a JSON object from a <code>FileList</code> object (e.g. from a <code>&lt;input type="file"&gt;</code> element).
  * Usually this is used to restore a saved state from a previous session.
  * @param files The <code>FileList</code> object which should only contain one <code>File</code> object.
  * @param cb Optional function that is called with the JSON object as parameter. By default, {@link restoreScript()} is called.
  */
function readStateFromFile(files, cb) {
    if ( typeof(cb) != "function" ) cb = restoreScript;
    for (let i = 0; i < files.length; i++) {
	const file = files[i];
//    if (!file.type.startsWith('text/')){ continue }
	console.log("Reading file '" + file.name + "' (type='"+file.type+"', size="+file.size+")");
    
	const reader = new FileReader();
	reader.onload = function(e) { 
	    try {
		cb(JSON.parse(e.target.result));
	    } catch ( e ) {
    		scriptError( "Error parsing restored state from JSON: " + e.message);
	    }
	}
	reader.readAsText(file);
    }
}

5
