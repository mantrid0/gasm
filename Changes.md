Feb 04 2023
===========
* Fork from [MPGenerator](https://gitlab.com/mantrid0/mpgenerator/blob/master/Readme.md), initial release as standalone project
* Added Estim support to GAsm RTL

Jul 31 2023
===========
* Improved timing support for videos and audio

Mar 24 2024
===========
* Fixed formatting errors in Readme.md
* New low level EStim API
* High level EStim API
* More input elements for forms
* EStim API supports Support of saving / restoring state

Aug 04 2024
===========
* Fixed bug in function `findPoolLabel` in rtl/utils.js
* Added GAam macro `@{`
