package gasm;

import java.io.*;

// *****************************************************************************
// ******* FindFiles ***********************************************************
// *****************************************************************************
/** 
  * Class for finding files with given suffixes
  */
public class FindFiles implements FileFilter {
/// Searches for images
    public static final FindFiles images = new FindFiles( "jpg", "jpeg", "png", "gif" );
/// Searches for videos
    public static final FindFiles videos = new FindFiles( "mp4", "webm", "ogv" );
/// Searches for audio files
    public static final FindFiles audioFiles = new FindFiles( "ogg", "mp3", "wav", "oga" );
/// Searches for audio web
    public static final FindFiles webFiles = new FindFiles( "htm", "html" );
    
    private String[] suffixes;

/** Constructs a file finder
  * @param suffixes List of suffixes without point. Case is ignored.
  */  
    public FindFiles ( String... suffixes ) {
	this.suffixes = new String[suffixes.length];
	for ( int i=0; i<suffixes.length; i++ )
	    this.suffixes[i] = suffixes[i].toLowerCase();
    }

/** Tests whether a given file is a match
  * @param file File to test
  * @return Return true if it is a match.
  */  
    public boolean accept(File file) {
	if ( !file.isFile() ) return false;
	String fn = file.getName();
	int i = fn.lastIndexOf('.');
	if ( (i<0) || (i>=fn.length()-1) ) return false;
	String s = fn.substring(i+1).toLowerCase();
	for ( int j=0; j<suffixes.length; j++ )
	    if ( s.equals(suffixes[j]) ) return true;
	return false;
    }    

/** List matching files in given directory
  * @param dir Directory
  * @return Return a string array with matches
  */  
    public String[] list(File dir) {
	File[] files = dir.listFiles(this);
	if ( files == null ) return null;
	String[] res = new String[files.length];
	for ( int i=0; i<files.length; i++ )
	    res[i] = files[i].getName();
	return res;
    }
    
}
