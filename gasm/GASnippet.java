package gasm;

// *****************************************************************************
// ******* GASnippet ***********************************************************
// *****************************************************************************
/** Class representing a code snippet
  */
public class GASnippet {
    // The JavaScript code.
    protected String code;	
    // The label of the snippet. Always lowercase.
    protected String label;	
    // Source file name (for debugging)
    protected String sourceFN;
    // Line offset in source file (for debugging)
    protected int sourceLN;
    
/** Constructs a new code snippet
  * @param code The JavaScript code snippet.
  * @param label The label of the snippet.
  * @param sourceFN Source file name (for debugging)
  * @param sourceLN Line offset in source file (for debugging)
  */  
    GASnippet ( String code, String label, String sourceFN, int sourceLN ) {
	this.code = ((code!=null) && (code.length()==0)) ? null : code;
	this.label = ((label==null) || (label.length()==0)) ? null : label.toLowerCase();
	this.sourceFN = sourceFN == null ? "(undefined)" : sourceFN.replace('\\','/');
	this.sourceLN = sourceLN;
    }
}
