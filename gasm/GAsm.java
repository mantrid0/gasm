package gasm;

import java.io.*;
import java.util.*;
import java.text.*;
import java.util.regex.*;

/* 
    Todo: 
    * JSON reader using json-p (javax.json)
    * pool command: batch include using shell filter and pools creation
    * form example
    * (JavaScript) SourceMaps
*/

// *****************************************************************************
// ******* Gasm ****************************************************************
// *****************************************************************************
/** Class which provides the assembler, disassembler and routines for json export.
  */
public class GAsm {
    private class SourcePos {
	public String name;	// name or target
	public String sourceFN;	// source file name
	public int sourceLN;	// source line number
	public SourcePos(String name, String sourceFN, int sourceLN ) {
	    this.name = name;
	    this.sourceFN = sourceFN;
	    this.sourceLN = sourceLN;
	}
    }

    /// Path that is prepended to all path names. Usually that should be the directory of the script source.
    public String baseDir = ".";

    /// Data directory in server mode
    private String dataDir = "";

    /// New line
    private String nl = System.getProperty("line.separator");
    
    private AbstractList<String> includes = new LinkedList<String>();		// list of included files
    private AbstractList<SourcePos> targets = new LinkedList<SourcePos>();	// list of targets, checked in 2nd stage
    private AbstractList<String> buttonNames = new LinkedList<String>();	// list of button names
    private AbstractList<SourcePos> buttons = new LinkedList<SourcePos>();	// list of names in remove button macros, checked in 2nd stage
    private String imageDir = ".";						// search images in that directory
    private String videoDir = ".";						// search videos in that directory
    private String audioDir = ".";						// search audio files in that directory
    private LinkedList<String> currentDir = new LinkedList<String>();		// also search files in current directory stored in this stack. Use empty string or null for main directory.
    
    // scans for assignment of a variable using @ macro. Matching group 1 is the variable name, matching group 3 the value.
    private final Pattern macroVarAssignment = Pattern.compile("\\s*@(\\w+)\\s*=\\s*([\"'])(.*)\\2.*");

    // scans for a string literal
    private final Pattern stringLiteral = Pattern.compile("\\s*([\"'])(.*)\\1\\s*");

    // scans setCall calls with one or two string literals
    private final Pattern setCall2 = Pattern.compile(".*setCall\\s*\\(\\s*([\"'])(.*)\\1\\s*,\\s*([\"'])(.*)\\3\\s*[,\\)].*");
    private final Pattern setCall1 = Pattern.compile(".*setCall\\s*\\(\\s*([\"'])(.*)\\1\\s*[,\\)].*");

    // scans setGoto calls with one string literals
    private final Pattern setGoto1 = Pattern.compile(".*setGoto\\s*\\(\\s*([\"'])(.*)\\1\\s*[,\\)].*");
    
/** 
  * Constructs an instance. 
  */
    public GAsm ( ) {
	dataDir = "";
    }

/** 
  * Constructs an instance which runs in a server as JIT compiler.
  * @param dataDir Date directory of the server
  */
    public GAsm (String dataDir ) {
	this.dataDir = dataDir + File.separator;
    }

    // cheks for non-existent labels in ins setCall and setGoto commands
    private void findLabels (String str, String sourceFN, int sourceLN) {
        Matcher m = setCall2.matcher(str);
        if ( m.matches() ) {
    	    String g2 = m.group(2);
    	    String g4 = m.group(4);
    	    if ( (g2.indexOf(m.group(1))<0) && (g4.indexOf(m.group(3))<0) ) {	// may happen with complex expressions
    		targets.add(new SourcePos(g2.toLowerCase(), sourceFN, sourceLN));
    		targets.add(new SourcePos(g4.toLowerCase(), sourceFN, sourceLN));
    	    }
    	} else {
    	    m = setCall1.matcher(str);
    	    if ( m.matches() ) {
    	        String g2 = m.group(2);
    	        if ( g2.indexOf(m.group(1))<0 ) targets.add(new SourcePos(g2.toLowerCase(), sourceFN, sourceLN));
    	    }
    	}
        m = setGoto1.matcher(str);
        if ( m.matches() ) {
            String g2 = m.group(2);
            if ( g2.indexOf(m.group(1))<0 ) targets.add(new SourcePos(g2.toLowerCase(), sourceFN, sourceLN));
    	}
    }
    
    // Replaces @ macros and removes trailing spaces.
    private String replaceAt (String str, boolean onlyVars, boolean inText, String sourceFN, int sourceLN) throws ParseException {
	if ( str == null ) return "";
	int i=0, j=0, l=str.length()-1;
	StringBuilder sb = new StringBuilder();
	while ( i < l ) {
	    char ch = str.charAt(i);
	    if ( ch == '@' ) {
		char ch1 = str.charAt(i+1);
		if ( ch1 == '@' ) {
		    sb.append(str.substring(j,i+1));
		    j=i+2;
		    i=j-1;
		} else if ( inText && (ch1=='{') ) {
		    if ( i > j ) sb.append(str.substring(j,i));
		    int bc = 1;
		    int k = i + 1;
		    while ( (bc>0) && (k<l) ) {
			k++;
			ch1 = str.charAt(k);
			if ( ch1 == '{' ) bc+=1;
			else if ( ch1 == '}' ) bc-=1;
		    }
		    if ( bc>0 ) throw new ParseException("Invalid JavaScript macro: '{'", i+1);
		    if ( k > i+2 ) sb.append("' + (" + replaceAt(str.substring(i+2,k), false, false, sourceFN, sourceLN) + ") + '");
		    j = k + 1;
		    i = j - 1;
		} else if ( ((ch1>='a') && (ch1<='z')) || ((ch1>='A') && (ch1<='Z')) ) {
		    int k=i+1;
		    char ch2 = ' ';
		    do {
			k++;
			ch1 = (k>l) ? ' ' : str.charAt(k);
			ch2 = (k+1>l) ? ' ' : str.charAt(k+1);
		    } while ( ((ch1>='a') && (ch1<='z')) || ((ch1>='A') && (ch1<='Z')) || ((ch1>='0') && (ch1<='9')) || (ch1=='_') || ((ch1=='.') && ( ((ch2>='a') && (ch2<='z')) || ((ch2>='A') && (ch2<='Z')) )) );
		    if ( i > j ) sb.append(str.substring(j,i));
		    if ( (ch1 == '(') && !inText && !onlyVars ) { // JavaScript macro 
			String cmd = str.substring(i+1,k);
			int m = str.indexOf(");",k);
			if ( m <= k+1 ) throw new ParseException("');' expected",l+1);
			String args = replaceAt( str.substring(k+1,m), true, false, sourceFN, sourceLN);	// replace @variables in arguments
			if ( cmd.equals("goto") ) sb.append("{ setGoto(").append(args).append("); return; }");
			else if ( cmd.equals("call") ) sb.append("{ setCall(").append(args).append("); return; }");
			else if ( cmd.equals("delay") ) sb.append("{ if ( await delay(").append(args).append(") ) return; }");
			else if ( cmd.equals("label") ) {
			    Matcher ma = stringLiteral.matcher(args);
			    if ( ma.matches() ) {
				String target = adjustTarget(ma.group(2));
				targets.add(new SourcePos(target, sourceFN, sourceLN));
				sb.append("\"").append(target).append("\";");
			    } 
			    else {
				sb.append(args).append(";");
			    }
			}
			else throw new ParseException("Invalid JavaScript macro: '" + cmd + "'", i+1);
			j = m + 2;
			i = j - 1;
		    } else {
/*    			sb.append( inText ? "'+scriptVars['" : "scriptVars['" );
			sb.append( str.substring(i+1,k) );
			sb.append( inText ? "']+'" : "']" );  */
			sb.append( inText ? "'+scriptVars." : "scriptVars." );
			sb.append( str.substring(i+1,k) );
			if ( inText ) sb.append( "+'" );
			j = k;
			i = j - 1;
		    }
		}
	    }
	    i++;
	}
	String out;
	if ( j == 0 ) out=str;
	else {
	    if (j<=l) sb.append(str.substring(j,l+1));
	    out = sb.toString();
	}
	if ( ! inText) findLabels(out, sourceFN, sourceLN );
	return out;
    }


    /* Searches file in absolute directory / current directory and returns it name ralative to absolute directory or null if not found. 
       In particular it returns:
       * fileName if dataDir+baseDir+dirName+fileName exists
       * currentDir.getFirst()+fileName if dataDir+baseDir+dirName+currentDir.getFirst()+fileName exists
       * null otherwise.
       dataDir is ignored if null or empty 
     */
    private String findFile (String dirName, String fileName ) {
	String fn = ( File.separatorChar == '/' ) ? fileName : fileName.replace('/',File.separatorChar);
	String dir = ( (dirName==null) || (dirName.length()<1) ) ? (dataDir+baseDir+File.separator) : (dataDir+baseDir+File.separator+dirName+File.separator);
	if ( new File( dir + fn ).isFile() ) return fileName;
	else {
	    try {
		String cd = currentDir.getFirst();
		if ( (cd==null) || (cd.length()<1) ) return null;
		if ( new File( dir + cd + File.separator + fn ).isFile() ) return cd + '/' + fileName;
	    }
	    catch ( Exception e ) {
	    }
	}
        return null;
    }

    // return true if target is valid 
    private static boolean checkTarget(String target, AbstractList<GASnippet> script) {
	if ( (target==null) || (target.length()<1) ) return true;		// empty targets are always valid
	if ( script==null) return false;
	for ( GASnippet cs: script ) 
	    if ( (cs.label!=null) && (cs.label.equals(target)) ) return true;
	return false;
    }

    // return true if button name is known 
    private boolean checkButton(String name) {
	if ( (name==null) || (name.length()<1) ) return false;		// empty names are considered as invalid
	for ( String bn: buttonNames ) 
	    if ( (bn!=null) && (bn.equalsIgnoreCase(name)) ) return true;
	return false;
    }

    // removes last colon and convert it to lowercase 
    private static String adjustTarget(String target) {
	int l = target.length()-1;
	if ( (l>0) && (target.charAt(l)==':') ) return target.substring(0,l).toLowerCase();
	return target.toLowerCase();
    }

    // escape some characters in text and button names
    private static String escape(String s) {
	return s.replace("\\","\\\\").replace("'","\\'");
    }

/** 
  * Reads and assembles a script. 
  * @param reader Input stream. Closed before return.
  * @param name Name of the input source
  * @param script The assembled script.
  * @param msgs Syntax errors and messages are written to this StringBuilder
  * @throws IOException if an error occurs while attempting to read the file.
  * @return The number of errors
  */  
    public int asm (BufferedReader reader, String name, AbstractList<GASnippet> script, StringBuilder msgs) throws IOException {
	int line = 0;
	int csLine = -1; 							// start line of the code snippet or <0 if no code snippet is assembled
	boolean csEmpty = true;
	String label = null;
	StringBuilder sb = new StringBuilder(256);				// the code snippet builder
	String lbuf;
	boolean eof = false;
	boolean first = script.size() == 0;
	int errors = 0;
	do {
	    lbuf = reader.readLine();
	    line++;
	    if ( lbuf == null ) {
		lbuf = ":"; 							// enforced output of the last code snippet
		eof = true;
	    }
	    int ll = lbuf.length();
	    
	    while ( (ll>0) && (lbuf.charAt(ll-1)=='\\') ) {			// append lines that end with '\'
		lbuf = lbuf.substring(0,ll-1);
		String lbuf2 = reader.readLine();
		if ( lbuf2 == null ) break;
		lbuf += lbuf2;
		ll = lbuf.length();
	        line++;
	    }
	    
	    int i = 0;
	    while ( (i<ll) && ( lbuf.charAt(i)<=' ') ) 
		i++;

	    if ( i>=ll ) {	 						// an empty line
		if (csLine>0 ) sb.append('\n');					// ensures correct line calculations 
	    } else {
		String cmd = null;
		String newLabel = null;
	    	String includeFile = null;
	    	boolean isLabel = false;

		char ch = lbuf.charAt(i);
		if ( (ch=='/') && (i+1<ll) && (lbuf.charAt(i+1)=='/') ) {  	// //...
		    if (csLine > 0) {
	    		sb.append(lbuf); 					// save the comment for better debugging
			sb.append('\n');
		    }
		} else if ( ch == '#' ) {					// #: JavaScript code
		    if (i+1<ll) {
			cmd = lbuf.substring(i+1,ll);
		 	if ( first ) {
			    Matcher m = macroVarAssignment.matcher(cmd);
			    if ( m.matches() ) {
				String vn = m.group(1);
				if ( "imageDir".equals(vn) ) imageDir = m.group(3);
				else if ( "videoDir".equals(vn) ) videoDir = m.group(3);
				else if ( "audioDir".equals(vn) ) audioDir = m.group(3);
		    	    }
		    	}
		    	try {
			    cmd = replaceAt(cmd, false, false, name, line);
			}
			catch ( ParseException pe ) {
			    errors++;
			    msgs.append("Syntax Error: " + name + "(" + line + "," + pe.getErrorOffset() + "): " + pe.getLocalizedMessage() + nl);
			}
		    }
		} else {
		    // end of usable line
		    int j = lbuf.indexOf("//");
		    if ( j>0 ) ll = j;
		    while ( lbuf.charAt(ll-1)<=' ' )
			ll--;
		    // scan keyword
		    j = i;
		    while ( (j<ll) && (lbuf.charAt(j)>' ') && (lbuf.charAt(j)!=':') )
			j++;
		    if ( (j<ll) && (lbuf.charAt(j)==':') ) { 			// it's a label
			isLabel = true; // empty labels are allowed
			if ( i<j ) newLabel = lbuf.substring(i,j).toLowerCase();
		    } else {
			String key = lbuf.substring(i,j);
			j++;
			while ( (j<ll) && (lbuf.charAt(j)<=' ') ) 
			    j++;
			if ( j>=ll ) {
		    	    // statements with accept empty argument
			    if ( key.equals("<") ) cmd = "   clearText('');";			// set text macro
			    else if ( key.equals("<<") ) cmd = "   appendText('');";		// append empty line 
			    else if ( key.equalsIgnoreCase("I") || key.equalsIgnoreCase("V") ) cmd = "   clearMedia();";	// clear media area
			    else if ( key.equalsIgnoreCase("A") ) cmd = "   stopAudio();";	// stop audio
			    else if ( key.equalsIgnoreCase("R") ) cmd = "   { callReturn(); return; }";	// return from call
			    else if ( key.equalsIgnoreCase("D") ) {
				cmd = "   { if ( await delay() ) return; }";
				isLabel = true;					// starts a new code block
			    }
			    else {
				errors++;
				msgs.append("Error: " + name + "(" + line + "): Invalid statement: Unknown keyword or insufficient parameters." + nl);
			     }
			}
			// statements with one parameter
			else if ( key.equals("<") ) {	
			    try {			// set text macro
				cmd = "   setText('" + replaceAt(escape(lbuf.substring(j,ll)), true, true, name, line) + "');";
			    }
			    catch ( ParseException pe ) {
				errors++;
				msgs.append("Error: " + name + "(" + line + "," + pe.getErrorOffset() + "): " + pe.getLocalizedMessage() + nl);
			    }
			}
			else if ( key.equals("<<") ) {				// append text macro
			    try {			// set text macro
				cmd = "   appendText('" + replaceAt( escape(lbuf.substring(j,ll)), true, true, name, line) + "');";
			    }
			    catch ( ParseException pe ) {
				errors++;
				msgs.append("Error: " + name + "(" + line + "," + pe.getErrorOffset() + "): " + pe.getLocalizedMessage() + nl);
			    }
			}
			else if ( key.equalsIgnoreCase("I") ) {			// image macro
			    String fn = lbuf.substring(j,ll);
			    String fn2 = findFile(imageDir,fn);
			    cmd = "   setImage('" + ( fn2==null ? fn : fn2 ) + "');";
			    if ( fn2==null ) msgs.append("Warning: " + name + "(" + line + "): Cannot find image '"+ fn + "'" + nl);
			}
			else if ( key.equalsIgnoreCase("V") ) {			// video macro
			    String fn = lbuf.substring(j,ll);
			    String fn2 = findFile(videoDir,fn);
			    cmd = "   playVideo('" + ( fn2==null ? fn : fn2 ) + "');";
			    if ( fn2==null ) msgs.append("Warning: " + name + "(" + line + "): Cannot find video '" + fn + "'" + nl);
			}
			else if ( key.equalsIgnoreCase("A") ) {			// audio macro
			    String fn = lbuf.substring(j,ll);
			    String fn2 = findFile(audioDir,fn);
			    cmd = "   playAudio('" + ( fn2==null ? fn : fn2 ) + "');";
			    if ( fn2==null ) msgs.append("Warning: " + name + "(" + line + "): Cannot find audio file '" + fn + "'" + nl);
			}
			else if ( key.equalsIgnoreCase("G") ) {			// goto macro
			    String target = adjustTarget(lbuf.substring(j,ll));
			    cmd = "   { setGoto('" + target + "'); return; }";
			    targets.add(new SourcePos(target,name,line));
			}
			else if ( key.equalsIgnoreCase("C") ) {			// call macro
			    String target = adjustTarget(lbuf.substring(j,ll));
			    cmd = "   { setCall('" + target + "'); return; }";
			    targets.add(new SourcePos(target,name,line));
			    isLabel = true;					// starts a new code block
			}
			else if ( key.equalsIgnoreCase("RB") ) {		// remove button macro
			    String bname = lbuf.substring(j,ll);
			    cmd = "   rmButton('" + escape(bname) + "');";
			    buttons.add(new SourcePos(bname,name,line));
			}
			else if ( key.equalsIgnoreCase("D") ) {			// delay macro
			    String delay = lbuf.substring(j,ll);
			    if ( delay.equalsIgnoreCase("A") || delay.equalsIgnoreCase("V") ) delay = "'" + delay + "'";
			    else {
				try {
    				    Double.parseDouble( delay );
				} 
				catch (Exception e) {
				    errors++;
				    msgs.append("Error: " + name + "(" + line + "): Number expected after 'D'" + nl);
				}
			    }
			    cmd = "   { if ( await delay(" + delay + ") ) return; }";
			    isLabel = true;					// starts a new code block
			}
			else if ( key.equals(".") ) {				// include directive
			    includeFile = lbuf.substring(j,ll);
			}
			else {	
			    //statements with two parameters
			    int k=j+1;
			    while ( (k<ll) && (lbuf.charAt(k)>' ') ) 
				k++;
			    String arg1 = lbuf.substring(j,k);
			    j=k+1;
			    while ( (j<ll) && (lbuf.charAt(j)<=' ') ) 
				j++;
			    if ( j>=ll ) {
				errors++;
				msgs.append("Error: " + name + "(" + line + "): Invalid statement: Unknown keyword or insufficient parameters." + nl);
			    }
			    else if ( key.equalsIgnoreCase("BG") ) {		// goto button macro
				arg1 = adjustTarget(arg1);
				String bname = lbuf.substring(j,ll);
				cmd = "   addButton(\"setGoto('" + arg1 + "')\", '" + escape(bname) + "');";
				targets.add(new SourcePos(arg1,name,line));
				buttonNames.add(bname);
			    } 
			    else if ( key.equalsIgnoreCase("BC") ) {		// call button macro. return address is next code block
				arg1 = adjustTarget(arg1);
				String bname = lbuf.substring(j,ll);
				cmd = "   addButton(\"setCall('" + arg1 + "')\", '" + escape(bname) + "');";
				targets.add(new SourcePos(arg1,name,line));
				buttonNames.add(bname);
			    } 
			    else if ( key.equalsIgnoreCase("BCR") ) {		// call button macro. return address is current code block
				arg1 = adjustTarget(arg1);
				String bname = lbuf.substring(j,ll);
				cmd = "   addButton(\"setCall('" + arg1 + "',true)\", '" + escape(bname) + "');";
				targets.add(new SourcePos(arg1,name,line));
				buttonNames.add(bname);
			    } 
			    else if ( key.equalsIgnoreCase("TBG") ) {		// temporary goto button macro
				arg1 = adjustTarget(arg1);
				String bname = lbuf.substring(j,ll);
				cmd = "   appendTButton(\"setGoto('" + arg1 + "')\", '" + escape(bname) + "');";
				targets.add(new SourcePos(arg1,name,line));
			    } 
			    else if ( key.equalsIgnoreCase("TBC") ) {		// temporary call button macro. return address is next code block
				arg1 = adjustTarget(arg1);
				String bname = lbuf.substring(j,ll);
				cmd = "   appendTButton(\"setCall('" + arg1 + "')\", '" + escape(bname) + "');";
				targets.add(new SourcePos(arg1,name,line));
			    } 
			    else if ( key.equalsIgnoreCase("TBCR") ) {		// temporary call button macro. return address is next code block
				arg1 = adjustTarget(arg1);
				String bname = lbuf.substring(j,ll);
				cmd = "   appendTButton(\"setCall('" + arg1 + "',true)\", '" + escape(bname) + "');";
				targets.add(new SourcePos(arg1,name,line));
			    } 
			    else if ( key.equalsIgnoreCase("B") ) {		// button macro
				String bname = lbuf.substring(j,ll);
				cmd = "   addButton('" + arg1 + "', '" + escape(bname) + "');";
				buttonNames.add(bname);
			    } 
			    else if ( key.equalsIgnoreCase("TB") ) {		// temporary button macro
				String bname = lbuf.substring(j,ll);
				cmd = "   appendTButton('" + arg1 + ";', '" + escape(bname) + "');";
				buttonNames.add(bname);
			    } 
			    else if ( key.equalsIgnoreCase("VS") ) {		// video macro with given start time
				String fn = lbuf.substring(j,ll);
				String fn2 = findFile(videoDir,fn);
				cmd = "   playVideo('" + ( fn2==null ? fn : fn2 ) + "'," + arg1 +");";
				if ( fn2==null ) msgs.append("Warning: " + name + "(" + line + "): Cannot find video '" + fn + "'" + nl);
			    }
			    else if ( key.equalsIgnoreCase("AS") ) {		// audio macro with given start time
				String fn = lbuf.substring(j,ll);
				String fn2 = findFile(videoDir,fn);
				cmd = "   playAudio('" + ( fn2==null ? fn : fn2 ) + "'," + arg1 +");";
				if ( fn2==null ) msgs.append("Warning: " + name + "(" + line + "): Cannot find video '" + fn + "'" + nl);
			    }
			    else {
				errors++;
				msgs.append("Error: " + name + "(" + line + "): Invalid statement: Unknown keyword." + nl);
			    }
			}
		    }

		}
		// append command		
		if ( cmd!=null ) {
		    if ( csLine<0 ) csLine = line;
	    	    sb.append(cmd);
	    	    sb.append('\n');
	    	    csEmpty = false;
	    	}

		// finish the code snippet and start a new one. Include files are read here. 
		if ( isLabel || ( includeFile!=null ) ) {
		    if ( label!=null ) {
		        for ( GASnippet cs: script ) {
		    	    if ( label.equals(cs.label) ) {
				errors++;
				msgs.append("Error: " + name + "(" + csLine + ") and " + cs.sourceFN + "(" +cs.sourceLN + "): Multiple definitions of label '"+label+"'" + nl);
			    }
			}
		    }
		    if ( (label!=null) || (!csEmpty) ) script.add( new GASnippet( csEmpty ? null : sb.toString(), label, name, csLine ) );
		    first &= csEmpty;
		    sb.setLength(0);
		    label = newLabel;
		    csLine = line;
		    csEmpty = true;
		    if ( includeFile!=null ) {
		    	try { 
		    	    errors+=include(includeFile,script,msgs, name + "(" + line + ")");
		    	}
		    	catch ( IOException e ) {
		    	    errors++;
			    msgs.append("Error: " + name + "(" + line + "): Unable to read file '" + includeFile +"': " + e.getLocalizedMessage() + nl);
		    	}
		    }
		}
	    }
	} while ( !eof );

	try {
	    reader.close();
	}
	catch ( Exception e ) {
	    // Ignore exceptions here 
	}
	return errors;
    }


/** 
  * Reads and assembles a script. 
  * @param fileName of the input source
  * @param script The assembled script.
  * @param msgs Syntax errors and messages are written to this StringBuilder
  * @param posStr Position if the include directive of null
  * @throws IOException if an error occurs while attempting to read the file.
  * @return The number of errors
  */  
    private int include ( String fileName, AbstractList<GASnippet> script, StringBuilder msgs, String posStr) throws IOException {
	String fn = findFile(null,fileName);
	if ( fn == null ) throw new IOException( "File not found: '"+ fileName + "'");
	System.err.println(fn);
	File file = new File( dataDir + baseDir + File.separator + (( File.separatorChar == '/' ) ? fn : fn.replace('/',File.separatorChar)) );
	if ( includes == null ) includes = new LinkedList<String>();
	if ( (posStr!=null) && includes.contains(fn) ) msgs.append("Warning: " + posStr + ": File '" + fileName +"' already has been included" + nl);
	includes.add(fn);
	int i = fn.lastIndexOf('/');
	currentDir.push( (i<=0) ? null : fn.substring(0,i) );
	int e = asm( new BufferedReader(new FileReader(file)), fileName, script, msgs );
	currentDir.pop();
	return e;
    }

/** 
  * Reads and assembles a script. 
  * @param fileName of the input source
  * @param script The assembled script.
  * @param msgs Syntax errors and messages are written to this StringBuilder
  * @throws IOException if an error occurs while attempting to read the file.
  * @return The number of errors
  */  
    public int asm ( String fileName, AbstractList<GASnippet> script, StringBuilder msgs) throws IOException {
	int i = fileName.lastIndexOf(File.separatorChar);
	if ( (i>0) && (i<fileName.length()-1) ) {
	    baseDir = fileName.substring(0,i);
	    if ( (dataDir.length()>0) && baseDir.startsWith(dataDir) ) {	// strip data dir
		int l = dataDir.length();
		if ( baseDir.length() <= l ) baseDir = ".";
		else baseDir = baseDir.substring(l);
	    }
	    fileName = fileName.substring(i+1);
	}
	return include(fileName, script, msgs, null);
    }

/** 
  * Reads and assembles a script. 
  * @param in An input stream
  * @param name Name of the input
  * @param script The assembled script.
  * @param msgs Syntax errors and messages are written to this StringBuilder
  * @throws IOException if an error occurs while attempting to read the file.
  * @return The number of errors
  */  
    public int asm ( InputStream in, String name, AbstractList<GASnippet> script, StringBuilder msgs) throws IOException {
	return asm( new BufferedReader(new InputStreamReader(in)), name, script, msgs );
    }

/** 
  * 2nd stage of assembler: which checks targets and button names
  * @param script The assembled script.
  * @param msgs Syntax errors and messages are written to this StringBuilder
  * @return The number of errors
  */  
    public int asm2 (AbstractList<GASnippet> script, StringBuilder msgs) {
	int errors=0;
	// check targets
	for ( SourcePos sp: targets ) {
	    if ( ! checkTarget(sp.name, script) ) {
		errors++;
	        msgs.append("Error: " + sp.sourceFN+ "(" + sp.sourceLN + "): Label not found: '"+sp.name+"'" + nl);
	    }
	}
	// names of remove button macros
	for ( SourcePos sp: buttons ) {
	    if ( ! checkButton(sp.name) ) msgs.append("Warning: " + sp.sourceFN+ "(" + sp.sourceLN + "): A button with name '"+sp.name+"' has not been defined with button macros" + nl);
	}
	return errors;
    }


// List directories and sub-directories
// name: file name of the scrip which is used to extract base directory
// dirs: where to store the result
    private void findDirectories( File dir, AbstractList<File> dirs) {
	File[] files = dir.listFiles();
	if ( files == null ) return;	// not a directory, should not happen
	dirs.add(dir);
	for ( int i=0; i<files.length;i++ ) 
	    if ( files[i].isDirectory() ) findDirectories(files[i], dirs );
    }

// Print file listing
// out: output
// name: Name of the variable
// filter : File type filter
// dirs: Directory listing
    private void writeListing(StringBuilder out, String name, FindFiles filter, AbstractList<File> dirs) {
	String dbd = dataDir+baseDir;
	int bl = dbd.length()+1;
	out.append("\nconst ").append(name).append(" = [\n");
	for ( File f: dirs ) {
	    String n = f.getPath();
	    if ( n.startsWith(dbd) ) {
		if ( n.length() <= bl ) n = ".";
		else n = n.substring(bl);
	    }
	    if ( File.separatorChar != '/' ) n = n.replace(File.separatorChar,'/');
	    out.append("    { \"dir\" : \"").append(n).append("\", \"files\" : [ ");
	    String[] files = filter.list(f);
	    if ( files!=null ) {
		for ( int j=0; j<files.length; j++ ) {
		    n = files[j];
	    	    if ( File.separatorChar != '/' ) n = n.replace(File.separatorChar,'/');
		    out.append("\"").append(n).append("\", ");
		}
	    }
	    out.append("] },\n");
	}
	out.append("];\n");
    }


/**
  * Escapes JavaScrip string. Equvalent to sb.append(s.replace("\\","\\\\").replace("\n","\\n").replace("\"","\\\""))
  * @param sb StringBuilder for saving the data 
  * @param s String which is escaped"
  */
    private final StringBuilder encodeJSString(StringBuilder sb, String s) {
	if ( s==null ) return sb;
	int l = s.length();
	int i=0;
	for ( int j=0; j<l; j++ ) {
	    char ch = s.charAt(j);
	    if ( (ch!='\n') && (ch!='\\') && (ch!='\"') ) continue;
	    if ( i < j ) sb.append(s.substring(i,j));
	    i = j+1;
	    if ( ch == '\n' ) sb.append("\\n");
	    else if ( ch == '\\' ) sb.append("\\\\");
	    else sb.append("\\\"");
	}
	if ( i < l ) sb.append(s.substring(i));
	return sb;
    }
    

/** 
  * Writes an assembled script as JSON object
  * @param script The assembled script.
  * @param out Output is written to this StringBuilder 
  */  
    public void write ( AbstractList<GASnippet> script, StringBuilder out) {
	// write code clocks
	StringBuilder code = new StringBuilder();
	StringBuilder label = new StringBuilder();
	StringBuilder sourceFN = new StringBuilder();
	StringBuilder sourceLN = new StringBuilder();
	for ( GASnippet s: script ) {
	    if ( s.code == null ) code.append("	null,\n");
	    else encodeJSString(code.append("	\""), s.code).append("\",\n" );
//	    else code.append("	\"").append(s.code.replace("\\","\\\\").replace("\n","\\n").replace("\"","\\\"")).append("\",\n" );
	    if ( s.label == null ) label.append("	null,\n");
	    else label.append("	\"").append(s.label).append("\",\n");
	    sourceFN.append("	\"").append(s.sourceFN).append("\",\n");
	    sourceLN.append("	").append(s.sourceLN).append(",\n");
	}
	out.append("const scriptCode = {\n");
	out.append("    \"code\" : [\n");
	out.append(code);
	code = null;
	out.append("    ],\n");
	out.append("    \"label\" : [\n");
	out.append(label);
	label = null;
	out.append("    ],\n");
	out.append("    \"sourceFN\" : [\n");
	out.append(sourceFN);
	sourceFN = null;
	out.append("    ],\n");
	out.append("    \"sourceLN\" : [\n");
	out.append(sourceLN);
	sourceLN = null;
	out.append("    ]\n");
	out.append("};\n");
	code = null;
	label = null;
	sourceFN = null;
	sourceFN = null;
	
	// write file listings
	LinkedList<File> dirs = new LinkedList<File>();		// list of path where to search for source files
	findDirectories(new File(dataDir+baseDir), dirs);
	writeListing(out, "imageList", FindFiles.images, dirs);
	writeListing(out, "videoList", FindFiles.videos, dirs);
	writeListing(out, "audioList", FindFiles.audioFiles, dirs);

	// write base directory
	out.append("let baseDirectory=\"").append( (File.separatorChar=='/') ? baseDir : baseDir.replace(File.separatorChar,'/') ).append("\";\n");
    }

/** 
  * Disassemble a script. 
  * @param script The assembled script.
  * @param out Output is written to this StringBuilder 
  */  
    public static void dasm (AbstractList<GASnippet> script, StringBuilder out) {
	for ( GASnippet s: script ) {
	    if ( s.label!=null ) out.append("").append(s.label).append(":  ");
	    else out.append(":  ");
	    out.append("// ").append(s.sourceFN).append("(").append(s.sourceLN).append(")\n");
	    if ( s.code != null ) {
		String[] lines = s.code.split("\n");
		for ( int i=0; i<lines.length; i++ )
		    out.append("    #").append(lines[i]).append("\n");
	    }
	}
    }
    

// help message
    static class ParameterException extends Exception {
	public final static String helpMsg = new String (
		"Usage: java -jar GAam.jar [<option>] [<input> [<output>]]\n"+
		"Parameters:\n"+ 
		"    <input>    Input file. If undefined or '-' stdin is used\n" +
		"    <output>   Output file. If undefined or '-' stdout is used\n" +
		"Options:\n"+ 
//		"    -d         Disassemble (default: assemble)\n"+
		"    -ad        Assemble + disassemble (useful for debugging, default: assemble)\n"+
		"    -p <path>  Add a search path. Can be specified multiple times.\n" +
		"    -s <name>  Source name (useful if read from stdin)\n" +
		"    -h         Print this help message\n"
	    );
    
	public ParameterException (String msg) {
	    super( msg + "\n" + helpMsg );
	}

	public ParameterException () {
	    super( helpMsg );
	}
    }

/** 
  * Command line interface.
  * @param args Command line parameters
  */  
    public static void main(final String[] args) {
	try {
	    // parse parameters 
	    String ifn = null, ofn = null, sfn = null;
	    int mode = -1;
    	    for (int i=0; i<args.length; i++ ) {
		if ( args[i].equals("-s") ) {
		    i++;
	    	    if (i>=args.length) throw new ParameterException("<name> expected after -s");
	    	    sfn = args[i];
		}	
		else if ( args[i].equals("-h") ) {
		    System.out.println(ParameterException.helpMsg);
	    	    System.exit(0);
		}
/*		else if ( args[i].equals("-d") ) {
		    mode = 1;
		} */
		else if ( args[i].equals("-ad") ) {
		    mode = 0;
		}
		else if ( args[i].charAt(0) == '-' ) {
		    throw new ParameterException("Invalid option: " + args[i]);
		}
		else {
		    if ( ifn == null ) ifn = args[i];
		    else if ( ofn == null ) ofn = args[i];
		    else throw new ParameterException("To much parameters");
		}
	    }
	    
	    // process input 
	    LinkedList<GASnippet> script = new LinkedList<GASnippet>();
	    StringBuilder sb = new StringBuilder();
	    int errors = 0;
	    GAsm gasm = new GAsm();
	    if ( mode<=0 ) {
		if ( (ifn==null) || (ifn.equals("-")) ) errors = gasm.asm(System.in, "(stdin)", script, sb);
		else errors = gasm.asm(ifn, script, sb);
		errors += gasm.asm2(script, sb);
	    } else {
		// todo: parse JSON
	    }
	    if ( sb.length()>0 ) System.err.print(sb);
	    if ( errors>0 ) {
		System.err.println( "" + errors + " errors occurred");
		System.exit(2);
	    }

	    // process output
	    sb.setLength(0);
	    if ( mode>=0 ) dasm(script,sb);
	    else gasm.write(script,sb);
	    if ( (ofn==null) || (ofn.equals("-")) ) System.out.println(sb.toString());
	    else {
		PrintStream out = new PrintStream(ofn);
		out.println(sb.toString());
		out.close();
	    }
	    System.exit(0);
	}
	catch ( ParameterException e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	} 
	catch ( IllegalArgumentException e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	} 
	catch ( Exception e ) {
	    System.err.println("Error: " + e.getLocalizedMessage() );
	    e.printStackTrace();
	}
	System.exit(1);
    }

}
